<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return redirect()->route('login');
});
Route::group(['prefix' => env('PREFIX_ADMIN')],function(){
Route::get('/', function () {
	return redirect()->route('login');
});
});
	Route::get('/login', 'Auth\LoginController@showLoginAdmin')->name('login');
Route::group(['prefix' => env('PREFIX_ADMIN')],function(){
	Route::get('/login', 'Auth\LoginController@showLoginAdmin')->name('login');
	Route::get('/login', 'Auth\LoginController@showLoginAdmin')->name('auth');
	Route::post('/login', 'Auth\LoginController@prosesLoginAdmin')->name('prosesloginadmin');
	Route::get('/logout', 'Auth\LoginController@logoutAdmin')->name('doLogoutAdmin');
});
