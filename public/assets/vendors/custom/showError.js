function showErrors(data){
	var obj = data.responseJSON;
	$.each(obj, function(index, el){
		$('#group-'+index).addClass('has-error');
		$('#group-'+index).append('<span class="help-block with-errors">'+el+'</span>');
	})	
}