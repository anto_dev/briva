function notify(message, type = 'success', position = 'right'){
	$.notify({
		message: message 
	},
	{
		placement: {
			from: "top",
			align: "center"
		},
		delay : 1000,
		type: type,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		},
	});
}