<?php

use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Member::class,10)->create();

         DB::table('tb_member')->insert([
            'noregistrasi' => str_random(6),
            'nomember' => "dean12",
            'tanggalregistrasi' => rand(2019).'-'.rand(1,2).'-'.rand(1,30),
            'kodecabang' => '2019'
            'kodejenismember' => '1',
            'namamember' => str_random(8),
            'alamat' => str_random(8),
            'kota' => str_random(8),
            'kodepos' => rand(1,10),
            'telepon' => rand(1,10),
            'handphone' =>rand(1,10),
            'email' => strtolower(str_random(6)).'@gmail.com',
            'kodejenisidentitas' => rand(1,10),
            'noidentitas' => rand(1,10),
            'tempatlahir' => str_random(8),
            'tanggallahir' => rand(2019).'-'.rand(1,2).'-'.rand(1,30),
            'kewarganegaraan' => str_random(6),
            'kodeagama' => "1",
            'kodestatus' => '1',
            'jumlahanak' => '1',
            'kodejeniskelamin' => '1',
            'blokir' => "1",
            'poin' => '1',
            'internal' => rand(1,10),
            'eksternal' => rand(1,1000),
        ]);
    }
}
