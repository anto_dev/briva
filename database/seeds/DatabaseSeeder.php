<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         for ($i=0; $i <100 ; $i++) { 
         	DB::table('tb_member')->insert([
            'noregistrasi' => rand(1,6000),
            'nomember' => '2019'.rand(1,2).'OJK',
            'tanggalregistrasi' => '2019-'.rand(1,2).'-'.rand(1,30),
            'kodecabang' => '2010',
            'kodejenismember' => '1',
            'namamember' => str_random(8),
            'alamat' => str_random(8),
            'kota' => str_random(8),
            'kodepos' => rand(1,10),
            'telepon' => rand(1,10),
            'handphone' =>rand(1,10),
            'email' => strtolower(str_random(6)).'@gmail.com',
            'kodejenisidentitas' => rand(1,10),
            'noidentitas' => rand(1,10),
            'tempatlahir' => str_random(8),
            'tanggallahir' => '2019'.'-'.rand(1,2).'-'.rand(1,30),
            'kewarganegaraan' => str_random(6),
            'kodeagama' => "1",
            'kodestatus' => '1',
            'jumlahanak' => '1',
            'kodejeniskelamin' => '1',
            'blokir' => "1",
            'poin' => '1',
            'internal' => rand(1,10),
            'eksternal' => rand(1,1000),
        ]);
         }
    }
}
