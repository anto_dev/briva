<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'gagalLogin' => 'Username atau password salah.',
    'suksesLogin' => 'Anda berhasil login.',
    'tidakAktif' => 'Maaf, login anda tidak diaktifkan.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
];