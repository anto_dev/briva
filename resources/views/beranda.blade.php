<!DOCTYPE html>
<html lang="zxx">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="Orbitor,business,company,agency,modern,bootstrap4,tech,software">
  <meta name="author" content="">

  <title>Registrasi Mahasiswa Baru</title>
  
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('') }}images/logo.png" />
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{asset('')}}novena/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('_assetsLTE/font-awesome/css/font-awesome.min.css')}}">
  <!-- Icon Font Css -->
  <link rel="stylesheet" href="{{asset('')}}_assetsLTE/fonts/icofont/icofont.min.css">
  <!-- Slick Slider  CSS -->
  <link rel="stylesheet" href="{{ asset('') }}novena/css/main.css?210812">
  <link rel="stylesheet" href="{{ asset('') }}novena/css/main_baru.css?210813">
  <link rel="stylesheet" href="{{ asset('') }}novena/css/header_new.css?210922">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.1/dist/sweetalert2.all.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
  <!-- <link rel="stylesheet" href="https://pmb.uhnp.ac.id/spmbfront/assets/default/css/typeahead-bootstrap/typeaheadjs.css"> -->
  <!-- <link rel="stylesheet" href="https://pmb.uhnp.ac.id/spmbfront/assets/default/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="https://tracerstudy.uhn.ac.id/assets/frontend/css/select2.bundle.css">
  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Round" rel="stylesheet">
  <script src="{{ asset('') }}js/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{asset('')}}css/style.css">

</head>

<body id="top">

    <header>
        <div class="header-top-bar">
            <div class="container">
                <div class="row align-items-center">
                    <div><img src="{{ asset('') }}images/logo.png" width="80px" alt=""></div>
                    <div class="col-lg-6">
                        <div class="text-lg-left top-left-bar mt-2 mt-lg-0">
                            <span class="h4 text-white">Penerimaan Mahasiswa Baru</span><br>
                            <span class="h3 text-white">Universitas HKBP Nommensen Medan</span>
                            <ul class="top-bar-info list-inline-item pl-0 mb-0">
                                <li class="list-inline-item">Jl. Sutomo No.4A, Perintis, Kec. Medan Timur, Kota Medan, Sumatera Utara 20235 </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navigation" id="navbar">
            <div class="container">
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain" aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icofont-navigation-menu"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarmain">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ url('') }}/beranda">Beranda</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="about.html">Informasi</a></li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Akreditasi</a></li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="department.html" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fakultas <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown02">
                                <li><a class="dropdown-item" href="department.html">Sarjana</a></li>
                                <li><a class="dropdown-item" href="department-single.html">Magister</a></li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="doctor.html" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Doctors <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown03">
                                <li><a class="dropdown-item" href="doctor.html">Doctors</a></li>
                                <li><a class="dropdown-item" href="doctor-single.html">Doctor Single</a></li>
                                <li><a class="dropdown-item" href="appoinment.html">Appoinment</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('') }}/registrasi">Registrasi</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header><section class="mainpos-content">
        <div class="container">
            <img src="{{ asset('') }}images/pmb1.jpg" width="100%" alt="">
            <div class="row ">
                <div class="pos-main-content">
                    <!-- pos-content-body -->
                    <section class="pos-content-body col-md-9 form-page">

                        <div class="body-pmb">
                            <div class="row">
                                <div class="pos-content-header col-md-12" translate="no">
                                    <h2 class="main-header">Pendaftaran</h2>
                                    <p class="sub-header">Anda akan melakukan proses pendaftaran</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="pos-content-top01">
                                    <div class="col-md-12 text-center">
                                        <form role="form" id="form-daftar" method="post" class="f1">
                                            {{ csrf_field() }}
                                            <div class="text-left" style="color:#FEBD28; margin-top:8px;padding-left:0; margin-bottom: 12px">

                                            </div>

                                            <fieldset style="display: block">
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group" id="groupNIK">
                                                            <label for="nik">NIK / No. KTP <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="NIK / No. KTP hanya dapat digunakan 1 kali untuk pendaftaran pada periode ini">
                                                                info
                                                            </span></label> <input type="text" id="nik" name="NIK" class="form-control  required" maxlength="20" placeholder="Isi No. KTP Anda" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 20 karakter, Isian wajib diisi">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="groupNoHandphone">
                                                            <label for="nohp">No. HP <span class="text-danger">*</span></label> <input type="text" id="noTelp" name="NoHandphone" class="form-control  required" maxlength="20" placeholder="081234567890" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 20 karakter, Isian wajib diisi">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="groupNama">
                                                            <label for="namapendaftar">Nama Lengkap (Sesuai Ijazah)<span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Pastikan Nama Lengkap sesuai Ijazah Pendidikan Terakhir / Akte Kelahiran">
                                                                info
                                                            </span></label> <input type="text" id="Nama" name="Nama" class="form-control  required" maxlength="200" style="text-transform:uppercase" placeholder="Isi nama lengkap anda" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 200 karakter, Isian wajib diisi">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="groupFakultas">
                                                            <label for="email">Fakultas <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Isikan sesuai email yang anda gunakan. Semua Informasi terkait pendaftaran akan dikirimkan ke email yang Anda isikan">
                                                                info
                                                            </span></label>
                                                            <select name="Fakultas" id="faculty_id" class="form-control select2 setFakultas">
                                                            </select>
                                                        </div>
                                                    </div>
                                            
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="groupEmail">
                                                            <label for="email">Alamat Email <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Isikan sesuai email yang anda gunakan. Semua Informasi terkait pendaftaran akan dikirimkan ke email yang Anda isikan">
                                                                info
                                                            </span></label> <input type="text" id="email" name="Email" class="form-control  required" maxlength="60" placeholder="email@domain.com" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 60 karakter, Isian wajib diisi">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="groupProdi">
                                                            <label for="email">Program Studi <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Isikan sesuai email yang anda gunakan. Semua Informasi terkait pendaftaran akan dikirimkan ke email yang Anda isikan">
                                                                info
                                                            </span></label>
                                                            <select name="Prodi" id="prodi_id" class="form-control select2 setProdi">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="f1-buttons">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">

                                                        </div>
                                                        <div class="col-md-4 col-sm-4">
                                                            <button type="button" id="simpandata" class="btn btn-warning btn-next btn-block">DAFTAR <i id="iconsimpan"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                            <input type="hidden" name="key" id="key">
                                            <input type="hidden" name="act" id="act">
                                        </form>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /pos-content-body -->
                    <aside class="pos-content-right col-md-3">
                        <div class="side-banner text-center">
                            <div class="title-banner">
                                <p>Apakah Anda Telah Memiliki Akun Pendaftaran?</p>
                            </div>
                            <div class="button-banner">
                                <a href="{{ url('') }}/login"><button class="btn btn-primary btn-detail">Login Pendaftar</button></a>
                            </div>
                        <!-- <div class="subtitle-banner">
        <small>Masuk untuk melanjutkan proses pendaftaran hingga cetak kartu ujian</small>
    </div> -->
</div>
</aside>
</div>
</div>
</div>

</section>
<script>
    $(document).ready(function() {
        $("#faculty_id").select2({
            ajax: {
                url:"{{URL::to('')}}/faculty",
                dataType: "JSON",
                delay: 250,
                data: function(params) {
                    return {
                        NamaPanjang: params.term
                    };
                },
                processResults: function(data) {
                    var results = [];
                    $.each(data, function(index, item) {
                        results.push({
                            id: item.FakultasID,
                            text: item.NamaPanjang
                        })
                    });
                    // console.log(results)
                    return {
                        results: results
                    };

                },
                cache: true
            }
        });
        $("#faculty_id").change(function(e){
            var faculty=$(this).val();
            $("#prodi_id").val("")
            getProdi(faculty)
        })
        
        $("#simpandata").click(function(e){
            Swal.fire({
                title: 'Yakin ingin daftar?',
                showCancelButton: true,
                confirmButtonText: 'Ya'
            }).then((result) => {
              /* Read more about isConfirmed, isDenied below */
              if (result.isConfirmed) {
                $("#form-daftar").submit();
            }
        })
        })
        $("#simpanpendaftaran").click(function(e){
            $("#form-daftar").submit();
        })
        $("#form-daftar").submit(function(e){
            e.preventDefault();
            $.ajax({
                type: "POST",
                url:"{{URL::to('')}}/simpanpendaftaran",
                data: $("#form-daftar").serialize(),
                beforeSend : function(){
                    removeError()
                    $("#simpandata").addClass("disabled");
                    $("#iconsimpan").addClass("fa fa-spinner fa-spin");
                },
                complete : function(){
                    $("#simpandata").removeClass("disabled");
                    $("#iconsimpan").removeClass("fa fa-spinner fa-spin");
                },
                error : function(data){
                    showError(data.responseJSON);
                },
                success : function(response){
                    $('form')[0].reset();
                    Swal.fire(
                      'Simpan Data',
                      response,
                      'success'
                      )
                }
            });
        })
    })
    function getProdi(faculty){
        console.log(faculty)
        $("#prodi_id").select2({
            ajax: {
                url:"{{URL::to('')}}/prodi/"+faculty,
                dataType: "JSON",
                delay: 250,
                data: function(params) {
                    return {
                        NamaPanjang: params.term
                    };
                },
                processResults: function(data) {
                    var results = [];
                    $.each(data, function(index, item) {
                        results.push({
                            id: item.ProdiID,
                            text: item.Nama
                        })
                    });
                    console.log(results)
                    return {
                        results: results
                    };

                },
                cache: true
            }
        });
    }
    function showError(responseJSON){
        console.log(responseJSON)
        $.each(responseJSON, function(index, el){
            $('#group'+index).addClass('has-error');
            $.each(el, function(index1, el1){
                $('#group'+index).append('<span class="help-block with-errors">'+el1+'</span>');
            })
        })
    }
    function removeError(){
        $('body .has-error').removeClass('has-error');
        $('body .with-errors').remove();
    }
</script><!-- footer Start -->
<footer class="footer section gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mr-auto col-sm-6">
                <div class="widget mb-5 mb-lg-0">
                    <div class="logo mb-4">
                        <img src="{{ asset('') }}images/logo.png" width="50%" alt="" class="img-fluid">
                    </div>
                    <p class="text-justify">Universitas HKBP Nommensen adalah sebuah Universitas swasta di Kota Medan, Provinsi Sumatra Utara, Indonesia dengan Motto <b>World Class University</b>.</p>

                    <ul class="list-inline footer-socials mt-4">
                        <li class="list-inline-item"><a href="https://www.facebook.com/themefisher"><i class="icofont-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="https://twitter.com/themefisher"><i class="icofont-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="https://www.pinterest.com/themefisher/"><i class="icofont-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="widget mb-5 mb-lg-0">
                    <h4 class="text-capitalize mb-3">Department</h4>
                    <div class="divider mb-4"></div>

                    <ul class="list-unstyled footer-menu lh-35">
                        <li><a href="#">Surgery </a></li>
                        <li><a href="#">Wome's Health</a></li>
                        <li><a href="#">Radiology</a></li>
                        <li><a href="#">Cardioc</a></li>
                        <li><a href="#">Medicine</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="widget mb-5 mb-lg-0">
                    <h4 class="text-capitalize mb-3">Support</h4>
                    <div class="divider mb-4"></div>

                    <ul class="list-unstyled footer-menu lh-35">
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Company Support </a></li>
                        <li><a href="#">FAQuestions</a></li>
                        <li><a href="#">Company Licence</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget widget-contact mb-5 mb-lg-0">
                    <h4 class="text-capitalize mb-3">Get in Touch</h4>
                    <div class="divider mb-4"></div>

                    <div class="footer-contact-block mb-4">
                        <div class="icon d-flex align-items-center">
                            <i class="icofont-email mr-3"></i>
                            <span class="h6 mb-0">Support Available for 24/7</span>
                        </div>
                        <h4 class="mt-2"><a href="tel:+23-345-67890">Support@email.com</a></h4>
                    </div>

                    <div class="footer-contact-block">
                        <div class="icon d-flex align-items-center">
                            <i class="icofont-support mr-3"></i>
                            <span class="h6 mb-0">Mon to Fri : 08:30 - 18:00</span>
                        </div>
                        <h4 class="mt-2"><a href="tel:+23-345-67890">+23-456-6588</a></h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-btm py-4 mt-5">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6">
                    <div class="copyright">
                        &copy; Copyright Reserved to <span class="text-color">Novena</span> by <a href="https://themefisher.com/" target="_blank">Themefisher</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="subscribe-form text-lg-right mt-5 mt-lg-0">
                        <form action="#" class="subscribe">
                            <input type="text" class="form-control" placeholder="Your Email address">
                            <a href="#" class="btn btn-main-2 btn-round-full">Subscribe</a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <a class="backtop js-scroll-trigger" href="#top">
                        <i class="icofont-long-arrow-up"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>



<!-- 
    Essential Scripts
    =====================================-->



    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://tracerstudy.usu.ac.id/js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="https://tracerstudy.usu.ac.id/js/loadingoverlay.min.js"></script>
    <!-- <script language="JavaScript" type="text/javascript" src="https://tracerstudy.usu.ac.id/js/account.js"></script> -->

    <link rel="stylesheet" media="screen, print" href="https://tracerstudy.uhn.ac.id/assets/frontend/js/bootstrap-datepicker.js">


</body>

</html>