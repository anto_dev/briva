@extends('app')
@section('content')
<section class="mainpos-content">
    <div class="container">
        <img src="{{ asset('') }}images/pmb1.jpg" width="100%" alt="">
        <div class="pos-main-content">
            <div class="row">
                <!-- pos-content-body -->
                <section class="pos-content-body col-md-9 form-page">
                    <div class="body-pmb">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pos-content-header translate="no">
                                    <h2 class="main-header">Silahkan Login</h2>
                                </div>
                                <div class="pos-content-top01">
                                    <div class="text-center">
                                        <form role="form" id="form-login" method="post" action="{{route('loginpendaftaran')}}" class="f1">
                                            {{ csrf_field() }}
                                            <div class="form-group" id="groupusername">
                                                <label for="username">Email </label>
                                                <input type="text" id="username" name="username" class="form-control" title="" placeholder="Email akun pendaftaran" autocomplete="off">
                                                <input type="hidden" id="typeOf" name="typeOf" value="app">
                                            </div>
                                            <div class="form-group" id="grouppassword">
                                                <label for="password">Password </label>
                                                <input type="password" id="password" name="password" class="form-control" title="" placeholder="Password akun pendaftaran">
                                            </div>
                                            <button type="submit" id="simpandata" class="btn btn-warning btn-next btn-block">LOGIN
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /pos-content-body -->
                    <aside class="pos-content-right col-md-3">
                        <div class="side-banner text-center">
                            <div class="title-banner">
                                <p>Apakah Anda Belum Memiliki Akun Pendaftaran?</p>
                            </div>
                            <div class="button-banner">
                                <a href="{{ url('') }}/registrasi"><button class="btn btn-primary btn-detail">Daftar Sekarang</button></a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>

    </section>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#form-login").submit(function(e){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url:"{{route('loginpendaftaran')}}",
                    data: $("#form-login").serialize(),
                    beforeSend : function(){
                        removeError()
                        $("#simpandata").addClass("disabled");
                        $("#iconsimpan").addClass("fa fa-spinner fa-spin");
                    },
                    complete : function(){
                        $("#simpandata").removeClass("disabled");
                        $("#iconsimpan").removeClass("fa fa-spinner fa-spin");
                    },
                    error : function(response){
                        console.log(response)
                        if(response.status == 401){
                          Swal.fire({
                              icon: 'error',
                              title: response.responseJSON,
                              showConfirmButton: false
                          })
                      }else{
                        showError(data.responseJSON);
                    }
                },
                success : function(data){
                    $('form')[0].reset();
                    var status = data.code
                    var akses = data.akses
                    if(status == '1'){
                      Swal.fire({
                          icon: 'success',
                          title: 'Berhasil Login',
                          showConfirmButton: false,
                          timer: 1500
                      })
                      if(akses==0){
                        window.location.replace('{{URL::to(env('PREFIX_ADMIN').'/')}}/dashboard')
                    }else{
                        window.location.replace('{{URL::to(env('PREFIX_PMB').'/')}}/beranda')
                    }
                  }
              }
          });
            })
        })
        
        function showError(responseJSON){
            console.log(responseJSON)
            $.each(responseJSON, function(index, el){
                $('#group'+index).addClass('has-error');
                $.each(el, function(index1, el1){
                    $('#group'+index).append('<span class="help-block with-errors">'+el1+'</span>');
                })
            })
        }
        function removeError(){
            $('body .has-error').removeClass('has-error');
            $('body .with-errors').remove();
        }
        </script>
    
@endsection