@extends('app')
@section('content')
<section class="mainpos-content">
    <div class="container">
        <img src="{{ asset('') }}images/pmb1.jpg" width="100%" alt="">
        <div class="row ">
            <div class="pos-main-content">
                <!-- pos-content-body -->
                <section class="pos-content-body col-md-9 form-page">

                    <div class="body-pmb">
                        <div class="row">
                            <div class="pos-content-header col-md-12" translate="no">
                                <h2 class="main-header">Pendaftaran</h2>
                                <p class="sub-header">Anda akan melakukan proses pendaftaran</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="pos-content-top01">
                                <div class="col-md-12 text-center">
                                    <form role="form" id="form-daftar" method="post" class="f1">
                                        {{ csrf_field() }}
                                        <div class="text-left" style="color:#FEBD28; margin-top:8px;padding-left:0; margin-bottom: 12px">

                                        </div>

                                        <fieldset style="display: block">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group" id="groupNIK">
                                                        <label for="nik">NIK / No. KTP <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="NIK / No. KTP hanya dapat digunakan 1 kali untuk pendaftaran pada periode ini">
                                                            info
                                                        </span></label> <input type="text" id="nik" name="NIK" class="form-control  required" maxlength="20" placeholder="Isi No. KTP Anda" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 20 karakter, Isian wajib diisi">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" id="groupNoHandphone">
                                                        <label for="nohp">No. HP <span class="text-danger">*</span></label> <input type="text" id="noTelp" name="NoHandphone" class="form-control  required" maxlength="20" placeholder="081234567890" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 20 karakter, Isian wajib diisi">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" id="groupNama">
                                                        <label for="namapendaftar">Nama Lengkap (Sesuai Ijazah)<span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Pastikan Nama Lengkap sesuai Ijazah Pendidikan Terakhir / Akte Kelahiran">
                                                            info
                                                        </span></label> <input type="text" id="Nama" name="Nama" class="form-control  required" maxlength="200" style="text-transform:uppercase" placeholder="Isi nama lengkap anda" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 200 karakter, Isian wajib diisi">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" id="groupFakultas">
                                                        <label for="email">Fakultas <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Isikan sesuai email yang anda gunakan. Semua Informasi terkait pendaftaran akan dikirimkan ke email yang Anda isikan">
                                                            info
                                                        </span></label>
                                                        <select name="Fakultas" id="faculty_id" class="form-control select2 setFakultas">
                                                        </select>
                                                    </div>
                                                </div>
                                        
                                                <div class="col-md-6">
                                                    <div class="form-group" id="groupEmail">
                                                        <label for="email">Alamat Email <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Isikan sesuai email yang anda gunakan. Semua Informasi terkait pendaftaran akan dikirimkan ke email yang Anda isikan">
                                                            info
                                                        </span></label> <input type="text" id="email" name="Email" class="form-control  required" maxlength="60" placeholder="email@domain.com" data-toggle="tooltip" title="" data-original-title="Keterangan : isian maksimal 60 karakter, Isian wajib diisi">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" id="groupProdi">
                                                        <label for="email">Program Studi <span class="text-danger">*</span><span class="material-icons-round text-primary btn-icon-right" data-toggle="tooltip" data-placement="right" title="" data-original-title="Isikan sesuai email yang anda gunakan. Semua Informasi terkait pendaftaran akan dikirimkan ke email yang Anda isikan">
                                                            info
                                                        </span></label>
                                                        <select name="Prodi" id="prodi_id" class="form-control select2 setProdi">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="f1-buttons">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">

                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <button type="button" id="simpandata" class="btn btn-warning btn-next btn-block">DAFTAR <i id="iconsimpan"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                        <input type="hidden" name="key" id="key">
                                        <input type="hidden" name="act" id="act">
                                    </form>
                                </div>



                            </div>
                        </div>
                    </div>
                </section>
                <!-- /pos-content-body -->
                <aside class="pos-content-right col-md-3">
                    <div class="side-banner text-center">
                        <div class="title-banner">
                            <p>Apakah Anda Telah Memiliki Akun Pendaftaran?</p>
                        </div>
                        <div class="button-banner">
                            <a href="{{ url('') }}/login"><button class="btn btn-primary btn-detail">Login Pendaftar</button></a>
                        </div>
                    <!-- <div class="subtitle-banner">
    <small>Masuk untuk melanjutkan proses pendaftaran hingga cetak kartu ujian</small>
</div> -->
</div>
</aside>
</div>
</div>
</div>

</section>
<script>
$(document).ready(function() {
    $("#faculty_id").select2({
        ajax: {
            url:"{{URL::to('')}}/faculty",
            dataType: "JSON",
            delay: 250,
            data: function(params) {
                return {
                    NamaPanjang: params.term
                };
            },
            processResults: function(data) {
                var results = [];
                $.each(data, function(index, item) {
                    results.push({
                        id: item.FakultasID,
                        text: item.NamaPanjang
                    })
                });
                // console.log(results)
                return {
                    results: results
                };

            },
            cache: true
        }
    });
    $("#faculty_id").change(function(e){
        var faculty=$(this).val();
        $("#prodi_id").val("")
        getProdi(faculty)
    })
    
    $("#simpandata").click(function(e){
        Swal.fire({
            title: 'Yakin ingin daftar?',
            showCancelButton: true,
            confirmButtonText: 'Ya'
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            $("#form-daftar").submit();
        }
    })
    })
    $("#simpanpendaftaran").click(function(e){
        $("#form-daftar").submit();
    })
    $("#form-daftar").submit(function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url:"{{URL::to('')}}/simpanpendaftaran",
            data: $("#form-daftar").serialize(),
            beforeSend : function(){
                removeError()
                $("#simpandata").addClass("disabled");
                $("#iconsimpan").addClass("fa fa-spinner fa-spin");
            },
            complete : function(){
                $("#simpandata").removeClass("disabled");
                $("#iconsimpan").removeClass("fa fa-spinner fa-spin");
            },
            error : function(data){
                showError(data.responseJSON);
            },
            success : function(response){
                $('form')[0].reset();
                Swal.fire(
                  'Simpan Data',
                  response,
                  'success'
                  )
            }
        });
    })
})
function getProdi(faculty){
    console.log(faculty)
    $("#prodi_id").select2({
        ajax: {
            url:"{{URL::to('')}}/prodi/"+faculty,
            dataType: "JSON",
            delay: 250,
            data: function(params) {
                return {
                    NamaPanjang: params.term
                };
            },
            processResults: function(data) {
                var results = [];
                $.each(data, function(index, item) {
                    results.push({
                        id: item.ProdiID,
                        text: item.Nama
                    })
                });
                console.log(results)
                return {
                    results: results
                };

            },
            cache: true
        }
    });
}
function showError(responseJSON){
    console.log(responseJSON)
    $.each(responseJSON, function(index, el){
        $('#group'+index).addClass('has-error');
        $.each(el, function(index1, el1){
            $('#group'+index).append('<span class="help-block with-errors">'+el1+'</span>');
        })
    })
}
function removeError(){
    $('body .has-error').removeClass('has-error');
    $('body .with-errors').remove();
}
</script><!-- footer Start -->
@endsection
   