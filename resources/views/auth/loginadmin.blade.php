<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>NobleUI Responsive Bootstrap 4 Dashboard Template</title>
  <!-- core:css -->
  <link rel="stylesheet" href="../../../assets/vendors/core/core.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- end plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../../../assets/fonts/feather-font/css/iconfont.css">
  <link rel="stylesheet" href="../../../assets/vendors/flag-icon-css/css/flag-icon.min.css">
  <!-- endinject -->
  <!-- Layout styles -->  
  <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="../../../assets/images/favicon.png" />
  <link rel="stylesheet" href="../../../assets/vendors/sweetalert2/sweetalert2.min.css">
  <script src="../../../assets/vendors/sweetalert2/sweetalert2.min.js"></script>
  <style type="text/css">
    #overlay{
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 10;
      background-color: rgba(0,0,0,0.5); /*dim the background*/
    }
  </style>
</head>
<body class="sidebar-dark">
  <div class="main-wrapper">
    <div class="page-wrapper full-page bg-white">
      <div class="page-content d-flex align-items-center justify-content-center">

        <div class="row w-100 mx-0 auth-page">
          <div class="col-md-8 col-xl-6 mx-auto">
            {{-- <div class="card"> --}}
              <div class="row" style="background-color: #eaeaea !important;">
                <div id="overlay" style="display: none;">
                  <div class="w-100 d-flex justify-content-center align-items-center" style="position: fixed;top: 50%;left: 0%;">
                    <div class="spinner-grow text-primary" role="status">
                      <span class="sr-only">Loading...</span>
                    </div>
                    <img src="{{ asset('') }}assets/images/logo.png" width="100" style="padding: 10px;" alt="image">
                  </div>
                </div>
                <div class="col-md-4 pr-md-0 bg-primary">
                  <div class="auth-form-wrapper px-4 py-5">
                    <center>
                      {{-- <img src="{{ asset('assets/images/logo.jpg') }}" width="100"> --}}
                    </center>
                    <a href="#" class="noble-ui-logo d-block mb-2 text-white text-center"> BRI-VA <br> <span class="text-white">Universitas HKBP Nommensen Medan</span></a>

                  </div>
                </div>
                <div class="col-md-8 pl-md-0">
                  <div class="auth-form-wrapper px-4 py-5">
                    <a href="#" class="noble-ui-logo d-block mb-2">Sistem Management<span> BRI-VA</span></a>
                    <h5 class="text-muted font-weight-normal mb-4">Welcome back! Log in to your account.</h5>
                    <form action="{{URL::to(env('PREFIX_ADMIN').'/login')}}" method="post" id="frmLogin">
                      {{csrf_field()}}
                      <div class="form-group" id="group-username">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" class="form-control" placeholder="Username" name="username" id="username">
                      </div>
                      <div class="form-group" id="group-password">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                      </div>
                      {{-- <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                          <input type="checkbox" class="form-check-input">
                          Remember me
                        </label>
                      </div> --}}
                      <div class="mt-3">
                        <button type="submit" class="btn btn-primary mr-2 mb-2 mb-md-0 text-white btn-sm"> <i class="btn-icon-prepend" data-feather="log-in"></i> LOGIN</button>
                        {{-- <button type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                          <i class="btn-icon-prepend" data-feather="twitter"></i>
                          Login with twitter
                        </button> --}}
                      </div>
                      {{-- <a href="register.html" class="d-block mt-3 text-muted">Not a user? Sign up</a> --}}
                    </form>
                  </div>
                </div>
              </div>
            {{-- </div> --}}
          </div>
        </div>
        
      </div>
    </div>
  </div>
  
  <!-- core:js -->
  <script src="../../../assets/vendors/core/core.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <!-- end plugin js for this page -->
  <!-- inject:js -->
  <script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
  <script src="../../../assets/js/template.js"></script>
  <script src="{{asset('assets/vendors/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
  <script src="{{asset('assets/vendors/custom/loader.js')}}"></script>
  <script src="{{asset('assets/vendors/custom/notify.js')}}"></script>
  <script src="{{asset('assets/vendors/custom/showErrors.js')}}"></script>
  <!-- endinject -->
  <!-- custom js for this page -->
  <!-- end custom js for this page -->
  <script>
    $(document).ready(function(){
      $('#frmLogin').submit(function(e){
        e.preventDefault();
        var loaderId = 'loader'
        $.ajax({
          url : $('#frmLogin').attr('action'),
          data : $('#frmLogin').serialize(),
          type : 'POST',
          beforeSend : function(){
            $("#overlay").show();
          },
          complete : function(){
            $("#overlay").hide();
          },
          success : function(data){
            console.log(data)
            var status = data.code
            var akses = data.akses
            if(status == '1'){
              // notify('Anda berhasil login', 'success','center')
              Swal.fire(
                'Berhasil Login',
                'Sedang mengalihkan...',
                'success'
                )
              window.location.replace('{{URL::to(env('PREFIX_ADMIN').'/')}}/dashboard')
              
            }
          },
          error : function(data){
            if(data.status == 401){
              // notify(data.responseText, 'danger','center')
              Swal.fire(
                'Kesalahan',
                data.responseText,
                'error'
                )
              // window.location.replace('{{URL::to(env('PREFIX_ADMIN'))}}/login') 
            }else{
              showErrors(data);
            }
          }
        })
      })
    })
    function showErrors(data){
      var obj = data.responseJSON;
      $.each(obj, function(index, el){
        $('#group-'+index).addClass('has-error');
        $('#group-'+index).append('<code class="help-block with-errors">'+el+'</code>');
      })  
    }
    function removeError(){
      $('body .has-error').removeClass('has-error');
      $('body .with-errors').remove();
    }
  </script>
</body>
</html>