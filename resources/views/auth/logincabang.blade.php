<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{isset(LayoutBackend::getSetting()->AppTitle) ? LayoutBackend::getSetting()->AppTitle : ""}} | Log In</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('_assetsLTE/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('_assetsLTE/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('_assetsLTE/ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('_assetsLTE/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/iCheck/square/blue.css')}}">
</head>
<body class="hold-transition login-page">
  <div class="login-box" style="margin:auto !important;padding-top:5%">
    <div class="login-logo">
      <b>{{isset(LayoutBackend::getSetting()->AppTitle) ? LayoutBackend::getSetting()->AppTitle : ""}}</b>
    </div>
    <div class="login-box-body" style="width: 342px !important">
      <p class="login-box-msg">Enter username &amp; password </p>
      <form action="{{URL::to(env('PREFIX_CABANG').'/login')}}" method="post" id="frmLogin">
        {{csrf_field()}}
        <div class="form-group has-feedback" id="group-username">
          <input type="text" class="form-control" placeholder="Username" name="username" id="username">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback" id="group-password">
          <input type="password" class="form-control" placeholder="Password" name="password" id="password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        {{-- @if(LayoutBackend::getSetting()->UseRecaptcha == 'Y')
        <div class="form-group has-feedback" id="group-g-recaptcha-response">
          {!! Recaptcha::render() !!}
        </div>
        @endif --}}
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                &nbsp;
              </label>
            </div>
          </div>
          <div class="col-xs-4">
            <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script src="{{asset('_assetsLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/plugins/iCheck/icheck.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/custom/loader.js')}}"></script>
  <script src="{{asset('_assetsLTE/custom/notify.js')}}"></script>
  <script src="{{asset('_assetsLTE/custom/showErrors.js')}}"></script>
  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'   
      });

      $('#frmLogin').submit(function(e){
        e.preventDefault();
        var loaderId = 'loader'
        $.ajax({
          url : $('#frmLogin').attr('action'),
          data : $('#frmLogin').serialize(),
          type : 'POST',
          beforeSend : function(){
            showLoader(loaderId,'.login-box-body');
            $('body .has-error').removeClass('has-error');
            $('body .with-errors').remove();
          },
          complete : function(){
            removeLoader(loaderId)
          },
          success : function(data){
            console.log(data)
            var status = data.code
            if(status == '1'){
              notify('Anda berhasil login', 'success','center')
              window.location.replace('{{URL::to(env('PREFIX_CABANG').'/cabangberanda')}}')
            }
          },
          error : function(data){
            if(data.status == 401){
              notify(data.responseText, 'error','center')
            }else{
              showErrors(data);
            }
          }
        })
      })
    });
  </script>
</body>
</html>
