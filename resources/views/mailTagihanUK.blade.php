@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title" colspan="2">
				Pendaftaran Mahasiswa Baru UHN Medan
			</td>
		</tr>
		<tr>
			<td width="100%" height="10" colspan="2"></td>
		</tr>
		<tr>
			<td class="paragraph" colspan="2">
				Terimakasih sudah melakukan pendaftaran di Universitas HKBP Nommensen Medan, berikut ini informasi pendaftaran anda :
			</td>
		</tr>
		<tr>
			<td width="100%" height="25" colspan="2"></td>
		</tr>
		<tr>
			<td width="40%">Nama</td><td> : {{ \Session::get('nama') }}</td>
		</tr>
		<tr>
			<td width="40%">Program Studi</td><td> : {{ \Session::get('prodi') }}</td>
		</tr>
		<tr>
			<td width="40%">No Briva (Kode Pembayaran) </td><td>: {{ \Session::get('kodebriva') }}</td>
		</tr>
		<tr>
			<td width="40%">Nominal </td><td> : Rp. {{ number_format(\Session::get('nominal'),0,',','.') }}</td>
		</tr>
		<tr>
			<td width="100%" height="25" colspan="2"></td>
		</tr>
		<tr>
			<td width="100%" height="25" colspan="2"></td>
		</tr>
		<tr>
			<td width="80%" align="center" colspan="2">
				<img src="https://uhn.ac.id/media/2201181449_WhatsApp Image 2022-01-18 at 2.26.49 PM.jpeg">
			</td>
		</tr>
		<tr>
			<td width="100%" height="25" colspan="2"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop