<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HO SZY :: </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="{{asset('')}}/{{LayoutBackend::getSetting()->favicon}}">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('_assetsLTE/css/print.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/dist/css/skins/_all-skins.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/datepicker/datepicker3.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/toastr/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('_assetsLTE/select2/css/select2.min.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('_assetsLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  
  <script src="{{asset('_assetsLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/js/recta.js')}}"></script>
  <script src="{{asset('_assetsLTE/js/JsBarcode.all.min.js')}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{asset('_assetsLTE/plugins/jQueryUI/jquery-ui.min.js')}}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{{asset('_assetsLTE/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/js/jquery.PrintArea.js')}}"></script>
  <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
  <!-- Sparkline -->
  <script src="{{asset('_assetsLTE/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jvectormap -->
  <script src="{{asset('_assetsLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{asset('_assetsLTE/plugins/knob/jquery.knob.js')}}"></script>
  <!-- daterangepicker -->
  <script src="{{asset('_assetsLTE/plugins/moment/moment.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/plugins/daterangepicker/daterangepicker.js')}}"></script>
  <!-- datepicker -->
  <script src="{{asset('_assetsLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
  <!-- datepicker -->
  <script src="{{asset('_assetsLTE/plugins/datepicker/locales/bootstrap-datepicker.id.js')}}"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{asset('_assetsLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
  <!-- Slimscroll -->
  <script src="{{asset('_assetsLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- FastClick -->
  <script src="{{asset('_assetsLTE/plugins/fastclick/fastclick.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('_assetsLTE/plugins/toastr/toastr.min.js')}}"></script>
  <script src="{{asset('_assetsLTE/bootcomplete/js/jquery.bootcomplete.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('_assetsLTE/dist/js/app.min.js')}}"></script>
  <!-- InputMask -->
<script src="{{asset('_assetsLTE/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('_assetsLTE/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('_assetsLTE/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="{{asset('_assetsLTE/select2/js/select2.min.js')}}"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
 
</head>
<body class="hold-transition sidebar-mini fixed skin-blue">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">HO</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{ HO SZY }</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            
            {{-- @include('layout.task') --}}
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{asset('_assetsLTE/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                <span class="hidden-xs"></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{asset('_assetsLTE/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                  <p>
                    
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="" id="ajaxlink" class="btn btn-default btn-flat">Update Password</a>
                  </div>
                  <div class="pull-right">
                    <a href="" class="btn btn-default btn-flat" id="btnLogout">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="width: 100% !important;">
      
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.7
      </div>
      <strong>Copyright &copy; 2019.
    </footer>

    <div class="control-sidebar-bg"></div>
  </div>

  <script>
    var token = '{{csrf_token()}}'
  </script>
  @include('layouts.backend.js.customscripts')
  @stack('scripts')
</body>
</html>


