<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>NobleUI Responsive Bootstrap 4 Dashboard Template</title>
  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<!-- core:css -->
	<link rel="stylesheet" href="{{ asset('assets/vendors/core/core.css') }}">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
	<!-- Font Awesome -->
	{{-- <link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}"> --}}
	<!-- endinject -->
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('assets/vendors/datepicker/datepicker3.css')}}">
	<!-- Layout styles -->  
	<link rel="stylesheet" href="{{ asset('assets/css/demo_1/style.css') }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{asset('assets/vendors/daterangepicker/daterangepicker.css')}}">
	<!-- Toastr -->
	<link rel="stylesheet" href="{{asset('assets/vendors/toastr/toastr.min.css')}}">
	<link rel="stylesheet" href="{{ asset('') }}assets/css/bootstrap-multiselect.css" type="text/css"/>
	<!-- End layout styles -->
	<link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" />
	<!-- {{-- <script src="{{asset('assets/js/jquery-2.2.3.min.js')}}"></script> --}} -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	<!-- <script src="https://code.jquery.com/jquery-migrate-3.3.2.js"></script> -->
	
	<!-- core:js -->
	<script src="{{ asset('assets/vendors/core/core.js') }}"></script>
	<!-- endinject -->
	<!-- daterangepicker -->
	<script src="{{asset('assets/vendors/moment/moment.min.js')}}"></script>
	<script src="{{asset('assets/vendors/daterangepicker/daterangepicker.js')}}"></script>
	<!-- datepicker -->
	{{-- <script src="{{asset('assets/vendors/datepicker/bootstrap-datepicker.js')}}"></script>
	<!-- datepicker -->
	<script src="{{asset('assets/vendors/datepicker/locales/bootstrap-datepicker.id.js')}}"></script> --}}
	<!-- Toastr -->
	<script src="{{asset('assets/vendors/toastr/toastr.min.js')}}"></script>
	<!-- InputMask -->
	<script src="{{asset('assets/vendors/input-mask/jquery.inputmask.js')}}"></script>
	<script src="{{asset('assets/vendors/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
	<script src="{{asset('assets/vendors/input-mask/jquery.inputmask.extensions.js')}}"></script>
	<!-- plugin js for this page -->
	<script src="{{ asset('assets/vendors/chartjs/Chart.min.js') }}"></script>
	<script src="{{ asset('assets/vendors/jquery.flot/jquery.flot.js') }}"></script>
	<script src="{{ asset('assets/vendors/jquery.flot/jquery.flot.resize.js') }}"></script>
	<script src="{{ asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
	{{-- <script src="{{ asset('assets/vendors/apexcharts/apexcharts.min.js') }}"></script> --}}
	{{-- <script src="../assets/vendors/progressbar.js/progressbar.min.js"></script> --}}
	<!-- end plugin js for this page -->
	<!-- inject:js -->
	<script src="{{ asset('assets/vendors/feather-icons/feather.min.js') }}"></script>
	<script src="{{ asset('assets/js/template.js') }}"></script>
	<!-- endinject -->
	<!-- custom js for this page -->
	<script src="{{ asset('assets/js/dashboard.js') }}"></script>
	<script src="{{ asset('assets/js/datepicker.js') }}"></script>

	<script src="{{ asset('assets/charts/highcharts.js') }}"></script>
	<script src="{{ asset('assets/charts/series-label.js') }}"></script>
	<script src="{{ asset('assets/charts/exporting.js') }}"></script>
	<script src="{{ asset('assets/charts/export-data.js') }}"></script>
	<script type="text/javascript" src="{{ asset('') }}assets/js/bootstrap-multiselect.js"></script>
	<style>

		#overlay{
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: 10;
			background-color: rgba(0,0,0,0.5); /*dim the background*/
		}
		.highcharts-figure,
		.highcharts-data-table table {
			min-width: 310px;
			max-width: 100%;
			margin: 1em auto;
		}

		.highcharts-data-table table {
			font-family: Verdana, sans-serif;
			border-collapse: collapse;
			border: 1px solid #ebebeb;
			margin: 10px auto;
			text-align: center;
			width: 100%;
			max-width: 500px;
		}

		.highcharts-data-table caption {
			padding: 1em 0;
			font-size: 1.2em;
			color: #555;
		}

		.highcharts-data-table th {
			font-weight: 600;
			padding: 0.5em;
		}

		.highcharts-data-table td,
		.highcharts-data-table th,
		.highcharts-data-table caption {
			padding: 0.5em;
		}

		.highcharts-data-table thead tr,
		.highcharts-data-table tr:nth-child(even) {
			background: #f8f8f8;
		}

		.highcharts-data-table tr:hover {
			background: #f1f7ff;
		}
	</style>
</head>
<body class="sidebar-dark">
	<div class="main-wrapper">
		<nav class="sidebar">
			<div class="sidebar-header">
				<a href="#" class="sidebar-brand">
					Noble<span>UI</span>
				</a>
				<div class="sidebar-toggler">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="sidebar-body">
				<ul class="nav">
					<li class="nav-item nav-category">Main</li>
					
					@include('layouts.backend.content.menusidebar')
				</ul>
			</div>
		</nav>		
		<div class="page-wrapper">
			<nav class="navbar">
				<a href="#" class="sidebar-toggler">
					<i data-feather="menu"></i>
				</a>
				<div class="navbar-content">
					<form class="search-form">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i data-feather="search"></i>
								</div>
							</div>
							<input type="text" class="form-control" id="navbarForm" placeholder="Search here...">
						</div>
					</form>
					<ul class="navbar-nav">
						<li class="nav-item dropdown nav-profile">
							<a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="{{ asset('assets/images') }}/{{Auth::guard('Admin')->user()->avatar}}" alt="userr">
							</a>
							<div class="dropdown-menu" aria-labelledby="profileDropdown">
								<div class="dropdown-header d-flex flex-column align-items-center">
									<div class="figure mb-3">
										<img src="{{ asset('assets/images') }}/{{Auth::guard('Admin')->user()->avatar}}" alt="">
									</div>
									<div class="info text-center">
										<p class="name font-weight-bold mb-0">{{Auth::guard('Admin')->user()->username}}</p>
										<p class="email text-muted mb-3">amiahburton@gmail.com</p>
									</div>
								</div>
								<div class="dropdown-body">
									<ul class="profile-nav p-0 pt-3">
										<li class="nav-item">
											<a href="pages/general/profile.html" class="nav-link">
												<i data-feather="user"></i>
												<span>Profile</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="javascript:;" class="nav-link">
												<i data-feather="edit"></i>
												<span>Edit Profile</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="javascript:;" class="nav-link">
												<i data-feather="repeat"></i>
												<span>Switch User</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="#" id="btnLogout" class="nav-link">
												<i data-feather="log-out"></i>
												<span>Log Out</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</nav>
			<div id="overlay" style="display: none;">
				<div class="w-100 d-flex justify-content-center align-items-center" style="position: fixed;top: 50%;left: 0%;">
					<div class="spinner-grow text-primary" role="status">
						<span class="sr-only">Loading...</span>
					</div>
					<img src="{{ asset('') }}assets/images/logo.png" width="100" style="padding: 10px;" alt="image">
				</div>
			</div>
			<div class="page-content" id="konten">
				{!! $konten !!}				
			</div>
			<footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
				<p class="text-muted text-center text-md-left"></p>
				<p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Copyright © 2020 <a href="https://www.nobleui.com" target="_blank">NobleUI</a>. All rights reserved <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
			</footer>
			<!-- partial -->

		</div>
	</div>
	<script>
		var token = '{{csrf_token()}}';
	</script>
	<script type="text/javascript">
		$(document).on('click', '#subscribe', function(event) {
			event.preventDefault();
		});
	</script>


	@include('layouts.backend.js.customscripts')


	<script>
		var isFluid = JSON.parse(localStorage.getItem('isFluid'));
		if (isFluid) {
			var container = document.querySelector('[data-layout]');
			container.classList.remove('container');
			container.classList.add('container-fluid');
		}
	</script>

</script>
<script type="text/javascript">

	$(document).ready(function(){

	})
</script>
</body>
</html>    