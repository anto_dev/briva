
<!-- style pagination -->
<style>
	.pagination li .nomor{
		width: 40px;
		text-align: center;
	}
</style>
<!-- / style pagination -->
<script>
// label untuk javascript dari language
var lbl = [];
var lblType = [];
var lblaktif = [];
var lbladjust = [];
var token = '{{csrf_token()}}';

lbl['0'] = '{{Lang::get("globals.0")}}'; 
lbl['1'] = '{{Lang::get("globals.1")}}';
lblaktif['0'] = '{{Lang::get("globals.N")}}'; 
lblaktif['1'] = '{{Lang::get("globals.Y")}}'; 
lbladjust['0'] = '{{Lang::get("globals.adj0")}}'; 
lbladjust['1'] = '{{Lang::get("globals.adj1")}}'; 
lbl['Y'] = '{{Lang::get("globals.Y")}}'; 
lbl['N'] = '{{Lang::get("globals.N")}}';  
lblType['0'] = '{{Lang::get("globals.Type0")}}'; 
lblType['1'] = '{{Lang::get("globals.Type1")}}'; 
var globalLink = '';
var globalType=0
var sortname='';
var simpan = {primary :'{!! Lang::get("globals.simpanYLabel") !!}',default :'{!! Lang::get("globals.simpanNLabel") !!}'};

var edit = {warning :'{!! Lang::get("globals.editYLabel") !!}',default :'{!! Lang::get("globals.editNLabel") !!}'};

var hapus = {danger :'{!! Lang::get("globals.hapusYLabel") !!}',default :'{!! Lang::get("globals.hapusNLabel") !!}'};

var banned = {danger :'Blokir',default :'{!! Lang::get("globals.bannedNLabel") !!}'};
var unbanned = {danger :'Buka Blokir',default :'{!! Lang::get("globals.bannedNLabel") !!}'};

var hapusDipilih = {danger : '{!! Lang::get('globals.hapusSemuaYLabel') !!}', default : '{!! Lang::get('globals.hapusSemuaNLabel') !!}'};

var periode='';
var d = new Date();
var m = d.getMonth();
var bulan = d.getMonth()+1;
var tgl=d.getDate();
// $('body #periode').daterangepicker({
	
// 	startDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
// 	endDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
// 	locale: {
// 		format: 'DD-MM-YYYY'
// 	}
// })
// periode= $('body #periode').val();

// localStorage.setItem("lastname", periode);
function appendLeadingZeroes(n){
	if(n <= 9){
		return "0" + n;
	}
	return n
}
// function setPeriode(){
// 	if (typeof periode === 'undefined') {
// 		let current_datetime = new Date()
// 		let formatted_date = appendLeadingZeroes(current_datetime.getDate()) + "-" + appendLeadingZeroes(current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear() 
// 		periode='01-'+appendLeadingZeroes(current_datetime.getMonth() + 1)+'-'+current_datetime.getFullYear()+' - '+formatted_date;
// 		console.log(periode)
// 	}
// 	$('body #periode').daterangepicker({
// 		startDate: periode.substring(0,10),
// 		endDate: periode.substring(13,23),
// 		locale: {
// 			format: 'DD-MM-YYYY'
// 		}
// 	})
// }
// Restricts input for the given textbox to the given inputFilter.
function filterNumeric(textbox, inputFilter) {
	["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
		textbox.addEventListener(event, function() {
			if (inputFilter(this.value)) {
				this.oldValue = this.value;
				this.oldSelectionStart = this.selectionStart;
				this.oldSelectionEnd = this.selectionEnd;
			} else if (this.hasOwnProperty("oldValue")) {
				this.value = this.oldValue;
				this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			}
		});
	});
}
function normCurr(data){
	return data.replace(/[.*+?^${}()|[\]\\]/g, "");
}
const formatter = new Intl.NumberFormat(['ban', 'id'], {
      // style: 'currency',
      // currency: 'IDR',
      // minimumFractionDigits: 2
  })
// $('[data-mask]').inputmask();

$('#btnLogout').click(function(e){
	e.preventDefault();
	konfirmasi('{{Lang::get('globals.logoutMessage')}}','{!! Lang::get('globals.logoutTitle') !!}',{ primary : 'Ya',default : 'Tidak'},'doLogout' , $(this));
})
function doLogout(){
	$.ajax({
		url : '{{url('')}}/{{env('PREFIX_ADMIN')}}/logout',
		success : function(){
			toastr['success']('Anda berhasil logout')
			window.location.reload();
		},
		error : function(response){
			toastr['error'](response.responseText);	
		}
	})
}
$('body').on('click','#ajaxlink',function(e){
	e.preventDefault();
	var link = $(this).attr('href');
	globalLink = link;
	$.ajax({
		url : $(this).attr('href'),
		beforeSend : function(){
			callOverlay('box-primary');
		},
		success : function(data){
			$('#konten').html(data.konten);
			$('title').html(data.title);
			history.pushState(data.konten, data.title, link);
			// refreshTask();

			$('body .datepicker').datepicker({
				format : 'yyyy-mm-dd',
				language : 'id',
				autoclose : true
			});
			removeOverlay();

		},
		error :function(data){
			toastr['error'](data.responseText);
			removeOverlay();
		}
	})
	$('body #ajaxlink').parent().removeClass('active');
	$(this).parent().addClass('active');
})		
function callOverlay(targetId = 'box'){
	// $('#' + targetId).append('<div class="overlay">Loading</div>');
	$("#overlay").show();
}
function removeOverlay(){
	// $('body .overlay').remove();
	$("#overlay").hide();
}
function loadPagination(targetClass, totalPage, activePage, totalRecord){
	totalRecord = parseInt(totalRecord);
	totalPage  	= parseInt(totalPage);
	activePage  = parseInt(activePage);

	var listPageHtml = '';
	
	if(activePage == 1){
		listPageHtml += '<li class="paginate_button page-item previous disabled"><a id="no-session" class="page-link">First</a></li>'
		listPageHtml += '<li class="paginate_button page-item previous disabled"><a id="no-session" class="page-link">Previous</a></li>'
	}else{
		listPageHtml += '<li class="paginate_button page-item"><a id="no-session" class="page-link" href="javascript:muatData(1)">First</a></li>'
		listPageHtml += '<li class="paginate_button page-item"><a id="no-session" class="page-link" href="javascript:muatData('+(activePage - 1)+')">Previous</a></li>'
	}
	if(activePage - 3 <= 0){
		if(totalPage >= 7){
			var y = 7;
		}else{
			y = totalPage;
		}
		for(var x = 1 ; x <= y ; x++){
			if(x == activePage){
				listPageHtml += '<li class="active paginate_button page-item"><a class="page-link" id="no-session" class="nomor">'+x+'</a></li>'
			}
			else{
				listPageHtml += '<li class="paginate_button page-item"><a class="page-link" id="no-session" href="javascript:muatData('+x+')" class="nomor">'+x+'</a></li>'
			}
		}
	}
	else if(activePage + 3 >= totalPage){
		for(var x = totalPage - 6 ; x <= totalPage ; x++){

			if(x > 0){
				if(x == activePage){
					listPageHtml += '<li class="active paginate_button page-item"><a class="page-link" id="no-session" class="nomor">'+x+'</a></li>'
				}
				else{
					listPageHtml += '<li class="paginate_button page-item"><a class="page-link" id="no-session" href="javascript:muatData('+x+')" class="nomor">'+x+'</a></li>'
				}
			}
		}
	}
	else{
		if(activePage+3 <= totalPage ){
			var y = activePage + 3;
		}else{
			var y = totalPage;
		}
		for(var x = activePage - 3 ; x <= y; x++){
			if(x > 0){
				if(x == activePage){
					listPageHtml += '<li class="active paginate_button page-item"><a class="page-link" id="no-session" class="nomor">'+x+'</a></li>'
				}
				else{
					listPageHtml += '<li class="paginate_button page-item"><a class="page-link" id="no-session" href="javascript:muatData('+x+')" class="nomor">'+x+'</a></li>'
				}
			}
		}
	}
	if(activePage == totalPage){
		listPageHtml += '<li class="disabled paginate_button page-item"><a class="page-link" id="no-session">Next</a></li>'
		listPageHtml += '<li class="disabled paginate_button page-item"><a class="page-link" id="no-session">Last</a></li>'
	}else{
		listPageHtml += '<li class="paginate_button page-item"><a class="page-link" id="no-session" href="javascript:muatData('+(parseInt(activePage + 1))+')">Next</a></li>'
		listPageHtml += '<li class="paginate_button page-item"><a class="page-link" id="no-session" href="javascript:muatData('+totalPage+')">Last</a></li>'
	}
	$('.'+targetClass).html(listPageHtml);
	$('body #totalRecord').html((totalRecord));
}
window.addEventListener('popstate', function(e) {
	e.preventDefault();	
	window.location.reload();
});
window.addEventListener('pushstate', function(e) {
	e.preventDefault();	
	window.location.reload();
});
function removeError(){
	$('body .has-error').removeClass('has-error');
	$('body .with-errors').remove();
}
function showError(responseJSON){
	console.log(responseJSON)
	$.each(responseJSON, function(index, el){
		$('#group'+index).addClass('has-error');
		$.each(el, function(index1, el1){
			$('#group'+index).append('<code class="help-block with-errors">'+el1+'</code>');
		})
	})
}
var confirm = false;
var objek ;

function confirmDelete(){
	var page = objek.data('page');
	var id = objek.data('id');
	var to = objek.data('url'); 
	$.ajax({
		url : to+"/"+id,
		success : function(response){
			// nextAction();
			muatData()
			toastr['success']('Data berhasil dihapus')
		},
		error : function(response){
			toastr['error'](response.responseText);
		}
	})
}
function confirmDeleteAll(){
	var to = objek.data('url');
	var page = 1;
	var id = [];
	$('#checkRow:checked').each(function(index,el){
		$.ajax({
			url : to+"/"+$(this).data('id'),
			success : function(response){
				toastr['success']('Data berhasil dihapus');
				if(index == $('#checkRow:checked').length -1){
					muatData(page)
				}
			},
			error : function(response){
				toastr['error'](response.responseText);
			}
		})

	});
}
function konfirmasi(message = '', title = '', button, onOk, obj , allow = 'primary', dismiss = 'default', nextaction = 'next'){
	
	objek = obj;
	console.log(objek.data('id'));
	$('#modal-box .modal-body').html(message);
	$('#modal-box .modal-title').html(title);
	var buttonHtml = '';
	$.each(button,function(index,el){
		if(index == dismiss)
			buttonHtml += '<button type="button" class="btn btn-'+index+'" data-dismiss="modal">'+el+'</button>';
		else if(index == allow){
			buttonHtml += '<a id="no-session" type="button" class="btn btn-'+index+'" onclick="'+onOk+'();$(\'#modal-box\').modal(\'hide\');">'+el+'</a>';
		}
	})
	$('#modal-box .modal-footer').html(buttonHtml);

	$('#modal-box').modal('show');
}
function confirmSubmit(){

	$.ajax({
		url : objek.attr('action'),
		type : 'POST',
		data : objek.serialize(),
		beforeSend : function(){
			removeError();
			callOverlay('box-primary');
		},
		complete : function(){
			removeOverlay();
		},
		error : function(data){
			console.log(data)
			if(data.status == 409){
				toastr['error'](data.responseText);
			}else if(data.status == 500){
				toastr['error'](data.responseText);
			}
			else{
				showError(data.responseJSON);
			}
		},
		success : function(data){
			console.log(data)
			toastr['success']('Data {{Lang::get('globals.notifSuksesSimpan')}}');
			nextAction(data);
		}
	})
}
$('body').on('change','#checkAll',function(e){
	e.preventDefault();
	$('tbody #checkRow').not(this).prop('checked', this.checked);
});
$('body').on('change','#checkAllLulus',function(e){
	e.preventDefault();
	$('tbody #cekLulus').not(this).prop('checked', this.checked);
});
$('body').on('change','#checkAllTagihan',function(e){
	e.preventDefault();
	$('tbody #checkTagihan').not(this).prop('checked', this.checked);
	var Subtotal=0;
	$('#checkTagihan:checked').each(function(index,el){
		Subtotal+=$(this).data('nominal');
	});
	var lblSubtotal=formatter.format(Subtotal);
	$("#Subtotal").text(lblSubtotal);
	$("#Terbilang").text(terbilang(Subtotal).replace(/  +/g, ' '));
});
$('body').on('change','#checkTagihan',function(e){
	var Subtotal=0;
	e.preventDefault();
	$('#checkTagihan:checked').each(function(index,el){
		Subtotal+=$(this).data('nominal');
	});
	var lblSubtotal=formatter.format(Subtotal);
	$("#Subtotal").text(lblSubtotal);
	$("#Terbilang").text(terbilang(Subtotal).replace(/  +/g, ' '));
});
$('body').on('click','#hapusCheckedkData',function(e){
	e.preventDefault();
	konfirmasi('{{Lang::get('globals.hapusDataDipilihLabel')}}','{!! Lang::get('globals.hapusDipilihTitle') !!}',hapusDipilih,'confirmDeleteAll' , $(this),'danger');
})
$('body').on('click','#hapusData',function(e){
	e.preventDefault();
	konfirmasi('{{Lang::get('globals.hapusDataLabel')}}','{!! Lang::get('globals.hapusTitle') !!}', hapus ,'confirmDelete' , $(this),'danger');
})
// refresh task yang ada di navigasi atas
// function refreshTask(){
// 	$.ajax({
// 		url : '{{url(env('PREFIX_ADMIN').'/'.'beranda/ambiltask')}}',
// 		success : function(response){
// 			if(response.length > 0){
// 				var listMenu ="";
// 				$.each(response,function(index,el){
// 					listMenu += '<li><a id="no-session"><h3>' + el.namaModul + ' : '+el.waktu+' <small class="pull-right"><button class="btn btn-xs btn-danger" data-id="'+el.id+'"  id="btn-delete-task"><i class="fa fa-trash"></i></button> </small> </h3> </a> </li>';
// 				})
// 				$('#task #list-menu').html(listMenu);
// 				$('#label-task-count').css('display','');
// 				$('#label-task-count').html(response.length);
// 				$('#task #dropdown-menu').css('display','');
// 			}
// 			else{
// 				$('#label-task-count').html('');
// 				$('#task #dropdown-menu').css('display','none');
// 			}
// 		}
// 	})
// }

//ketika button delete di task di klik

// $('#task').on('click','#btn-delete-task', function(e){
// 	e.stopPropagation();
// 	var idSession = $(this).data('id');
// 	var btnDeleteTask = $(this);
// 	$.ajax({
// 		url : '{{url(env('PREFIX_ADMIN').'/beranda/hapustask')}}',
// 		data : {
// 			id : idSession
// 		},
// 		success : function(response){
// 			btnDeleteTask.parent().parent().parent().parent().remove();
// 			refreshTask();
// 			toastr['success']('{{Lang::get('globals.deletedTask')}}');
// 		},
// 		error : function(response){
// 			toastr['error'](response.responseText);
// 		}
// 	})
// })


//ketika button refresh di task di klik
// $('#btn-refresh-task').click(function(e){
// 	e.stopPropagation();
// 	refreshTask();
// })
// var parameterGlobal = null;
// var modalGlobal = null;
// //action browseData
// function browseData(parameter){
// 	var modal = $('#modal-browse');
// 	modal.find('.modal-title').html(parameter.title);
// 	var headerTable = "";
// 	$.each(parameter['headerTable'],function(index,el){
// 		headerTable += '<th>'+el+'</th>'; 
// 	})
// 	headerTable += '<th width="10%">Action</th>';
// 	modal.find('thead').find('tr').html(headerTable);
// 	var filterLabel = '<option value="">{{Lang::get('globals.filterSearch')}}</option>';
// 	$.each(parameter['searchFieldLabel'],function(index,el){
// 		filterLabel += '<option value="'+parameter['searchField'][index]+'">'+el+'</option>'; 
// 	});
// 	modal.find('#browse-filter').html(filterLabel);
// 	parameterGlobal = parameter;
// 	modalGlobal = modal;
// 	fetchBrowseData()
// 	// modal.modal('show');
// }

// var dataToReturn = [];
// var returnValue = [];

// function fetchBrowseData(page = 1){
// 	var ret = [];
// 	var modal = modalGlobal;
// 	var parameter = parameterGlobal;
// 	var callback = parameter['callback'];
// 	var filter = modal.find('#browse-filter').val();
// 	var data = modal.find('#browse-data').val();
// 	var take = modal.find('#browse-take').val();
// 	$.ajax({
// 		url : parameter['url'],
// 		type : 'POST',
// 		data : {
// 			_token : token,
// 			filter : filter,
// 			data : data,
// 			page : page,
// 			take : take
// 		},
// 		beforeSend : function(){
// 			callOverlay('box-browse');
// 		},
// 		complete : function(){
// 			removeOverlay();
// 		},
// 		success : function(response){
// 			var activePage = response.activePage;
// 			var totalPage = response.totalPage;
// 			var totalRecord = response.totalRecord;

// 			var tbodyHtml = '';
// 			$.each(response.data, function(index, el){
// 				tbodyHtml += '<tr>'
// 				$.each(parameter['fieldTable'],function(index1,el1){
// 					tbodyHtml += '<td>'+el[el1]+'</td>';
// 				})
// 				dataToReturn[el.id] = [];
// 				$.each(parameter['return'],function(index1,el1){
// 					dataToReturn[el.id][el1] = el[el1];
// 				})
// 				tbodyHtml += '<td><button class="btn btn-primary btn-sm" onclick="returnBrowse('+el.id+','+callback+')"><i class="fa fa-check"></i> Pilih</button></td>';
// 				tbodyHtml += '</tr>' 
// 			})
// 			loadPaginationBrowse('paginationBrowse',totalPage, activePage, totalRecord)
// 			modal.find('table').find('#data').html(tbodyHtml);
// 			if(response.data.length == 0){
// 				modal.find('table').find('#data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
// 			}
// 		},
// 		error : function(data){
// 			toastr['error'](data.statusText);
// 		}
// 	})
// }
// function returnBrowse(ret,callback){
// 	returnValue = dataToReturn[ret];
// 	$('#modal-browse').modal('hide');
// 	callback();
// }
function readSession(){
	if(globalLink == ""){
		globalLink = window.location.href;
	}
	var url = globalLink;
	globalLink = "";
	url = url.split('=');
	var session = url[1];
	// console.log(session)
	if(url[1]){
		console.log($.parseJSON(atob(session)))
		return $.parseJSON(atob(session));
	}
	return false;
}
// function checkBack(){
// 	if(globalLink == ""){
// 		globalLink = window.location.href;
// 	}
// 	var url = globalLink;
// 	url = url.split('&');
// 	var session = url[1];
// 	// console.log(session);
// 	if(url[1]){
// 		return true;
// 	}
// 	return false;
// }

function createSession(modul,page,take,filter,data){
	var id = "";
	var objek = {};
	objek.page = page;
	objek.take = take;
	objek.filter = filter;
	objek.data = data;
	// console.log(objek);
	var session = JSON.stringify(objek);
	session = btoa(session);
	// console.log(session);
	history.pushState(data.konten, data.title, modul+"?session="+session);
	$('body a:not(#no-session)').each(function(index,el){
		var kelas = $(this).attr('class');
		var ada = false;
		if(kelas){
			kelas = kelas.split(' ');
			$.each(kelas,function(index,el){
				if(el == 'no-session'){
					ada = true;
				}
			})
		}
		if(ada == false){
			$(this).attr('href',$(this).attr('href') + "?session="+session)
		}
	})	
}
// $('body .datepicker').datepicker({
// 	format : 'yyyy-mm-dd',
// 	language : 'id',
// 	autoclose : true
// });

</script>
<div class="modal fade" id="modal-browse">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body box" id="box-browse">
				<div class="row">
					<span class="col-md-4">
						<select name="filter" id="browse-filter" class="form-control">
						</select>
					</span>
					<span class="col-md-8">
						<div class="input-group">
							<input class="form-control" type="text" id="browse-data" placeholder="{{Lang::get('globals.placeHolderSearch')}}"></input>
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" onclick="fetchBrowseData()"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</span>
				</div>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>

						</tr>
					</thead>
					<tbody id="data">
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-md-5">
						<table width="100%">
							<tr>
								<td width="40%">
									{{Lang::get('globals.labelJumlahTampil')}}: 
								</td>
								<td width="20%">
									<select id="browse-take" class="form-control input-sm" onchange="fetchBrowseData()">
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="20">20</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
								</td>
								<td>
									&nbsp; Total : <span id="totalRecord"></span> Data
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-7">
						<div class="pull-right">
							<ul class="pagination pagination-browse" style="margin: 0px">
							</ul>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-box">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>
