<!DOCTYPE html>
<html lang="zxx">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="Orbitor,business,company,agency,modern,bootstrap4,tech,software">
  <meta name="author" content="">

  <title>Login Mahasiswa Baru</title>
  
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('') }}images/logo.png" />
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{asset('')}}novena/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('_assetsLTE/font-awesome/css/font-awesome.min.css')}}">
  <!-- Icon Font Css -->
  <link rel="stylesheet" href="{{asset('')}}_assetsLTE/fonts/icofont/icofont.min.css">
  <!-- Slick Slider  CSS -->
  <link rel="stylesheet" href="{{ asset('') }}novena/css/main.css?210812">
  <link rel="stylesheet" href="{{ asset('') }}novena/css/main_baru.css?210813">
  <link rel="stylesheet" href="{{ asset('') }}novena/css/header_new.css?210922">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.1/dist/sweetalert2.all.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
  <!-- <link rel="stylesheet" href="https://pmb.uhnp.ac.id/spmbfront/assets/default/css/typeahead-bootstrap/typeaheadjs.css"> -->
  <!-- <link rel="stylesheet" href="https://pmb.uhnp.ac.id/spmbfront/assets/default/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="https://tracerstudy.uhn.ac.id/assets/frontend/css/select2.bundle.css">
  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Round" rel="stylesheet">
  <script src="{{ asset('') }}js/jquery-3.6.0.min.js"></script>
  

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{asset('')}}css/style.css">

</head>


<body id="top">

    <header>
        <div class="header-top-bar">
            <div class="container">
                <div class="row align-items-center">
                    <div><img src="{{ asset('') }}images/logo.png" width="80px" alt=""></div>
                    <div class="col-lg-6">
                        <div class="text-lg-left top-left-bar mt-2 mt-lg-0">
                            <span class="h4 text-white">Penerimaan Mahasiswa Baru</span><br>
                            <span class="h3 text-white">Universitas HKBP Nommensen Medan</span>
                            <ul class="top-bar-info list-inline-item pl-0 mb-0">
                                <li class="list-inline-item">Jl. Sutomo No.4A, Perintis, Kec. Medan Timur, Kota Medan, Sumatera Utara 20235 </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navigation" id="navbar">
            <div class="container">
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain" aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icofont-navigation-menu"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarmain">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="http://localhost/pmb/user">Beranda</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('') }}/informasi">Informasi</a></li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Akreditasi</a></li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="department.html" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fakultas <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown02">
                                <li><a class="dropdown-item" href="department.html">Sarjana</a></li>
                                <li><a class="dropdown-item" href="department-single.html">Magister</a></li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="doctor.html" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Doctors <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown03">
                                <li><a class="dropdown-item" href="doctor.html">Doctors</a></li>
                                <li><a class="dropdown-item" href="doctor-single.html">Doctor Single</a></li>
                                <li><a class="dropdown-item" href="appoinment.html">Appoinment</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="http://localhost/pmb/user/registrasi">Registrasi</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    