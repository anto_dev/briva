<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Orbitor,business,company,agency,modern,bootstrap4,tech,software">
    <meta name="author" content="">
    
    <title>Aktivasi Akun Pendaftaran</title>
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('') }}images/logo.png" />
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{asset('')}}novena/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('_assetsLTE/font-awesome/css/font-awesome.min.css')}}">
    <!-- Icon Font Css -->
    <link rel="stylesheet" href="{{asset('')}}_assetsLTE/fonts/icofont/icofont.min.css">
    <!-- Slick Slider  CSS -->
    <link rel="stylesheet" href="{{ asset('') }}novena/css/main.css?210812">
    <link rel="stylesheet" href="{{ asset('') }}novena/css/main_baru.css?210813">
    <link rel="stylesheet" href="{{ asset('') }}novena/css/header_new.css?210922">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.1/dist/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <!-- <link rel="stylesheet" href="https://pmb.uhnp.ac.id/spmbfront/assets/default/css/typeahead-bootstrap/typeaheadjs.css"> -->
    <!-- <link rel="stylesheet" href="https://pmb.uhnp.ac.id/spmbfront/assets/default/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://tracerstudy.uhn.ac.id/assets/frontend/css/select2.bundle.css">
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Round" rel="stylesheet">
    <script src="{{ asset('') }}js/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
    
    
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{asset('')}}css/style.css">
    
</head>

<body id="top">
    
    <header>
        <div class="header-top-bar">
            <div class="container">
                <div class="row align-items-center">
                    <div><img src="{{ asset('') }}images/logo.png" width="80px" alt=""></div>
                    <div class="col-lg-6">
                        <div class="text-lg-left top-left-bar mt-2 mt-lg-0">
                            <span class="h4 text-white">Penerimaan Mahasiswa Baru</span><br>
                            <span class="h3 text-white">Universitas HKBP Nommensen Medan</span>
                            <ul class="top-bar-info list-inline-item pl-0 mb-0">
                                <li class="list-inline-item">Jl. Sutomo No.4A, Perintis, Kec. Medan Timur, Kota Medan, Sumatera Utara 20235 </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navigation" id="navbar">
            <div class="container">
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain" aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icofont-navigation-menu"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarmain">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="http://localhost/pmb/user">Beranda</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="about.html">Informasi</a></li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Akreditasi</a></li>
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="department.html" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fakultas <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown02">
                                <li><a class="dropdown-item" href="department.html">Sarjana</a></li>
                                <li><a class="dropdown-item" href="department-single.html">Magister</a></li>
                            </ul>
                        </li>
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="doctor.html" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Doctors <i class="icofont-thin-down"></i></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown03">
                                <li><a class="dropdown-item" href="doctor.html">Doctors</a></li>
                                <li><a class="dropdown-item" href="doctor-single.html">Doctor Single</a></li>
                                <li><a class="dropdown-item" href="appoinment.html">Appoinment</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="http://localhost/pmb/user/registrasi">Registrasi</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header><section class="mainpos-content">
        <div class="container">
            <img src="{{ asset('') }}images/pmb1.jpg" width="100%" alt="">
            <div class="pos-main-content">
                <div class="row">
                    <!-- pos-content-body -->
                    <section class="pos-content-body col-md-9 form-page">
                        <div class="body-pmb">
                            <div class="row">
                                <div class="pos-content-header col-md-12" translate="no">
                                    @if($status=='Y')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pos-content-header" translate="no">
                                                <h2 class="main-header">Masukkan Password Akun Anda</h2>
                                            </div>
                                            <div class="pos-content-top01">
                                                <div class="text-center">
                                                    <form role="form" id="form-aktivasi" method="post" action="{{route('aktivasiakun')}}" class="f1">
                                                        {{ csrf_field() }}
                                                        <div class="form-group" id="grouppassword">
                                                            <label for="Password">Password </label>
                                                            <input type="password" id="Password" name="password" class="form-control" title="" placeholder="Password" autocomplete="off">
                                                            <input type="hidden" id="id" name="id" class="form-control" title="" placeholder="id" value="{{ $id }}" autocomplete="off">
                                                        </div>
                                                        <div class="form-group" id="grouppassword_confirmation">
                                                            <label for="KonfirmasiPassword">Konfirmasi Password </label>
                                                            <input type="password" id="KonfirmasiPassword" name="password_confirmation" class="form-control" title="" placeholder="Konfirmasi Password akun pendaftaran">
                                                        </div>
                                                        <button type="submit" id="simpandata" class="btn btn-warning btn-next btn-block"><i id="iconsimpan"></i> SIMPAN</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <h2 class="main-header">{{ $data }}</h2>
                                    @endif
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <!-- /pos-content-body -->
                    <aside class="pos-content-right col-md-3">
                        <div class="side-banner text-center">
                            <div class="title-banner">
                                <p>Apakah Anda Belum Memiliki Akun Pendaftaran?</p>
                            </div>
                            <div class="button-banner">
                                <a href="{{ url('') }}/registrasi"><button class="btn btn-primary btn-detail">Daftar Sekarang</button></a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        
    </section>
    <footer class="footer section gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mr-auto col-sm-6">
                    <div class="widget mb-5 mb-lg-0">
                        <div class="logo mb-4">
                            <img src="{{ asset('') }}images/logo.png" width="50%" alt="" class="img-fluid">
                        </div>
                        <p class="text-justify">Universitas HKBP Nommensen adalah sebuah Universitas swasta di Kota Medan, Provinsi Sumatra Utara, Indonesia dengan Motto <b>World Class University</b>.</p>
                        
                        <ul class="list-inline footer-socials mt-4">
                            <li class="list-inline-item"><a href="https://www.facebook.com/themefisher"><i class="icofont-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/themefisher"><i class="icofont-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="https://www.pinterest.com/themefisher/"><i class="icofont-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget mb-5 mb-lg-0">
                        <h4 class="text-capitalize mb-3">Department</h4>
                        <div class="divider mb-4"></div>
                        
                        <ul class="list-unstyled footer-menu lh-35">
                            <li><a href="#">Surgery </a></li>
                            <li><a href="#">Wome's Health</a></li>
                            <li><a href="#">Radiology</a></li>
                            <li><a href="#">Cardioc</a></li>
                            <li><a href="#">Medicine</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget mb-5 mb-lg-0">
                        <h4 class="text-capitalize mb-3">Support</h4>
                        <div class="divider mb-4"></div>
                        
                        <ul class="list-unstyled footer-menu lh-35">
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Company Support </a></li>
                            <li><a href="#">FAQuestions</a></li>
                            <li><a href="#">Company Licence</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget widget-contact mb-5 mb-lg-0">
                        <h4 class="text-capitalize mb-3">Get in Touch</h4>
                        <div class="divider mb-4"></div>
                        
                        <div class="footer-contact-block mb-4">
                            <div class="icon d-flex align-items-center">
                                <i class="icofont-email mr-3"></i>
                                <span class="h6 mb-0">Support Available for 24/7</span>
                            </div>
                            <h4 class="mt-2"><a href="tel:+23-345-67890">Support@email.com</a></h4>
                        </div>
                        
                        <div class="footer-contact-block">
                            <div class="icon d-flex align-items-center">
                                <i class="icofont-support mr-3"></i>
                                <span class="h6 mb-0">Mon to Fri : 08:30 - 18:00</span>
                            </div>
                            <h4 class="mt-2"><a href="tel:+23-345-67890">+23-456-6588</a></h4>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="footer-btm py-4 mt-5">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6">
                        <div class="copyright">
                            &copy; Copyright Reserved to <span class="text-color">Novena</span> by <a href="https://themefisher.com/" target="_blank">Themefisher</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscribe-form text-lg-right mt-5 mt-lg-0">
                            <form action="#" class="subscribe">
                                <input type="text" class="form-control" placeholder="Your Email address">
                                <a href="#" class="btn btn-main-2 btn-round-full">Subscribe</a>
                            </form>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-4">
                        <a class="backtop js-scroll-trigger" href="#top">
                            <i class="icofont-long-arrow-up"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    
    
    <!-- 
        Essential Scripts
        =====================================-->
        
        
        
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
        <script src="{{asset('_assetsLTE/custom/notify.js')}}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#form-aktivasi").submit(function(e){
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url:"{{route('aktivasiakun')}}",
                        data: $("#form-aktivasi").serialize(),
                        beforeSend : function(){
                            removeError()
                            $("#simpandata").addClass("disabled");
                            $("#iconsimpan").addClass("fa fa-spinner fa-spin");
                        },
                        complete : function(){
                            $("#simpandata").removeClass("disabled");
                            $("#iconsimpan").removeClass("fa fa-spinner fa-spin");
                        },
                        error : function(response){
                            console.log(response)
                            Swal.fire({
                                icon: 'error',
                                title: response.responseText,
                                showConfirmButton: false
                            })
                            showError(response.responseJSON);
                        },
                        success : function(data){
                            $('form')[0].reset();
                            Swal.fire({
                                icon: 'success',
                                title: data,
                                showConfirmButton: false,
                                timer: 1500
                            })
                            window.location.replace("{{route('loginapp')}}");
                        }
                    });
                })
            })
            
            function showError(responseJSON){
                console.log(responseJSON)
                $.each(responseJSON, function(index, el){
                    $('#group'+index).addClass('has-error');
                    $.each(el, function(index1, el1){
                        $('#group'+index).append('<span class="help-block with-errors">'+el1+'</span>');
                    })
                })
            }
            function removeError(){
                $('body .has-error').removeClass('has-error');
                $('body .with-errors').remove();
            }
        </script>
        
        
    </body>
    
    </html>