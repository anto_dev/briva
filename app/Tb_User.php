<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Izin;
class Tb_User extends Model
{
    protected $table = 'users';

    static function izinMasuk($izin){
        
    	if(Tb_User::cekIzin($izin)){
    		return true;
    	}
    	return false;
    }

    static function cekIzin($izin){
        $izin = Izin::where('grupUser',Auth::guard('Admin')->user()->grupUser)
        ->where('namaIzin', $izin)
        ->first();
        if(!empty($izin)){
    		return true;
        }
    	return false;
    }

    static function getJudul($izin){
        $izin = Izin::where('grupUser',Auth::guard('Admin')->user()->grupUser)
        ->where('namaIzin', $izin)
        ->first();

        if(!empty($izin)){
            return $izin->judulHalaman;
        }
        return false;
    }
}
