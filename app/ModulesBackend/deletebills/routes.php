<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'deletebills', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','deletebillscontroller@tambahData')->name('lihatdeletebills');
	Route::post('/','deletebillscontroller@muatData')->name('lihatdeletebills');
	Route::get('/tambah','deletebillscontroller@tambahData')->name('tambahdeletebills');
	Route::post('/tambah','deletebillscontroller@simpanData')->name('tambahdeletebills');
	Route::get('/hapus/{id}','deletebillscontroller@hapusData')->name('hapusdeletebills');
	Route::get('/edit/{id}','deletebillscontroller@editData')->name('editdeletebills');
	Route::post('/edit','deletebillscontroller@updateData')->name('editdeletebills');
});
});