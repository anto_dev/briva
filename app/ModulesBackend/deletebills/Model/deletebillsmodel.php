<?php
namespace App\ModulesBackend\deletebills\Model;

use Illuminate\Database\Eloquent\Model;

class deletebillsmodel extends Model
{
	protected $table = 'briva';
	public $timestamps = false;
}