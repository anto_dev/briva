<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="row grid-margin stretch-card">
		<div class="card">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<form action="{{route('tambahdeletebills')}}" class="form-vertical" method="post" id="frmTambahdeletebills">
				{{csrf_field()}}
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" id="groupNPM">
								<label for="NPM" class="control-label">N P M :</label>
								<input type="text" class="form-control input-sm" id="NPM" name="NPM" autocomplete="off">
							</div>
							
						</div>
					</div>
				</div>
				<div class="card-footer" align="right">
					<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatdeletebills')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
					<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
				</div>
			</form>
		</div>
	</div>
</section>
<script>
	$(document).ready(function(){
		
	})
	$('#frmTambahdeletebills').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('form')[0].reset();
		setPeriodeY();
	}
</script>