<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="row grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<form method="POST" action="" name="frmDeleteBills" id="frmDeleteBills">
				<div class="card-body">
					<div class="row">               
						<div class="col-md-12">
							<div class="form-group" id="groupdata">
								<label>Kode Briva</label>
								<div class="input-group col-6">
									<input class="form-control" id="data" name="data" autocomplete="off"></input>
									<span class="input-group-btn">
										<button class="btn btn-success form-control" type="submit" onclick="muatData()">
											Delete
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<div class="col-md-4">

					</div>
					<div class="col-md-8">

					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<style>
	input[type='checkbox']{
		appearance: none;
	}
</style>
<script>
	$(document).ready(function(){
		$(".data").hide();
		$('body #data').keypress(function (e) {
			var key = e.which;
 if(key == 13)  // the enter key code
 {
 	muatData();
 }
});
	});
	function muatData(page = 1){
		var data = $('#data').val();
		$.ajax({
			url : '{{url(env('PREFIX_ADMIN'))}}/deletebills',
			type : 'POST',
			data : {
				_token : token,
				data : data
			},
			beforeSend : function(){
				callOverlay('box-table');
			},
			complete : function(){
				removeOverlay();
			},
			success : function(response){
				console.log(response)
				toastr['success'](response.responseDescription);
			},
			error : function(data){
				toastr['error'](data.responseText);
			}
		})
	}
</script>