<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\deletebills\Model\deletebillsModel;
use App\Custom\CustomClass;
class deletebillscontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('deletebills::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		$rules = [
			'data' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response('Kesalahan! kode briva tidak valid', 400);
		}
		$data = $request->input('data');
		$response=Helper::q_briva_cek($data);
		$status=$response['status'];
		if ($status) {
			return response()->json(['data' => $response],200);
		}else{
			return response()->json($response['errDesc'],409);
		}
	}
	function tambahData(Request $request){
		$konten = view("deletebills::tambah")->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'NPM' => 'required|min:6'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$NPM=$request->input('NPM');
		$response=Helper::q_briva_delete($NPM);
		if ($response['status']) {
			try {
				DB::table('log_delete_bills')->insert([
					'reference_id'=>$NPM,
					'payload'=>json_encode($response),
					'created_ip'=>$request->ip(),
					'created_by'=>Auth::guard('Admin')->user()->username
				]);
				$message=$response['responseDescription'];
				$code=200;
			} catch (\Throwable $e) {
				$message='Kesalahan server';
				$code=400;
			}
		}else{
			$message=$response['errDesc'];
			$code=409;
		}
		return response($message, $code);
	}
}
