<?php 
namespace App\ModulesBackend;
use DB;
/**
* ServiceProvider
*
* The service provider for the modules. After being registered
* it will make sure that each of the modules are properly loaded
* i.e. with their routes, views etc.
*
* @author Kamran Ahmed <kamranahmed.se@gmail.com>
* @package App\Modules
*/
class ModulesBackendServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Will make sure that the required modules have been fully loaded
     * @return void
     */
    public function boot()
    {
        // For each of the registered modules, include their routes and Views
        $modules=DB::table('tb_modul')->where('status',1)
        ->whereIn('tb_modul.akses',['0','2','1'])->get();

        // while (list(,$module) = each($modules)) {
        foreach ($modules as $key => $module) {             

            // Load the routes for each of the modules
            if(file_exists(__DIR__.'/'.$module->folder.'/routes.php')) {
                include __DIR__.'/'.$module->folder.'/routes.php';
            }

            if(file_exists(__DIR__.'/'.$module->folder.'/Controller/'.$module->folder.'controller.php')) {
                include __DIR__.'/'.$module->folder.'/Controller/'.$module->folder.'controller.php';
            }

            if(file_exists(__DIR__.'/'.$module->folder.'/Model/'.$module->folder.'model.php')) {
                include __DIR__.'/'.$module->folder.'/Model/'.$module->folder.'model.php';
            }

            if(is_dir(__DIR__.'/'.$module->folder.'/Views')) {
                $this->loadViewsFrom(__DIR__.'/'.$module->folder.'/Views', $module->folder);
            }
        }
    }

    public function register() {}

}