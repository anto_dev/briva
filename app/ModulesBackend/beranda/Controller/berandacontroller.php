<?php

// namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class berandacontroller extends Controller
{

    public function index(Request $request)
    {
        $data=DB::table('statements')->select("SELECT COUNT(a.sequence),SUM(a.credit) FROM statements a WHERE YEAR(a.trx_date)='2022'");
        $konten=view("beranda::view",['data'=>$data])->render();
        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }   
        return view('layouts.backend.app',['konten'=>$konten]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    function ambiltask(){
        $idUser = Auth::guard('admin')->user()->id;
        $task = DB::table('tb_lock')
        ->join('tb_modul','tb_modul.id','=','tb_lock.idModul')
        ->where('tb_lock.idUser', $idUser)
        ->where('tb_lock.status',1)
        ->select('tb_lock.id','tb_modul.namaModul','tb_lock.waktu','tb_lock.url')
        ->get();
        foreach ($task as $key => $value) {
            $task[$key]->waktu = date('d/m/Y H:i:s',strtotime($value->waktu));
        }
        return response()->json($task);
    }
    function hapustask(Request $request){
        $id = $request->input('id');
        $idUser = Auth::guard('admin')->user()->id;
        $task = DB::table('tb_lock')
        ->where('idUser', $idUser)
        ->where('id',$id);

        if(Setting::getSetting()->DeleteLock == 'Y'){
            $task->delete();
        }
        else{
            $task->update([
                'status' => 0
            ]);
        }
    }
    function importExcel(Request $request){

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if($data->count() > 0)
        {
            // dd($data);
          foreach($data->toArray() as $key => $value)
          {
            // dd($value['no']);
            $sku=$value['sku'];
            $prefix=sprintf('4%06d',$sku);
            DB::table('tb_baranghadiah_fix_idempiere')->insert([
                'kodebaranghadiah'=>$prefix,
                'name'=>$value['name']
            ]);
        }

    }
    return redirect()->route('lihatberanda')->with('status', 'Success Update!');
}
}
