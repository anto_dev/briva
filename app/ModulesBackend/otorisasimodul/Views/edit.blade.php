<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
				<form action="{{url(env('PREFIX_ADMIN').'/otorisasimodul/edit')}}" class="form-vertical" method="post" id="frmEditOtorisasiModul">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
					{{csrf_field()}}
					<input type="hidden" name="grupUser" value="{{$data[0]->grupUser}}" id="grupUser">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group" id="groupGrupUser">
										<label for="GrupUser" class="control-label">Grup User :</label>
										<input type="text" class="form-control input-sm" disabled="" value="{{$data[0]->namaGrupUser}}">					
										<div class="checkbox">
											<label>
												<input type="checkbox" value="" id="checkAll">
												Cek semua
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<table class="table table-bordered table-striped table-condensed">
										<thead>
											<tr>
												<th>
													Menu
												</th>
												<th>
													Folder
												</th>
												@foreach($dataJenisOtorisasi as $itemJenisOtorisasi)
												<th width="10%">
													{{$itemJenisOtorisasi->namaIzin}}
												</th>
												@endforeach
									{{-- <th width="10%">
										Data
									</th> --}}
								</tr>
							</thead>
							<tbody>
								@foreach($dataModul as $key => $itemModul)
								<tr>
									<td>
										{{$itemModul->namaModul}}
									</td>
									<td>
										<span class="label label-success">{{$itemModul->folder}}</span> 
									</td>
									@foreach($dataJenisOtorisasi as $key1 => $itemJenisOtorisasi)
									<td>
										<input type="hidden" name="idModul[{{$key}}]" value="{{$itemModul->id}}">
										<input type="hidden" name="idIzin[{{$key}}][{{$key1}}]" value="{{$itemJenisOtorisasi->id}}">
										<input type="hidden" name="namaIzin[{{$key}}][{{$key1}}]" value="{{$itemJenisOtorisasi->inisial}}{{$itemModul->folder}}">
										<input type="hidden" name="judulHalaman[{{$key}}][{{$key1}}]" value="{{$itemModul->namaModul}}">
										<center>
											<input type="checkbox" name="autorisasi[{{$key}}][{{$key1}}]" 
											@if(isset($autorisasi[$itemModul->id][$itemJenisOtorisasi->id])) checked @endif>
										</center>	
									</td>
									@endforeach
									{{-- <td> --}}
										{{-- <button type="button" class="btn btn-success btn-sm" id="btnModalOtorisasiData" data-modul="{{$itemModul->id}}"><i class="fa fa-cogs"></i> Set</button> --}}
										<div id="holder-{{$itemModul->id}}">
											<?php 
											$htmlFilter = [];
											if(isset($filter[$itemModul->id])){
												$htmlFilter = json_decode($filter[$itemModul->id]);
												if(!empty($htmlFilter)){

													foreach ($htmlFilter as $keyf => $valuef) {
														if($valuef == ""){
															$data = "";
														}
														else{
															$data = implode(";", $valuef);
														}
														?>
														<input type="hidden" value="{{$keyf}}" name="filterOnTable[{{$itemModul->id}}][]">
														<input type="hidden" value="{{$data}}" name="dataOnTable[{{$itemModul->id}}][]">
														<?php
													}	
												}
											}
											?>
										</div>
									{{-- </td> --}}
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="card-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{url(env('PREFIX_ADMIN').'/otorisasimodul')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
	</form>
</div>
</div>
</section>
<script>
	$(document).ready(function(){
		$('#checkAll').click(function () {    
		$('input:checkbox').prop('checked', this.checked);    
	});
	$('#frmEditOtorisasiModul').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	})
	function nextAction(data){
		window.location.replace('{{route('lihatotorisasimodul')}}');
	}
	
</script>