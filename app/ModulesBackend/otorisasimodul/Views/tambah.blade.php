<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
				<form action="{{route('tambahotorisasimodul')}}" class="form-vertical" method="post" id="frmTambahOtorisasiModul">
					{{csrf_field()}}
							<div class="row">
								<div class="col-md-3">
									<div class="form-group" id="groupGrupUser">
										<label for="GrupUser" class="control-label">Grup User :</label>
										<select name="GrupUser" id="GrupUser" class="form-control input-sm">
											<option value="0">== Pilih grup user ==</option>
											@foreach($dataGrupUser as $itemGrupUser)
											<option value="{{$itemGrupUser->id}}">{{$itemGrupUser->namaGrupUser}}</option>
											@endforeach
										</select>
										<div class="checkbox">
											<label>
												<input type="checkbox" value="" id="checkAll">
												Cek semua
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<table class="table table-bordered table-condensed table-striped">
										<thead>
											<tr>
												<th>
													Menu
												</th>
												<th>
													Folder
												</th>
												@foreach($dataJenisOtorisasi as $itemJenisOtorisasi)
												<th width="10%">
													{{$itemJenisOtorisasi->namaIzin}}
												</th>
												@endforeach
									{{-- <th width="10%">
										Data
									</th> --}}
								</tr>
							</thead>
							<tbody id="konten-modul">
								<tr>
									<td colspan="7">Pilih dahulu grup user</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="card-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{url(env('PREFIX_ADMIN').'/otorisasimodul')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
	</form>
</div>
</div>
</section>

<div class="modal fade" id="modal-otorisasi-data">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Setting Otorsasi Data</h4>
			</div>
			<div class="modal-body" id="filter-holder">
				<div class="row">
					<div class="col-md-4">
						<input class="form-control" placeholder="Filter" id="filter" name="filter[]"></input>
					</div>
					<div class="col-md-7">
						<input class="form-control" placeholder="Data (pisah dengan titik koma)" id="data" name="data[]"></input>
					</div>
					<div class="col-md-1">
						<button class="btn btn-success" id="btn-add-filter"><i class="fa fa-plus"></i></button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="simpan-otorisasi-data"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$("#GrupUser").change(function(){
			var grupUser=$(this).val();
			if(grupUser==0){
				toastr['error']('Grup user tidak valid');
				$("#konten-modul").html('<tr><td colspan="7">Pilih grup user dahulu</td></tr>');
				return false;
			}
			$.ajax({
				url:"{{URL::to('')}}/{{env('PREFIX_ADMIN')}}/otorisasimodul/getmodulotorisasi/"+grupUser,
				beforeSend : function(){
					removeError()
					$("#simpandata").addClass("disabled");
					$("#iconsimpan").addClass("fa fa-spinner fa-spin");
				},
				complete : function(){
					$("#simpandata").removeClass("disabled");
					$("#iconsimpan").removeClass("fa fa-spinner fa-spin");
				},
				error : function(data){
					console.log(data)
				},
				success : function(response){
					console.log(response)
					var tBodyHtml='';
					$.each(response.dataModul,function(index,item){
						tBodyHtml+= '<tr>';
						tBodyHtml+= '<td>'+item.namaModul+'</td>'
						tBodyHtml+= '<td><span class="label label-success">'+item.folder+'</span></td>';
						$.each(response.dataJenisOtorisasi,function(index2,item2){
							tBodyHtml+= '<td><input type="hidden" name="idModul['+index+']" value="'+item.id+'">'
							tBodyHtml+= '<input type="hidden" name="idIzin['+index+']['+index2+']" value="'+item2.id+'">'
							tBodyHtml+= '<input type="hidden" name="namaIzin['+index+']['+index2+']" value="'+item2.inisial+item.folder+'">'
							tBodyHtml+= '<input type="hidden" name="judulHalaman['+index+']['+index2+']" value="'+item.namaModul+'">'
							tBodyHtml+= '<center><input type="checkbox" name="autorisasi['+index+']['+index2+']"></center>	</td>'
						})
						tBodyHtml+= '</tr>';
					})
					$("#konten-modul").html(tBodyHtml);
				}
			});
		})

	})
	$('#checkAll').click(function () {    
		$('input:checkbox').prop('checked', this.checked);    
	});
	$('#frmTambahOtorisasiModul').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	$('#simpan-otorisasi-data').click(function(){
		var filter = [];
		var data = [];
		$('#holder-'+holderId).html('');
		$('#filter-holder #filter').each(function(index, el){
			filter[index] = $(this).val();
			if($(this).val() != "")
				$('#holder-'+holderId).append('<input type="hidden" id="filter" name="filterOnTable['+holderId+'][]" value="'+filter[index]+'"/>');
		});
		$('#filter-holder #data').each(function(index, el){
			data[index] = $(this).val();
			if($(this).val() != "")
				$('#holder-'+holderId).append('<input type="hidden" id="data" name="dataOnTable['+holderId+'][]" value="'+data[index]+'"/>');
		});
		$('#modal-otorisasi-data').modal('hide');
	})
	function nextAction(data){
		$('form')[0].reset();
	}
	$('body tbody').on('click','#btnModalOtorisasiData',function(e){
		var idModul = $(this).data('modul');
		holderId = idModul;
		var htmlFilter = [];
		var htmlData   = [];
		$('#holder-'+idModul+' #filter').each(function(index,el){
			htmlFilter[index] = $(this).val();
		});
		$('#holder-'+idModul+' #data').each(function(index,el){
			htmlData[index] = $(this).val();
		});
		console.log(htmlData);
		var row = "";
		$.each(htmlFilter,function(index,el){
			row += '<div class="row" id="appended-row"  style="margin-bottom:10px"> <div class="col-md-4"> <input class="form-control" placeholder="Filter" name="filter[]" id="filter" value="'+htmlFilter[index]+'"></input> </div> <div class="col-md-7"> <input class="form-control" id="data" placeholder="Data (pisah dengan titik koma)" name="data[]" value="'+htmlData[index]+'"></input> </div> <div class="col-md-1"> <button class="btn btn-danger" id="btn-remove-filter"><i class="fa fa-times"></i></button> </div> </div>';
		})
		row += '</div>';
		row += '<div class="row"><div class="col-md-4"> <input class="form-control" placeholder="Filter" id="filter" name="filter[]"></input> </div> <div class="col-md-7"> <input class="form-control" placeholder="Data (pisah dengan titik koma)" id="data" name="data[]"></input> </div> <div class="col-md-1"> <button class="btn btn-success" id="btn-add-filter"><i class="fa fa-plus"></i></button> </div></div>';
		$('#modal-otorisasi-data #filter-holder').html(row);
		$('#modal-otorisasi-data').modal('show');

		$('#btn-add-filter').click(function(){
			var row = '<div class="row" id="appended-row"  style="margin-bottom:10px"> <div class="col-md-4"> <input class="form-control" placeholder="Filter" name="filter[]" id="filter"></input> </div> <div class="col-md-7"> <input class="form-control" id="data" placeholder="Data (pisah dengan titik koma)" name="data[]"></input> </div> <div class="col-md-1"> <button class="btn btn-danger" id="btn-remove-filter"><i class="fa fa-times"></i></button> </div> </div>';
			$('#filter-holder').prepend(row);
		})

	})
	$('.modal #filter-holder').on('click','#btn-remove-filter',function(){
		var filterRow = $(this).parent().parent();
		filterRow.remove();
	})
</script>