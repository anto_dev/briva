<?php 
Route::group(['prefix' => env('PREFIX_ADMIN')], function() {
	Route::group(['prefix' => 'otorisasimodul', 'middleware' => ['web','Admin','autorisasi']], function(){
		Route::get('/','otorisasimodulcontroller@index')->name('lihatotorisasimodul');
		Route::post('/','otorisasimodulcontroller@muatData')->name('lihatotorisasimodul');
		Route::get('/tambah','otorisasimodulcontroller@tambahData')->name('tambahotorisasimodul');
		Route::post('/tambah','otorisasimodulcontroller@simpanData')->name('tambahotorisasimodul');
		Route::get('/hapus/{id}','otorisasimodulcontroller@hapusData')->name('hapusotorisasimodul');
		Route::get('/edit/{id}','otorisasimodulcontroller@editData')->name('editotorisasimodul');
		Route::post('/edit','otorisasimodulcontroller@updateData')->name('editotorisasimodul');
		// Route::get('/lihatfilter','otorisasimodulcontroller@lihatFilter')->name('lihatotorisasimodul');
		// Route::post('/aturotorisasidata','otorisasimodulcontroller@aturOtorisasiData')->name('editotorisasimodul');
	});
	Route::group(['prefix' => 'otorisasimodul', 'middleware' => ['web','Admin']], function(){
		Route::get('/getmodulotorisasi/{param}','otorisasimodulcontroller@getmodulotorisasi')->name('getmodulotorisasi');
	});
});