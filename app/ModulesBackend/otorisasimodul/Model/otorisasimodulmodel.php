<?php
namespace App\ModulesBackend\otorisasimodul\Model;

use Illuminate\Database\Eloquent\Model;

class otorisasimodulmodel extends Model
{
	protected $table = 'tb_otorisasi';
	public $timestamps = false;
	protected $fillable = ['idModul','idIzin','namaIzin','judulHalaman','grupUser'];

}