<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\otorisasimodul\Model\otorisasimodulModel;
use App\ModulesBackend\jenisotorisasi\Model\jenisotorisasiModel;
use App\ModulesBackend\modul\Model\modulmodel;
use App\ModulesBackend\grupuser\Model\grupuserModel;
use App\Custom\CustomClass;
class otorisasimodulcontroller  extends Controller
{
	function index(Request $request){
		$konten=view("otorisasimodul::view")->render();

        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }
        return view('layouts.backend.app',['konten'=>$konten]);
	}
	function muatData(Request $request){
		
		$usedFilter[0] ='tb_grupuser.namaGrupUser';
		
		$filter = 'tb_otorisasi.id';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];	
		}

		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');
		
		$skippedData = ($page - 1) * $take;		

		$totalJumlah = otorisasimodulModel::where($filter , 'LIKE' ,'%'.$data.'%')
		->join('tb_grupuser','tb_grupuser.id','=','tb_otorisasi.grupUser')
		->groupBy('tb_grupuser.namaGrupUser','tb_grupuser.id','tb_otorisasi.idModul')
		->select('tb_grupuser.namaGrupUser','tb_grupuser.id as grupUser')
		->count();
		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = otorisasimodulModel::where($filter , 'LIKE' ,'%'.$data.'%')
		->join('tb_grupuser','tb_grupuser.id','=','tb_otorisasi.grupUser')
		->groupBy('tb_grupuser.namaGrupUser','tb_grupuser.id')
		->select('tb_grupuser.namaGrupUser','tb_grupuser.id as grupUser')
		->take($take)
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$grupUser = grupUserModel::get();
		$jenisotorisasi = jenisotorisasiModel::get();
		$modul = modulmodel::orderBy('namaModul','asc')->get();
		$konten = view("otorisasimodul::tambah",['dataModul' => $modul,'dataGrupUser' => $grupUser,'dataJenisOtorisasi' => $jenisotorisasi])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'GrupUser' => 'unique:tb_otorisasi,grupUser',
		];
		$message = [
			'unique' => ':attribute '.Lang::get('globals.unique')
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$indexRow = 0;
		$row = [];
		if(!empty($request->input('filterOnTable')))
			foreach ($request->input('filterOnTable') as $key => $value) {
				foreach ($value as $key1 => $value1) {
					$filter[$value1] = explode(";", $request->input('dataOnTable')[$key][$key1]);
				}
				$filterArray[$key] = $filter;
			}

			$idmodul='';
			$indexRef=0;
			if(count($request->input('idModul')) > 0)
				foreach ($request->input('idModul') as $key => $value) {
					$idinput=0;
					foreach ($request->input('idIzin')[$key] as $key1 => $value1) {
						if(isset($request->input('autorisasi')[$key][$key1])){
							$row[$indexRow]['grupUser'] = $request->input('GrupUser');
							$row[$indexRow]['idModul'] = $value;
							$row[$indexRow]['idIzin'] = $value1;
							$row[$indexRow]['namaIzin'] = $request->input('namaIzin')[$key][$key1];
							$row[$indexRow]['judulHalaman'] = $request->input('judulHalaman')[$key][$key1];
							$row[$indexRow]['filter'] = (isset($filterArray[$value])) ? json_encode($filterArray[$value]) : "";
							$indexRow +=1;
							if ($idinput==0) {
								if ($indexRef==0) {
									$idmodul=$value;
								}else{
									$idmodul=$idmodul.','.$value;
								}
							}
							$indexRef +=1;
							$idinput=1;
						}
					}
					DB::table('users')->where('GrupUser',$request->input('GrupUser'))->update([
						'idmodul'=>$idmodul
					]);
				}
				$cekopen=DB::table('tb_menuopen')->where('idgrupuser',$request->input('GrupUser'))->first();
				if (!empty($cekopen)) {
					DB::table('tb_menuopen')->where('idgrupuser',$request->input('GrupUser'))->update([
						'idmodul'=>$idmodul
					]);
				}else{
					DB::table('tb_menuopen')->insert([
						'idgrupuser'=>$request->input('GrupUser'),
						'idmodul'=>$idmodul
					]);
				}
				if(count($row) > 0){
					otorisasimodulModel::insert($row);
				}
				return response()->json(1, 200);
			}
			function updateData(Request $request){

				otorisasimodulModel::where('grupUser',$request->input('grupUser'))->delete();
				$indexRow = 0;
				$row = [];

				if(!empty($request->input('filterOnTable')))
					foreach ($request->input('filterOnTable') as $key => $value) {
						foreach ($value as $key1 => $value1) {
							$filter[$value1] = explode(";", $request->input('dataOnTable')[$key][$key1]);
						}
						$filterArray[$key] = $filter;
					}
					$idmodul='';
					$indexRef=0;
					if(count($request->input('idModul')) > 0)
						foreach ($request->input('idModul') as $key => $value) {
							$idinput=0;
							foreach ($request->input('idIzin')[$key] as $key1 => $value1) {
								if(isset($request->input('autorisasi')[$key][$key1])){
									$row[$indexRow]['grupUser'] = $request->input('grupUser');
									$row[$indexRow]['idModul'] = $value;
									$row[$indexRow]['idIzin'] = $value1;
									$row[$indexRow]['namaIzin'] = $request->input('namaIzin')[$key][$key1];
									$row[$indexRow]['judulHalaman'] = $request->input('judulHalaman')[$key][$key1];
									$row[$indexRow]['filter'] = (isset($filterArray[$value])) ? json_encode($filterArray[$value]) : "";
									$indexRow +=1;
									if ($idinput==0) {
										if ($indexRef==0) {
											$idmodul=$value;
										}else{
											$idmodul=$idmodul.','.$value;
										}
									}
									$indexRef +=1;
									$idinput=1;
								}
							}
							DB::table('users')->where('grupUser',$request->input('grupUser'))->update([
								'idmodul'=>$idmodul
							]);
						}

						$cekopen=DB::table('tb_menuopen')->where('idgrupuser',$request->input('grupUser'))->first();
						if (!empty($cekopen)) {
							DB::table('tb_menuopen')->where('idgrupuser',$request->input('grupUser'))->update([
								'idmodul'=>$idmodul
							]);
						}else{
							DB::table('tb_menuopen')->insert([
								'idmodul'=>$idmodul,
								'idgrupuser'=>$request->input('grupUser')
							]);
						}
						if(count($row) > 0){
							otorisasimodulModel::insert($row);
						}
						return response()->json(1, 200);
					}
					function editData(Request $request, $id){
						$cek=DB::table('tb_grupuser')->where('id',$id)->first();
						$akses=$cek->akses;
						$grupUser = grupUserModel::get();
						$jenisotorisasi = jenisotorisasiModel::get();
						$modul = modulmodel::whereIn('akses',[''.$akses.'','2'])->orderBy('namaModul','asc')->get();
						$data = otorisasimodulModel::where('grupUser',$id)
						->join('tb_grupuser','tb_grupuser.id','=','tb_otorisasi.grupUser')
						->select('tb_otorisasi.*','tb_grupuser.namaGrupUser')
						->get();
						foreach ($data as $key => $value) {
							$autorisasi[$value->idModul][$value->idIzin] = true;  
							$filter[$value->idModul] = $value->filter;
						}
						$konten = view("otorisasimodul::edit",['dataModul' => $modul,'dataGrupUser' => $grupUser,'dataJenisOtorisasi' => $jenisotorisasi, 'data' => $data, 'autorisasi' => $autorisasi, 'filter' => $filter])->render();

						if($request->ajax()){
							return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
						}
						return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);

					}
					function getmodulotorisasi($param){
						$cek=DB::table('tb_grupuser')->where('id',$param)->first();
						if(empty($cek)){
						return response()->json(['dataModul' => [],'dataJenisOtorisasi' => []],200);
						}
						$akses=$cek->akses;
						$jenisotorisasi = jenisotorisasiModel::get();
						$modul = modulmodel::where('akses',$akses)->orderBy('namaModul','asc')->get();
						return response()->json(['dataModul' => $modul,'dataJenisOtorisasi' => $jenisotorisasi],200);
					}
					function hapusData(Request $request, $id){
						$cek = otorisasimodulModel::where('grupUser',$id);
						if(empty($cek)){
							return response()->json('Data tidak tersedia', 403);
						}
						$cek->delete();
					}
					function lihatFilter(Request $request){
						$otorisasimodul = otorisasimodulModel::where('grupUser',$request->input('grupUser'))
						->where('idModul',$request->input('idModul'))
						->first();
						if(empty($otorisasimodul) || $otorisasimodul->filter == ""){
							return response()->json(['status' => 0]);
						}
						$filter = json_decode($otorisasimodul->filter);
						return response()->json(['status' => 1, 'data' => $filter]);
					}
					function aturOtorisasiData(Request $request){
						foreach ($request->input('filter') as $key => $value) {
							$data = explode(";",$request->input('data')[$key]);
							$otorisasidata[$value] = $data;
						}
						otorisasimodulModel::where('idModul',$request->input('idModul'))
						->where('grupUser', $request->input('grupUser'))
						->update([
							'filter' => json_encode($otorisasidata)
						]);
					}
				}
