<?php
namespace App\ModulesBackend\laporanbriva\Model;

use Illuminate\Database\Eloquent\Model;

class laporanbrivamodel extends Model
{
	protected $table = 'pmb';
	public $timestamps = false;
}