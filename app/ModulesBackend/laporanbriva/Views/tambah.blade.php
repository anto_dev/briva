<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('tambahgelombang')}}" class="form-vertical" method="post" id="frmTambahgelombang">
		{{csrf_field()}}
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group" id="groupNama">
								<label for="Nama" class="control-label">Nama Gelombang :</label>
								<input type="text" class="form-control input-sm" id="Nama" name="Nama" autocomplete="off">
							</div>
							<div class="form-group" id="groupPeriodePendaftaran">
								<label for="PeriodePendaftaran" class="control-label">Periode Pendaftaran :</label>
								<div class="input-group">
									<input type="text" class="form-control input-sm periodegelombang" id="PeriodePendaftaran" name="PeriodePendaftaran" readonly="" autocomplete="off">
									<span class="input-group-btn">
										<button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<div class="form-group" id="groupPeriodeUjian">
								<label for="PeriodeUjian" class="control-label">Periode Ujian :</label>
								<div class="input-group">
									<input type="text" readonly="" class="form-control input-sm periodegelombang" id="PeriodeUjian" name="PeriodeUjian" autocomplete="off">
									<span class="input-group-btn">
										<button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<div class="form-group" id="groupPeriodeBayar">
								<label for="PeriodeBayar" class="control-label">Periode Bayar Ujian :</label>
								<div class="input-group">
									<input type="text" readonly="" class="form-control input-sm periodegelombang" id="PeriodeBayar" name="PeriodeBayar" autocomplete="off">
									<span class="input-group-btn">
										<button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<div class="form-group" id="groupTelitiBayarProdi">
								<label for="TelitiBayarProdi" class="control-label">Teliti Bayar Prodi :</label>
								<input type="text" class="form-control input-sm" id="TelitiBayarProdi" name="TelitiBayarProdi" autocomplete="off">
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="form-group" id="groupKetua">
										<label for="Ketua" class="control-label">Ketua :</label>
										<input type="text" class="form-control input-sm" id="Ketua" name="Ketua" autocomplete="off">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group" id="groupStatus">
										<label for="Status" class="control-label">Status :</label>
										<select name="Status" id="Status" class="form-control input-sm" required="required">
											<option value="N">Aktif</option>
											<option value="Y">Tidak Aktif</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatgelombang')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$(document).ready(function(){
		setPeriodeY();
	})
	$('#frmTambahgelombang').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('form')[0].reset();
		setPeriodeY();
	}
	function setPeriodeY(){
		$('.periodegelombang').daterangepicker({
			minDate: new Date(),
			startDate: new Date(),
			autoClose:true,
			locale: {
				format: 'YYYY-MM-DD'
			}
		})
	}
</script>