<section class="content-header">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Table</li>
        </ol>
    </nav>
</section>
<section class="content">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card" id="box-primary">
            <div class="card-body">
                <h6 class="card-title">
                    {{Helper::showTitle()}}
                </h6>
                <div class="row" id="xaxa">
                    
                    <div class="col-md-12">
                        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                            <div class="d-flex align-items-center flex-wrap text-nowrap">
                                <button type="button" id="refresh" class="btn btn-outline-info btn-icon-text mr-2 d-none d-md-block">
                                    <i class="btn-icon-prepend" data-feather="download"></i>
                                    Refresh
                                </button>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col">
                                <label>Filter:</label>
                                <select name="filter" id="filter" class="form-control">
                                    <option value="">{{Lang::get('globals.placeholderSelect')}} - Filter Data</option>
                                    
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Data Pencarian:</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" placeholder="Ketikkan kata kunci...">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" onclick="muatData()" type="button">Cari</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group" style="width: 100%;">
                                    <label>Periode Tanggal : </label>
                                    <div class="input-group">
                                        <input class="form-control input-sm" type="text" id="periode" name="periode" placeholder="Masukkan periode pencarian data"></input>
                                        <span class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="muatData()">Cari</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="box-body">
                    <div class="box table-responsive table-condensed" id="box-table" class="col-md-12">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th width="2%">
                                        <input type="checkbox" id="checkAll" />
                                    </th>
                                    <th width="3%">
                                        No
                                    </th>
                                    <th width="20%">
                                        Teller ID
                                    </th>
                                    <th width="20%">
                                        Customer Code
                                    </th>
                                    <th width="20%">
                                        Name
                                    </th>
                                    <th width="10%">
                                        Description
                                    </th>
                                    <th width="10%">
                                        Amount
                                    </th>
                                    <th width="10%">
                                        Payment Date
                                    </th>
                                    
                                    <th width="7%">
                                        Company Account
                                    </th>
                                    
                                </tr>
                            </thead>
                            <tbody id="data">
                                <tr>
                                    <td colspan="12">Belum ada data ditampilkan</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="9">                                
                                        <label class="label label-default">esc : beranda</label>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><br>
                <div class="box-footer">
                    
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <table>
                                <tr>
                                    <td>{{Lang::get('globals.labelJumlahTampil')}}:  &nbsp;</td>
                                    <td>
                                        <select id="take" class="form-control input-sm" onchange="muatData()">
                                            <option value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </td>
                                    <td>
                                        &nbsp; Total : <span id="totalRecord"></span> Data
                                    </td>
                                </tr>
                            </table>
                            
                            
                            
                        </div>
                        <div class="col-sm-12 col-md-7 pull-right">
                            <ul class="pagination pagination-sm" style="margin: 0px;float: right !important;">
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <style>
        /*input[type='checkbox']{
            appearance: none;
        }*/
    </style>
    <script>
        $(document).ready(function(){
            var d = new Date();
            var m = d.getMonth();
            var bulan = d.getMonth()+1;
            var tgl=d.getDate();
            $('body #periode').daterangepicker({
                startDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
                endDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
                locale: {
                    format: 'DD-MM-YYYY'
                }
            })
            $('#refresh').click(function(){
                muatData();
            })
            $('#downloadexcell').click(function(){
                var link=$(this).data('url');
                periode=$("#periode").val();
                var query = {
                    periode: periode
                }
                var url = link+"?" + $.param(query)
                console.log(url);
                window.location = url;
            });
        });
        function muatData(page = 1){
            var filter='';
            periode=$("#periode").val();
            $.ajax({
                url : '{{url(env('PREFIX_ADMIN').'/laporanbriva')}}',
                type : 'POST',
                data : {
                    _token : token,
                    periode : periode
                },
                beforeSend : function(){
                    callOverlay('box-table');
                },
                complete : function(){
                    removeOverlay();
                },
                success : function(response){
                    var activePage = response.activePage;
                    var totalPage = response.totalPage;
                    var totalRecord = response.totalRecord;
                    var urut=0;
                    var tbodyHtml = '';
                    $.each(response.data, function(index, el){
                        urut++;
                        tbodyHtml += '<tr>'
                            tbodyHtml += '<td><input type="checkbox" id="checkRow" data-id="'+el.PMBID+'"/></td>'
                            tbodyHtml += '<td>'+urut+'</td>'
                            tbodyHtml += '<td>'+el.tellerid+'</td>'
                            tbodyHtml += '<td>'+el.custCode+'</td>'
                            tbodyHtml += '<td>'+el.nama+'</td>'
                            tbodyHtml += '<td>'+el.keterangan+'</td>'
                            tbodyHtml += '<td align="right">'+formatter.format(el.amount)+'</td>'
                            tbodyHtml += '<td>'+el.paymentDate+'</td>'
                            tbodyHtml += '<td>'+el.no_rek+'</td>'
                            @autorisasi(editlaporanbriva|hapuslaporanbriva)
                            tbodyHtml += '<td>'
                                @autorisasi('editlaporanbriva')
                                tbodyHtml += '<a href="{{url(env('PREFIX_ADMIN').'/laporanbriva/edit')}}/'+el.PMBID+'" id="ajaxlink" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>'
                                @endautorisasi
                                @autorisasi('hapuslaporanbriva')
                                tbodyHtml += '<button class="btn btn-danger btn-sm" data-url={{url(env('PREFIX_ADMIN').'/laporanbriva/hapus')}} data-id="'+el.PMBID+'" data-page="'+page+'" id="hapusData"><i class="fa fa-trash"></i></button>'
                                @endautorisasi
                                tbodyHtml += '</td>'
                                @endautorisasi
                                tbodyHtml += '</tr>' 
                            })
                            loadPagination('pagination',totalPage, activePage, totalRecord)
                            $('.content table #data').html(tbodyHtml);
                            if(response.data.length == 0){
                                $('.content table #data').html('<tr><td colspan="12"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
                                toastr['error'](response.message);
                            }
                            // createSession('laporanbriva',page,take,filter,data);
                        },
                        error : function(data){
                            toastr['error'](data.responseText);
                        }
                    })
                }
            </script>