<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'laporanbriva', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','laporanbrivacontroller@lihat')->name('lihatlaporanbriva');
	Route::post('/','laporanbrivacontroller@muatData')->name('lihatlaporanbriva');
	Route::get('/tambah','laporanbrivacontroller@tambahData')->name('tambahlaporanbriva');
	Route::post('/tambah','laporanbrivacontroller@simpanData')->name('tambahlaporanbriva');
	Route::get('/hapus/{id}','laporanbrivacontroller@hapusData')->name('hapuslaporanbriva');
	Route::get('/edit/{id}','laporanbrivacontroller@editData')->name('editlaporanbriva');
	Route::post('/edit','laporanbrivacontroller@updateData')->name('editlaporanbriva');
});
		Route::get('/laporanbriva/downloadexcel/{param?}', 'laporanbrivacontroller@downloadexcel')->name('downloadexcellaporanbriva');
});