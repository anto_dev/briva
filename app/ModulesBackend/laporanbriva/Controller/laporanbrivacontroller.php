<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\laporanbriva\Model\laporanbrivaModel;
use App\Custom\CustomClass;
class laporanbrivacontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('laporanbriva::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		$periode=$request->input('periode');
		$startDate=substr($periode, 0,10);
		$endDate=substr($periode, -10);
		if (Helper::validateDate($startDate,'d-m-Y') && Helper::validateDate($endDate,'d-m-Y')) {
			$startDate=date('Y-m-d',strtotime($startDate));
			$endDate=date('Y-m-d',strtotime($endDate));
			$startDate=str_replace('-', '', $startDate);
			$endDate=str_replace('-', '', $endDate);
			$response=Helper::q_briva_report($startDate,$endDate);
			$status=$response['status'];
			if ($status) {
				$data=$response['data'];
				$totalJumlah=count($data);
				$totalPage=1;
				$page=1;
				return response()->json(['data' => $data, 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
			}else{
				return response()->json(['data'=>[],'message'=>$response['errDesc']]);
			}
		}else{
			return response('Format periode tanggal tidak valid',409);
		}
	}
	public function downloadexcel(Request $request){
		$periode=$request->input('periode');
		$startDate=substr($periode, 0,10);
		$endDate=substr($periode, -10);
		if (Helper::validateDate($startDate,'d-m-Y') && Helper::validateDate($endDate,'d-m-Y')) {
			$startDate=date('Y-m-d',strtotime($startDate));
			$endDate=date('Y-m-d',strtotime($endDate));
			$startDate=str_replace('-', '', $startDate);
			$endDate=str_replace('-', '', $endDate);
			$response=Helper::q_briva_report($startDate,$endDate);
			$status=$response['status'];
			if ($status) {
				$data=$response['data'];
				$dq=$response['data'];
				if (count($dq)==0) {
					return response()->json('Data tidak tersedia', 403);
				}
				foreach ($dq as $key => $el) {
					$data[] = array(
						"No"=> ++$key,
						"custCode"=> $el['custCode'],
						"nama"=> $el['nama'],
						"keterangan"=> $el['keterangan'],
						"amount"=> number_format($el['amount'],0,'.',','),
						"paymentDate"=> $el['paymentDate'],
						"no_rek"=> $el['no_rek'],
					);
				}
				return Excel::create('LAPORAN DATA PAYMENT BRIVA', function($excel) use ($data,$periode) {

					$excel->sheet('LAPORAN BRIVA', function($sheet) use ($data,$periode)
					{
						$sheet->mergeCells('A1:G1');
						$sheet->mergeCells('A2:G2');
						$sheet->row(1, function ($row) {
							$row->setFontFamily('Tahoma');
							$row->setFontSize(11);

						});
						$sheet->setColumnFormat(array(
							'C' => '0'
						));
						$sheet->getStyle('A1')->getAlignment()->applyFromArray(
							array('horizontal' => 'center')
						);
						$sheet->getStyle('A2')->getAlignment()->applyFromArray(
							array('horizontal' => 'center')
						);
						$sheet->getStyle('E')->getAlignment()->applyFromArray(
							array('horizontal' => 'right')
						);
						$sheet->getStyle('N')->getAlignment()->applyFromArray(
							array('horizontal' => 'right')
						);
						$styleArray = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN
								)
							)
						);

						$sheet->getStyle()->applyFromArray($styleArray);
						$sheet->row(1, array('LAPORAN BRIVA '));
						$sheet->row(2, array('PER : '.$periode));
						$sheet->setWidth('A', 6);
						$sheet->setWidth('E', 12);
						$sheet->setWidth('F', 50);
						foreach ($data as $dt) {
							$sheet->appendRow($dt);
						}
						$headings = array('No', 'Cust Code', 'Nama', 'Keterangan', 'Amount','Payment Date', 'No Rekening');

						$sheet->prependRow(3, $headings);
						$sheet->cells('A3:G3', function($cells) {
							$cells->setBackground('#009933'); 
							$cells->setFontColor('#ffffff'); 
						});
					});
				})->download('xlsx');
			}else{
				return response($response['errDesc'],409);
			}
		}else{
			return response('Format periode tanggal tidak valid',409);
		}
		
	}
}
