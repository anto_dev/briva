<?php

// namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class dashboardcontroller extends Controller
{

  public function index(Request $request)
  {
    $konten=view("dashboard::view")->render();
    if($request->ajax()){
      return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
    }   
    return view('layouts.backend.app',['konten'=>$konten]);
  }
  public function muatData(Request $request){
    $tahun=date('Y');
    $sekarang=date('Y-m-d');
    $qty_revenue=DB::table('v_statements')->count('sequence');
    $sum_revenue=DB::table('v_statements')->sum('credit');
    $qty_revenue_today=DB::table('v_statements')->whereDate('trx_date',$sekarang)->count('sequence');
    $sum_revenue_today=DB::table('v_statements')->whereDate('trx_date',$sekarang)->sum('credit');
    $prodi_revenue=DB::select("SELECT a.majors,count(a.sequence) as qty,sum(a.credit) as credit FROM v_statements a GROUP BY a.majors");
    foreach ($prodi_revenue as $key => $value) {
      $prodi_revenue[$key]->nm_prodi=Helper::q_data_prodi($value->majors);
    }
        // dd($prodi_revenue);
    $pendapatan_cur=DB::select("SELECT MONTH(a.trx_date) as bulan,sum(a.credit) as credit FROM statements a WHERE YEAR(a.trx_date)='2022' GROUP BY MONTH(a.trx_date)");
    $data['prev']=[24000000, 100000000, 251000000, 120000000, 250400000, 300500000, 506000000,152000000,50060000, 150800000, 150090000, 250050000];
    foreach ($pendapatan_cur as $key => $value) {
      $data['current'][$key]=intval($value->credit);
    }
        // $data['current']=[8000000000, 9000000000, 15000000000, 20000000000, 18000000000, 12000000000, 19000000000, 11500000000, 12250000000, 15000000000, 25000000000, 45000000000];
    return response()->json(['data'=>$data,'qty_revenue'=>$qty_revenue,'sum_revenue'=>$sum_revenue,'qty_revenue_today'=>$qty_revenue_today,'sum_revenue_today'=>$sum_revenue_today,'pendapatan_cur'=>$pendapatan_cur,'prodi_revenue'=>$prodi_revenue]);
  }
  public function detail(Request $request)
  {
    $akses = $request->input('akses');
    $type = $request->input('type');
    $dataFilter = $request->input('data');
    $take = $request->input('take');
    $page = $request->input('page');
    $skippedData = ($page - 1) * $take;

    $data = DB::table('v_statements');
    $totalJumlah = DB::table('v_statements');
    if ($type==0) {
      $tahun=date('Y');
      $data=$data;
      $totalJumlah=$totalJumlah;
    }else{
      $sekarang=date('Y-m-d');
      $data=$data->whereDate('datetime',$sekarang);
      $totalJumlah=$totalJumlah->whereDate('datetime',$sekarang);
    }
    if (!empty($data)) {
      $data=$data->where('customer_code', 'LIKE','%'.$dataFilter.'%');
      $totalJumlah=$totalJumlah->where('customer_code', 'LIKE','%'.$dataFilter.'%');
    }
    $totalJumlah=$totalJumlah->count();

    $totalPage = $totalJumlah / $take;

    $totalPage = ceil($totalPage);

    $data=$data->orderBy('sequence')->take($take)->skip($skippedData)->get();

    return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    function ambiltask(){
      $idUser = Auth::guard('admin')->user()->id;
      $task = DB::table('tb_lock')
      ->join('tb_modul','tb_modul.id','=','tb_lock.idModul')
      ->where('tb_lock.idUser', $idUser)
      ->where('tb_lock.status',1)
      ->select('tb_lock.id','tb_modul.namaModul','tb_lock.waktu','tb_lock.url')
      ->get();
      foreach ($task as $key => $value) {
        $task[$key]->waktu = date('d/m/Y H:i:s',strtotime($value->waktu));
      }
      return response()->json($task);
    }
    function hapustask(Request $request){
      $id = $request->input('id');
      $idUser = Auth::guard('admin')->user()->id;
      $task = DB::table('tb_lock')
      ->where('idUser', $idUser)
      ->where('id',$id);

      if(Setting::getSetting()->DeleteLock == 'Y'){
        $task->delete();
      }
      else{
        $task->update([
          'status' => 0
        ]);
      }
    }
    function importExcel(Request $request){

      $path = $request->file('select_file')->getRealPath();

      $data = Excel::load($path)->get();

      if($data->count() > 0)
      {
            // dd($data);
        foreach($data->toArray() as $key => $value)
        {
            // dd($value['no']);
          $sku=$value['sku'];
          $prefix=sprintf('4%06d',$sku);
          DB::table('tb_baranghadiah_fix_idempiere')->insert([
            'kodebaranghadiah'=>$prefix,
            'name'=>$value['name']
          ]);
        }

      }
      return redirect()->route('lihatdashboard')->with('status', 'Success Update!');
    }
  }
