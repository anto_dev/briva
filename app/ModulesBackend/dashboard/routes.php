<?php 
Route::group(['prefix'=>env('PREFIX_ADMIN'),'middleware'=>['web','Admin']], function(){
	Route::group(['prefix' => 'dashboard', 'middleware' => ['web','Admin','autorisasi']], function(){
		Route::get('/', 'dashboardcontroller@index')->name('lihatdashboard');
		Route::post('/', 'dashboardcontroller@muatData')->name('lihatdashboard');
	});
		Route::post('dashboard/detail', 'dashboardcontroller@detail')->name('detaildashboard');
});
?>