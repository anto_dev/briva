<div class="row">
  <div class="col-12 col-xl-12 stretch-card">
    <div class="row flex-grow-1">
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline">
              <h6 class="card-title mb-0">Total Revenue</h6>
            </div>
            <div class="row">
              <div class="col-12 col-md-12 col-xl-12">
                <h3 class="mb-2" id="sumRevenue">0</h3>
                <div class="d-flex align-items-baseline">
                  <a href="#" class="text-danger" id="linkQtyRevenue">
                    <span id="qtyRevenue">0</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline">
              <h6 class="card-title mb-0">Today Revenue</h6>
            </div>
            <div class="row">
              <div class="col-12 col-md-12 col-xl-12">
                <h3 class="mb-2" id="sumRevenueToday">0</h3>
                <div class="d-flex align-items-baseline">
                  <a href="#" class="text-danger" id="linkQtyRevenueToday">
                    <span id="qtyRevenueToday">0</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <figure class="highcharts-figure">
        <div id="container"></div>
        <p class="highcharts-description">
          This chart shows how symbols and shapes can be used in charts.
          Highcharts includes several common symbol shapes, such as squares,
          circles and triangles, but it is also possible to add your own
          custom symbols. In this chart, custom weather symbols are used on
          data points to highlight that certain temperatures are warm while
          others are cold.
        </p>
      </figure>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 col-xl-12 stretch-card">
    <div class="row flex-grow-1" id="prodiRevenue">

    </div>
  </div>
</div>
</div>
</div>
<div class="modal fade" id="modal-detail">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Detail Revenue</h4>
      </div>
      <div class="modal-body">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-md-12">
                <label>Cari Berdasarkan NPM :</label>
                <div class="input-group col-xs-12">
                  <input type="text" class="form-control file-upload-info" id="data" placeholder="Ketikkan kata kunci...">
                  <span class="input-group-append">
                    <button class="file-upload-browse btn btn-primary" onclick="muatData()" type="button">Cari</button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="box table-responsive table-condensed" id="box-table" class="col-md-12">

              <table class="table table-bordered table-striped table-condensed">
                <thead>
                  <tr>
                    <th width="3%">
                      No
                    </th>
                    <th width="20%">
                      Invoice Number
                    </th>
                    <th width="10%">
                      NPM
                    </th>
                    <th width="10%">
                      Credit
                    </th>
                    <th width="20%">
                      Transaction Date
                    </th>
                  </tr>
                </thead>
                <tbody id="data">
                  <td colspan="12"></td>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div><br>
          <div class="card-footer">
            <div class="row">
              <div class="col-sm-12 col-md-5">
                <table>
                  <tr>
                    <td>Tampil:  &nbsp;</td>
                    <td>
                      <select id="take" class="form-control input-sm" onchange="muatData()">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                      </select>
                    </td>
                    <td>
                      &nbsp; Total : <span id="totalRecord"></span> Data
                    </td>
                  </tr>
                </table>



              </div>
              <div class="col-sm-12 col-md-7 pull-right">
                <ul class="pagination pagination-sm" style="margin: 0px;float: right !important;">
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
<script>

  $(document).ready(function(){
    muatRevenue();
    $("#linkQtyRevenue").click(function(e){
      e.preventDefault();
      $("#modal-detail").modal("show");
      $('#data').val('');
      globalType=0;
      muatData();
    })
    $("#linkQtyRevenueToday").click(function(e){
      e.preventDefault();
      $("#modal-detail").modal("show");
      $('#data').val('');
      globalType=1;
      muatData();
    })
  });
  function muatData(page=1){
    var data = $('#data').val();
    var take = $('#take').val();
    $.ajax({
      url : '{{url(env('PREFIX_ADMIN'))}}/dashboard/detail',
      type : 'POST',
      data : {
        _token : token,
        akses:2,
        type:globalType,
        data:data,
        page:page,
        take:take
      },
      beforeSend : function(){
        $('.modal-body table #data').html('<tr><td colspan="8"><center><div class="spinner-grow text-primary" role="status"><span class="sr-only">Loading...</span></div></center></td></tr>');
      },
      complete : function(){

      },
      success : function(response){
        var activePage = response.activePage;
        var totalPage = response.totalPage;
        var totalRecord = response.totalRecord;
        var urut=0;
        var tbodyHtml = '';
        $.each(response.data, function(index, el){
          urut++;
          tbodyHtml += '<tr>'
          tbodyHtml += '<td>'+urut+'</td>'
          tbodyHtml += '<td>'+el.invoice_number+'</td>'
          tbodyHtml += '<td><b>'+el.customer_code+'</b></td>'
          tbodyHtml += '<td align="right">'+formatter.format(el.credit)+'</td>'
          tbodyHtml += '<td>'+el.datetime+'</td>'
          tbodyHtml += '</tr>'
        })
        $('.modal-body table #data').html(tbodyHtml);
        loadPagination('pagination',totalPage, activePage, totalRecord)
        if(response.data.length == 0){
          $('.modal-body table #data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
        }
      },
      error : function(data){
        toastr['error'](data.statusText);
      }
    })
  }
  function muatRevenue(page = 1){
    var arrAmountCur=[];
    var arrAmountPrev=[];
    var filter = $('#filter').val();
    var data = $('#data').val();
    var take = $('#take').val();
    $.ajax({
      url : '{{url(env('PREFIX_ADMIN').'/dashboard')}}',
      type : 'POST',
      data : {
        _token : token
      },
      beforeSend : function(){
        callOverlay('box-table');
      },
      complete : function(){
        removeOverlay();
      },
      success : function(response){
        // console.log(response)
        $("#qtyRevenue").text(formatter.format(response.qty_revenue)+' Transaksi');
        $("#sumRevenue").text('Rp.'+formatter.format(response.sum_revenue));
        $("#qtyRevenueToday").text(formatter.format(response.qty_revenue_today));
        $("#sumRevenueToday").text('Rp.'+formatter.format(response.sum_revenue_today));
        var arrPendapatanCur=response.data.current;
        console.log(arrPendapatanCur);
        var tBodyProdi='';
        $.each(response.prodi_revenue, function(index,el){
          tBodyProdi+='<div class="col-3" style="margin:4px 0px 4px 0px;"><div class="card"><div class="card-body"><div class="d-flex justify-content-between align-items-baseline"><h6 class="card-title mb-0">'+el.majors+'--'+el.nm_prodi+'</h6></div><div class="row"><div class="col-12 col-md-12 col-xl-12"><h3 class="mb-2" id="sumRevenueProdi">Rp.'+formatter.format(el.credit)+'</h3><div class="d-flex align-items-baseline"><p class="text-danger"><span id="qtyRevenueProdi">'+formatter.format(el.qty)+' Transaksi</span></p></div></div></div></div></div></div>'
        });
        $("#prodiRevenue").html(tBodyProdi);
        arrAmountCur=response.data.current;
        arrAmountPrev=response.data.prev;
        Highcharts.chart('container', {
          chart: {
            type: 'spline'
          },
          title: {
            text: 'Graph Transaction'
          },
      // subtitle: {
      //   text: 'Source: WorldClimate.com'
      // },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Revenues'
        },
        labels: {
          formatter: function () {
            return this.value + '';
          }
        }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        spline: {
          marker: {
            radius: 4,
            lineColor: '#666666',
            lineWidth: 1
          }
        }
      },
      series: [{
        name: 'Pendapatan Tahun 2020',
        marker: {
          symbol: 'square'
        },
        data: arrAmountPrev

      }, {
        name: 'Pendapatan Tahun 2021',
        marker: {
          symbol: 'diamond'
        },
        data: arrAmountCur
      }]
    });
      },
      error : function(data){
        toastr['error'](data.statusText);
      }
    })
  }
</script>