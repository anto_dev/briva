<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'statements', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','statementscontroller@lihat')->name('lihatstatements');
	Route::post('/','statementscontroller@muatData')->name('lihatstatements');
	Route::get('/tambah','statementscontroller@tambahData')->name('tambahstatements');
	Route::post('/tambah','statementscontroller@simpanData')->name('tambahstatements');
	Route::get('/hapus/{id}','statementscontroller@hapusData')->name('hapusstatements');
	Route::get('/edit/{id}','statementscontroller@editData')->name('editstatements');
	Route::post('/edit','statementscontroller@updateData')->name('editstatements');
});
		Route::get('/statements/downloadexcel/{param?}', 'statementscontroller@downloadexcel')->name('downloadexcelstatements');
});