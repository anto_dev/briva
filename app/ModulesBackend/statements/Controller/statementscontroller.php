<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\statements\Model\statementsModel;
use App\Custom\CustomClass;
class statementscontroller  extends Controller
{
	function lihat(Request $request){
		$dataProdi=DB::connection('sisfo_pmb')->table('prodi')->select('ProdiID','Nama')->get();
		$dataCategory=DB::table('bills_category')->get();
		$konten = view('statements::view',['prodi'=>$dataProdi,'category'=>$dataCategory])->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		$usedFilter[0] = 'customer_code';
		$tanggalawal=substr($request->input('periode'), 0,10);
		$tanggalakhir=substr($request->input('periode'), 13,10);
		$tanggalawal = \DateTime::createFromFormat('d-m-Y',$tanggalawal);
		$tanggalawal = $tanggalawal->sub(new \DateInterval('P0D'));
		$tanggalakhir = \DateTime::createFromFormat('d-m-Y',$tanggalakhir);
		$tanggalakhir = $tanggalakhir->sub(new \DateInterval('P0D'));
		$filter = 'sequence';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];   
		}
		if ($request->input('prodi')!='') {
			$filterProdi=$request->input('prodi');
		}
		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');

		$skippedData = ($page - 1) * $take;


		$totalJumlah = DB::table('v_statements')->where($filter , 'LIKE' ,'%'.$data.'%')->whereDate('datetime','>=',$tanggalawal)->whereDate('datetime','<=',$tanggalakhir);
		if ($request->input('prodi')!='') {
			$totalJumlah=$totalJumlah->whereIn('majors',$request->input('prodi'));
		}
		if ($request->input('category')!='') {
			$totalJumlah=$totalJumlah->whereIn('category_id',$request->input('category'));
		}
		$totalJumlah=$totalJumlah->count();

		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = DB::table('v_statements')->where($filter , 'LIKE' ,'%'.$data.'%')->whereDate('datetime','>=',$tanggalawal)->whereDate('datetime','<=',$tanggalakhir);
		if ($request->input('prodi')!='') {
			$data=$data->whereIn('majors',$request->input('prodi'));
		}
		if ($request->input('category')!='') {
			$data=$data->whereIn('category_id',$request->input('category'));
		}
		$data=$data->orderBy('sequence')->take($take)
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$konten = view("statements::tambah")->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$periode=$request->input('periode');
		if (Helper::validateDate($periode,'d-m-Y')) {
			$startDate=date('Ymd',strtotime($periode));
			$endDate=date('Ymd',strtotime($periode));
			$response=Helper::q_briva_report($startDate,$endDate);
			$status=$response['status'];
			if ($status) {
				$qty_success=0;
				$data=$response['data'];
				$totaldata=count($data);
				foreach ($data as $key => $value) {
					$custCode=$value['custCode'];
					$brivaNo=$value['brivaNo'];
					$nama=$value['nama'];
					$keterangan=$value['keterangan'];
					$amount=$value['amount'];
					$paymentDate=$value['paymentDate'];
					$tellerid=$value['tellerid'];
					$no_rek=$value['no_rek'];
					DB::beginTransaction();
					DB::connection('sisfo')->beginTransaction();
					try {
						$cek=DB::table('invoices')->where('response',1)->where('npm',$custCode)->where('amount',$amount)->first();
						if (!empty($cek)) {
							$IDKEU=$cek->IDKEU;
							$id_category = $cek->category_id;
							$prodi_id=$cek->major;
							$institutionCode=env('BRIVA_ID');
							DB::table('statements')->insert([
								'company_id'=>$institutionCode,
								'accounts'=>$no_rek,
								'category_id'=>$id_category,
								'majors'=>$prodi_id,
								'trx_date'=>date('Y-m-d',strtotime($paymentDate)),
								'datetime'=>$paymentDate,
								'invoice_number'=>$IDKEU,
								'corporate_code'=>$brivaNo,
								'customer_code'=>$custCode,
								'debit'=>0,
								'credit'=>$amount,
								'teller_id'=>$tellerid,
								'status'=>5,
								'created_at'=>date('Y-m-d H:i:s')
							]);
							DB::connection('sisfo')->table('biayauk')->where('IDKEU',$IDKEU)->where('MhswID',$custCode)->where('Total',$amount)->where('response',3)->update([
								'response'=>5,
								'RekeningID'=>$no_rek,
								'Status'=>'sudah bayar',
								'TglSetor'=>$paymentDate,
								'KeteranganBayar'=>'BRI '.$tellerid,
								'LoginProses'=>'briva',
								'TanggalProses'=>$paymentDate
							]);
							DB::connection('sisfo')->table('detilbiayauk')->where('IDKEU',$IDKEU)->where('MhswID',$custCode)->update([
								'Status'=>'sudah bayar',
								'updated_at'=>$paymentDate
							]);
							DB::table('invoices')->where('IDKEU',$IDKEU)->where('npm',$custCode)->update([
								'Status'=>'sudah bayar',
								'response'=>5
							]);
							$briva_delete=Helper::q_briva_delete($custCode);
							DB::table('log_briva')->insert([
								'reference'=>'delete',
								'payload'=>json_encode($briva_delete),
								'created_ip'=>'172.27.1.67'
							]);
							DB::commit();
							DB::connection('sisfo')->commit();
							++$qty_success;
						}
					} catch (Exception $e) {
						Log::error($e);
						DB::rollBack();
						DB::connection('sisfo')->rollBack();
						continue;
					}
				}
				return response('Berhasil proses data! Jumlah : '.$totaldata.'. Data disimpan : '.$qty_success,200);
			}
		}else{
			return response('Kesalahan! Format tanggal tidak valid',409);
		}
	}
	public function downloadexcel(Request $request){
		$usedFilter[0] = 'customer_code';
		$periode=$request->input('periode');
		$startDate=substr($periode, 0,10);
		$endDate=substr($periode, -10);
		if (Helper::validateDate($startDate,'d-m-Y') && Helper::validateDate($endDate,'d-m-Y')) {
			$tanggalawal=substr($request->input('periode'), 0,10);
			$tanggalakhir=substr($request->input('periode'), 13,10);
			$tanggalawal = \DateTime::createFromFormat('d-m-Y',$tanggalawal);
			$tanggalawal = $tanggalawal->sub(new \DateInterval('P0D'));
			$tanggalakhir = \DateTime::createFromFormat('d-m-Y',$tanggalakhir);
			$tanggalakhir = $tanggalakhir->sub(new \DateInterval('P0D'));
			$filter = 'sequence';
			if($request->input('filter') != ''){
				$filter = $usedFilter[$request->input('filter')];   
			}
			$dataFilter = $request->input('data');
			$dq=DB::table('v_statements')->leftJoin('bills_category as cat','cat.category_id','v_statements.category_id')->where($filter , 'LIKE' ,'%'.$dataFilter.'%')->whereDate('datetime','>=',$tanggalawal)->whereDate('datetime','<=',$tanggalakhir);
			if ($request->input('prodi')!='') {
				$dq=$dq->whereIn('majors',$request->input('prodi'));
			}
			if ($request->input('category')!='') {
				$dq=$dq->whereIn('v_statements.category_id',$request->input('category'));
			}
			$dq=$dq->orderBy('sequence')->select('v_statements.*','cat.keterangan')->get();
			if (count($dq)==0) {
				return response()->json('Data tidak tersedia', 403);
			}
			foreach ($dq as $key => $el) {
				$data[] = array(
					"No"=> ++$key,
					"Invoice Number"=> $el->invoice_number,
					"NPM"=> $el->customer_code,
					"Prodi"=> $el->majors,
					"Category"=> $el->keterangan,
					"Credit"=> $el->credit,
					"Transaction Date"=> $el->trx_date
				);
			}
			return Excel::create('LAPORAN DATA STATEMENT BRIVA', function($excel) use ($data,$periode) {

				$excel->sheet('STATEMENTS', function($sheet) use ($data,$periode)
				{
					$sheet->mergeCells('A1:I1');
					$sheet->mergeCells('A2:I2');
					$sheet->row(1, function ($row) {
						$row->setFontFamily('Tahoma');
						$row->setFontSize(11);

					});
					$sheet->setColumnFormat(array(
						'F' => '0'
					));
					$sheet->getStyle('A1')->getAlignment()->applyFromArray(
						array('horizontal' => 'center')
					);
					$sheet->getStyle('A2')->getAlignment()->applyFromArray(
						array('horizontal' => 'center')
					);
					$sheet->getStyle('E')->getAlignment()->applyFromArray(
						array('horizontal' => 'right')
					);
					$sheet->getStyle('N')->getAlignment()->applyFromArray(
						array('horizontal' => 'right')
					);
					$styleArray = array(
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						)
					);

					$sheet->getStyle()->applyFromArray($styleArray);
					$sheet->row(1, array('STATEMENTS '));
					$sheet->row(2, array('PER : '.$periode));
					$sheet->setWidth('A', 6);
					$sheet->setWidth('E', 12);
					$sheet->setWidth('F', 15);
					foreach ($data as $dt) {
						$sheet->appendRow($dt);
					}
					$headings = array('No', 'Invoice Number', 'NPM', 'Prodi', 'Category','Credit', 'Transaction Date');

					$sheet->prependRow(3, $headings);
					$sheet->cells('A3:I3', function($cells) {
						$cells->setBackground('#009933'); 
						$cells->setFontColor('#ffffff'); 
					});
				});
			})->download('xlsx');
		}else{
			return response('Format periode tanggal tidak valid',409);
		}
		
	}
}
