<section class="content-header">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Table</li>
        </ol>
    </nav>
</section>
<section class="content">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card" id="box-primary">
            <div class="card-body">
                <h6 class="card-title">
                    {{Helper::showTitle()}}
                </h6>
                <div class="row" id="xaxa">

                    <div class="col-md-12">
                        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                            <div class="d-flex align-items-center flex-wrap text-nowrap">
                                <button type="button" id="refresh" class="btn btn-outline-info btn-icon-text mr-2 d-none d-md-block">
                                    <i class="btn-icon-prepend" data-feather="download"></i>
                                    Refresh
                                </button>
                                <button type="button" id="downloadexcell" data-url="{{url(env('PREFIX_ADMIN').'/statements/downloadexcel')}}" class="btn btn-outline-success btn-icon-text mr-2 mb-2 mb-md-0">
                                    <i class="btn-icon-prepend" data-feather="file"></i>
                                    Excell
                                </button>
                                <button id="hapusCheckedkData" data-url="{{url(env('PREFIX_ADMIN').'/modul/hapus')}}" type="button" class="btn btn-danger btn-icon-text mb-2 mb-md-0">
                                    <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                                    Hapus Cek
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="w-100" style="width: 100%;border: 1px solid;border-color: #eaeaea;">
                            <select id="Prodi" multiple="multiple" class="w-100" style="width: 100% !important;">
                                @foreach ($prodi as $element)
                                <option value="{{$element->ProdiID}}">{{$element->Nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label>Cari Berdasarkan NPM :</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" id="data" placeholder="Ketikkan kata kunci...">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" onclick="muatData()" type="button">Cari</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="input-group" style="width: 100%;">
                                    <label>Periode Tanggal : </label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" id="periode" name="periode" placeholder="Masukkan periode pencarian data"></input>
                                        <span class="input-group-append">
                                            <div class="btn-group">
                                                <select id="categoryID" class="input-sm form-control" name="categoryID" multiple="multiple">
                                                    @foreach ($category as $el)
                                                    <option value="{{$el->category_id}}">{{$el->keterangan}}</option>
                                                    @endforeach
                                                </select>
                                                <button type="reset" id="categoryID-button" class="btn btn-primary" onclick="muatData()">Generate</button>
                                            </div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="box-body">
                    <div class="box table-responsive table-condensed" id="box-table" class="col-md-12">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th width="2%">
                                        <input type="checkbox" id="checkAll" />
                                    </th>
                                    <th width="3%">
                                        No
                                    </th>
                                    <th width="20%">
                                        Invoice Number
                                    </th>
                                    <th width="10%">
                                        NPM
                                    </th>
                                    <th width="10%">
                                        Credit
                                    </th>
                                    <th width="20%">
                                        Transaction Date
                                    </th>
                                    <th width="10%">
                                        Status
                                    </th>

                                </tr>
                            </thead>
                            <tbody id="data">
                                <td colspan="12"></td>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">                                
                                        <label class="label label-default">esc : beranda</label>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><br>
                <div class="card-footer">

                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <table>
                                <tr>
                                    <td>{{Lang::get('globals.labelJumlahTampil')}}:  &nbsp;</td>
                                    <td>
                                        <select id="take" class="form-control input-sm" onchange="muatData()">
                                            <option value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </td>
                                    <td>
                                        &nbsp; Total : <span id="totalRecord"></span> Data
                                    </td>
                                </tr>
                            </table>



                        </div>
                        <div class="col-sm-12 col-md-7 pull-right">
                            <ul class="pagination pagination-sm" style="margin: 0px;float: right !important;">
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <style>
    /*input[type='checkbox']{
        appearance: none;
        }*/
    </style>
    <script>

        $(document).ready(function(){
            $('#Prodi').multiselect({
                buttonWidth: '100%',
                nonSelectedText:'-- Pilih Prodi --',
                enableFiltering: true,
                includeSelectAllOption: true,
                selectAllText: 'Pilih Semua'
            });
            $('#categoryID').multiselect({
                nonSelectedText:'Pilih Item Pembayaran',
                enableFiltering: true,
                includeSelectAllOption: true,
                selectAllText: 'Pilih Semua'
            });
            var d = new Date();
            var m = d.getMonth();
            var bulan = d.getMonth()+1;
            var tgl=d.getDate();
            var tahun=d.getFullYear()-1;
            $('body #periode').daterangepicker({
                startDate: +tgl+'-'+bulan+'-'+tahun,
                endDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
                locale: {
                    format: 'DD-MM-YYYY'
                }
            })
            var session = readSession();
            if(session){
                filter = session.filter;
                take = session.take;
                page = session.page;
                data = session.data;
                $('#take').val(take);
                $('#data').val(data);
                $('#filter').val(filter);
                muatData(page);
            }
            else{
                muatData();
            }
            $('#refresh').click(function(){
                $('#take').val(5);
                $('#data').val('');
                $('#filter').val('');
                muatData();
            })
            $('#downloadexcell').click(function(){
                var link=$(this).data('url');
                var filter=0;
                var data = $('#data').val();
                periode=$("#periode").val();
                var prodi=$("#Prodi").val();
                var category=$("#categoryID").val();
                var query = {
                    filter: filter,
                    data: data,
                    periode: periode,
                    prodi:prodi,
                    category:category
                }
                var url = link+"?" + $.param(query)
                console.log(url);
                window.location = url;
            });
        });
        function muatData(page = 1){
            var filter = 0;
            var data = $('#data').val();
            var take = $('#take').val();
            var prodi=$("#Prodi").val();
            var category=$("#categoryID").val();
            periode=$("#periode").val();
            $.ajax({
                url : '{{url(env('PREFIX_ADMIN'))}}/statements',
                type : 'POST',
                data : {
                    _token : token,
                    filter : filter,
                    data : data,
                    page : page,
                    take : take,
                    periode:periode,
                    prodi:prodi,
                    category:category
                },
                beforeSend : function(){
                    callOverlay('box-table');
                },
                complete : function(){
                    removeOverlay();
                },
                success : function(response){
                    var activePage = response.activePage;
                    var totalPage = response.totalPage;
                    var totalRecord = response.totalRecord;
                    var urut=0;
                    var tbodyHtml = '';
                    $.each(response.data, function(index, el){
                        urut++;
                        tbodyHtml += '<tr>'
                        tbodyHtml += '<td><input type="checkbox" id="checkRow" data-id="'+el.sequence+'"/></td>'
                        tbodyHtml += '<td>'+urut+'</td>'
                        tbodyHtml += '<td>'+el.invoice_number+'</td>'
                        tbodyHtml += '<td><b>'+el.customer_code+'</b></td>'
                        tbodyHtml += '<td align="right">'+formatter.format(el.credit)+'</td>'
                        tbodyHtml += '<td>'+el.datetime+'</td>'
                        tbodyHtml += '<td>'+el.status+'</td>'
                        
                        tbodyHtml += '</tr>'
                    })
                    loadPagination('pagination',totalPage, activePage, totalRecord)
                    $('.content table #data').html(tbodyHtml);
                    if(response.data.length == 0){
                        $('.content table #data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
                    }
                    // createSession('statements',page,take,filter,data);
                },
                error : function(data){
                    toastr['error'](data.statusText);
                }
            })
        }
    </script>