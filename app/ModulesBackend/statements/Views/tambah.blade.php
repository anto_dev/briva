<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<form action="{{route('tambahstatements')}}" class="form-vertical" method="post" id="frmTambahstatements">
				<div class="card-header">
					<h6 class="card-title">
						Update Payment Back Date
					</h6>
				</div>
				<div class="card-body">
					{{csrf_field()}}
					<div class="row">
						<div class="col-md-12">
							<label for="statements" class="control-label">Date Of Payment :</label>
							<div class="input-group date datepicker" id="datePickerExample">
								<input type="text" class="form-control" id="periode" name="periode">
								<span class="input-group-text input-group-addon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg></span>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer" align="right">
					<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatstatements')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
					<button class="btn btn-primary btn-sm" type="submit">Submit</button>
				</div>
			</form>
		</div>
	</div>
</section>
<script>
	$(document).ready(function(){

	})
	$('#frmTambahstatements').submit(function(e){
		e.preventDefault();
		konfirmasi('Proses data ini?','Proses', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('form')[0].reset();
	}
</script>