<?php
namespace App\ModulesBackend\statements\Model;

use Illuminate\Database\Eloquent\Model;

class statementsmodel extends Model
{
	protected $table = 'statements';
	public $timestamps = false;
}