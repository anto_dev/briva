<?php 
Route::group(['prefix' => env('PREFIX_PMB'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'tagihanuk', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','tagihanukcontroller@lihat')->name('lihattagihanuk');
	Route::post('/','tagihanukcontroller@muatData')->name('lihattagihanuk');
	Route::get('/tambah','tagihanukcontroller@tambahData')->name('tambahtagihanuk');
	Route::post('/tambah','tagihanukcontroller@simpanData')->name('tambahtagihanuk');
	Route::get('/hapus/{id}','tagihanukcontroller@hapusData')->name('hapustagihanuk');
	Route::get('/edit/{id}','tagihanukcontroller@editData')->name('edittagihanuk');
	Route::post('/edit','tagihanukcontroller@updateData')->name('edittagihanuk');
});
});