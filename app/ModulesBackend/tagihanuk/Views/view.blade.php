<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<div class="box box-primary" id="box-primary">
		<div class="box-header with-border">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info alert-dismissible">
						<h4><i class="icon fa fa-info"></i> Perhatian!</h4>
						Anda dinyatakan lulus ujian seleksi penerimaan mahasiswa baru Universitas HKBP Nommensen!<br>
					</div>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="box table-responsive" id="box-table" class="col-md-12">
				@if ($generated)
				@if (!empty($data))
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">Data Tagihan Kuliah Sudah digenerate</h3>
					</div>
					<div class="panel-body">
						<table class="table table-condensed table-hover">
							<tbody>
								<tr>
									<td>No Pendaftaran</td><td>: {{$data->PMBID}}</td>
								</tr>
								<tr>
									<td>Kode Briva</td><td>: {{$data->kodebriva}}</td>
								</tr>
								<tr>
									<td>Keterangan</td><td>: {{$data->UPemb}}</td>
								</tr>
								<tr>
									<td>Total Tagihan</td><td>: <b>Rp{{number_format($data->JumlahPUBank,0,',','.')}}</b></td>
								</tr>
								<tr>
									<td>Terbilang</td><td>: {{$data->Terbilang}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				@endif
				@else
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th width="2%">
								<input type="checkbox" id="checkAllTagihan" />
							</th>
							<th>
								Cicilan
							</th>
							<th>
								Keterangan
							</th>
							<th>
								Jumlah
							</th>
						</tr>
					</thead>
					<tbody id="data">
						@if (!empty($data) && !empty($cicilan))
						@foreach ($cicilan as $element)
						<tr>
							<td><input type="checkbox" id="checkTagihan" data-id="{{$element->KodeCicilan}}" data-nominal="{{$element->Jumlah}}"/></td>
							<td width="20%">{{$element->NamaCicilan}}</td>
							<td>{{$element->priority}}</td>
							<td align="right">{{number_format($element->Jumlah,0,',','.')}}</td>
						</tr>
						@endforeach
						<tr class="bg-primary">
							<td colspan="2">Subtotal</td><td align="right" colspan="2"><h2 id="Subtotal"></h2></td>
						</tr>
						<tr>
							<td colspan="2">Terbilang</td><td colspan="2" align="right"><p id="Terbilang"></p></td>
						</tr>
						@else
						<tr>
							<td colspan="6">Data cicilan belum ada</td>
						</tr>
						@endif
					</tbody>
				</table>
				@endif
			</div>
		</div>
		<div class="box-footer" align="right">
			@if (!$generated && !empty($cicilan))
			<button class="btn btn-primary btn-sm" id="btnProsesGenerate" type="button"><i class="fa fa-cog"></i> Generate Uang Kuliah</button>
			@endif
		</div>
	</div>
</section>
<div class="modal fade" id="modal-proses">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Konfirmasi</h4>
			</div>
			<div class="modal-body">
				<label>Anda akan generate cicilan uang kuliah?</label>
				<p>Generate hanya bisa dilakukan 1 kali.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary" id="btnProsesY">Ya</button>
			</div>
		</div>
	</div>
</div>
<style>
	/*input[type='checkbox']{
		appearance: none;
		}*/
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnProsesGenerate").click(function(){
				$("#modal-proses").modal('show');
			});
			$("#btnProsesY").click(function(){
				prosesData();
				$("#modal-proses").modal("hide");
			})
			
		})
		function prosesData(){
			var arrID=[];
			$('#checkTagihan:checked').each(function(index,el){
				arrID.push($(this).data('id'));
			});
			$.ajax({
				url : '{{route('tambahtagihanuk')}}',
				type : 'POST',
				data : {
					_token : token,
					data : arrID
				},
				beforeSend : function(){
					callOverlay('box-table');
				},
				complete : function(){
					removeOverlay();
				},
				success : function(response){
					toastr['success']('Data berhasil disimpan');
					window.location.reload();
				},
				error : function(response){
					toastr['error'](response.responseText);
				}
			})
		}
	</script>