<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('tambahtagihanuk')}}" class="form-vertical" method="post" id="frmTambahtagihanuk">
		{{csrf_field()}}
		<div class="box box-primary" id="box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Data Diri</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-striped">
								<tbody>
									<tr>
										<td>
											<div class="form-group" id="groupNama">
												<label for="Nama" class="col-md-2 control-label">Nama Calon Mahasiswa</label>
												<div class="col-md-8">
													<input type="text" name="Nama" class="form-control input-sm" id="Nama" placeholder="Nama" autocomplete="off">
												</div>
												<label class="col-md-2">*)Nama sesuai ijazah</label>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupNIK">
												<label for="NIK" class="col-md-2 control-label">NIK</label>
												<div class="col-md-8">
													<input type="text" name="NIK" class="form-control input-sm" id="NIK" placeholder="NIK"autocomplete="off">
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupAlamat">
												<label for="Alamat" class="col-md-2 control-label">Alamat di Medan</label>
												<div class="col-md-8">
													<textarea name="Alamat" id="Alamat" class="form-control" rows="3"></textarea>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupKeterangan">
												<label for="Alamat" class="col-md-2 control-label">Keterangan Tempat Tinggal</label>
												<div class="col-md-8">
													<select name="Keterangan" id="Keterangan" class="form-control">
														<option value="">== Pilih Opsi ==</option>
														<option value="0">Kos</option>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupProvinsi">
												<label for="Alamat" class="col-md-2 control-label">Provinsi</label>
												<div class="col-md-8">
													<select name="Provinsi" id="Provinsi" class="form-control select2 setProvinsi">
														<option value="">== Pilih Provinsi ==</option>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupKota">
												<label for="Kota" class="col-md-2 control-label">Kota/Kabupaten</label>
												<div class="col-md-8">
													<select name="Kota" id="Kota" class="form-control select2 setKota">
														<option value="">== Pilih Provinsi Dahulu ==</option>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupKecamatan">
												<label for="Kecamatan" class="col-md-2 control-label">Kecamatan</label>
												<div class="col-md-8">
													<select name="Kecamatan" id="Kecamatan" class="form-control select2 setKecamatan">
														<option value="">== Pilih Kota/Kabupaten Dahulu ==</option>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupKelurahan">
												<label for="Kelurahan" class="col-md-2 control-label">Kelurahan</label>
												<div class="col-md-8">
													<select name="Kelurahan" id="Kelurahan" class="form-control select2 setKelurahan">
														<option value="">== Pilih Kecamatan Dahulu ==</option>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupKodepos">
												<label for="Kodepos" class="col-md-2 control-label">Kodepos</label>
												<div class="col-md-8">
													<input type="text" name="Kodepos" class="form-control input-sm" id="Kodepos" placeholder="Kodepos"autocomplete="off">
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupTelepon">
												<label for="Telepon" class="col-md-2 control-label">Telepon</label>
												<div class="col-md-8">
													<input type="text" name="Telepon" class="form-control input-sm" id="Telepon" placeholder="Telepon"autocomplete="off">
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupEmail">
												<label for="Email" class="col-md-2 control-label">Email</label>
												<div class="col-md-8">
													<input type="text" name="Email" class="form-control input-sm" id="Email" placeholder="Email"autocomplete="off">
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group" id="groupAlamatAsal">
												<label for="AlamatAsal" class="col-md-2 control-label">Alamat Asal</label>
												<div class="col-md-8">
													<textarea name="AlamatAsal" id="AlamatAsal" class="form-control" rows="3"></textarea>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihattagihanuk')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$(document).ready(function(){
		
	})
	$('#frmTambahtagihanuk').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('#setadmin').show();
		$('form')[0].reset();
	}
</script>