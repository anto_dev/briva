<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\tagihanuk\Model\tagihanukModel;
use App\Custom\CustomClass;
class tagihanukcontroller  extends Controller
{
	function lihat(Request $request){
		$PMBFormJualID=\Session::get('PMBFormJualID');
		$cicilan=[];
		$generated=false;
		$data=DB::table('pmb')->where('PMBFormJualID',$PMBFormJualID)->leftJoin('pmbformulir','pmbformulir.PMBFormulirID','pmb.PMBFormulirID')->select('pmbformulir.FakultasID','pmb.ProdiID','pmb.UPemb','pmb.PMBID','pmb.JumlahPUBank','pmb.UPemb','pmb.Terbilang','pmb.kodebriva')->first();
		if (!empty($data)) {
			$FakultasID=$data->FakultasID;
			$ProdiID=$data->ProdiID;
			$UPemb=$data->UPemb;
			if ($UPemb!='') {
				$generated=true;
			}
			$cicilan=DB::table('cicilan')->where('FakultasID',$FakultasID)->where('ProdiID',$ProdiID)->get();
		}
		$konten = view('tagihanuk::view',['data'=>$data,'cicilan'=>$cicilan,'generated'=>$generated])->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		
		$usedFilter[0] = 'pmb.Nama';

		$filter = 'pmb.PMBID';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];	
		}

		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');
		
		$skippedData = ($page - 1) * $take;
		$totalJumlah = tagihanukModel::where($filter , 'LIKE' ,'%'.$data.'%')->orderBy('id','desc')
		->count();
		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = tagihanukModel::where($filter , 'LIKE' ,'%'.$data.'%')->orderBy('TanggalBuat','desc')
		->take($take)
		->select('pmb.*')
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$data = tagihanukModel::get();
		$konten = view("tagihanuk::tambah",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'data' => 'required|exists:cicilan,KodeCicilan'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$message = [
			'required' => Lang::get('globals.required').' :attribute',
			'exists' => ':attribute '.Lang::get('globals.exists')
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$arr_keterangan='';
		$subtotal=0;
		$index=1;
		$data=$request->input('data');
		foreach ($data as $key => $value) {
			$cicilan=DB::table('cicilan')->where('KodeCicilan',$value)->first();
			if (empty($cicilan)) {
				return response('Data cicilan tidak ditemukan',409);
			}
			$priority=$cicilan->priority;
			if ($priority!=$index) {
				return response('Kesalahan! cicilan harus dimulai dari urutan terkecil',409);
			}
			if ($key==0) {
				$arr_keterangan=$cicilan->NamaCicilan;
			}else{
				$arr_keterangan.=','.$cicilan->NamaCicilan;
			}
			$subtotal+=$cicilan->Jumlah;
			++$index;
		}
		$terbilang=Helper::terbilang($subtotal).' Rupiah';
		$PMBID=\Session::get('PMBID');
		$data_pmb=DB::table('pmb')->where('PMBID',$PMBID)->leftJoin('prodi','prodi.ProdiID','pmb.ProdiID')->select('PMBFormulirID','pmb.Nama','Email','prodi.Nama as namaprodi')->first();
		if(empty($data_pmb)){
			return response('Data pmb tidak ditemukan '.$PMBID,409);
		}
		$PMBFormulirID=$data_pmb->PMBFormulirID;
		$nama=$data_pmb->Nama;
		$email=$data_pmb->Email;
		$namaprodi=$data_pmb->namaprodi;
		$arr_pmbid=explode('-', $PMBID);
		$brivaNo="77678";
		$kodebriva_set=$PMBFormulirID.$arr_pmbid[1].$arr_pmbid[2];
		$keterangan="Tagihan UK Test";
		$briva=Helper::q_briva_create($kodebriva_set,$nama,$subtotal,$keterangan);
		$kodebriva=$brivaNo.$kodebriva_set;
		DB::table('pmb')->where('PMBID',$PMBID)->update([
			'JumlahPUBank'=>$subtotal,
			'UPemb'=>$arr_keterangan,
			'TotalSetoranMhsw'=>$subtotal,
			'Terbilang'=>$terbilang,
			'KwitansiPU'=>'N',
			'SudahBayar'=>'N',
			'response'=>3,
			'kodebriva'=>$kodebriva
		]);
		DB::table('tagihan_briva')->insert([
			'kode_briva'=>$kodebriva_set,
			'invoice_number'=>$PMBID,
			'nama'=>$nama,
			'amount'=>$subtotal,
			'keterangan'=>'CCL',
			'response'=>3,
			'created_ip'=>$request->ip()
		]);
		DB::table('log_briva')->insert([
			'reference'=>'create',
			'payload'=>json_encode($briva),
			'created_ip'=>$request->ip()
		]);
		$pesan='Tagihan Uang Kuliah';
		$mailview="mailTagihanUK";
		$mail=Helper::q_mailer($email,$nama,$namaprodi,$kodebriva_set,$subtotal,$pesan,$keterangan,$mailview);
		return response()->json(1, 200);
	}
	function updateData(Request $request){
		$rules = [
			'Nama' => 'required',
			'Gruptagihanuk' => 'required',
			'Status' => 'required',
			'Kelas' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$tagihanuk = tagihanukModel::find($request->input('id'));
		$tagihanuk->name = $request->input('Nama');
		$tagihanuk->gruptagihanuk = $request->input('Gruptagihanuk');
		$tagihanuk->kodecabang = $kodecabang;
		$tagihanuk->idkelas = $request->input('Kelas');
		// $tagihanuk->type = $type;
		if($request->input('Password') != ''){
			$tagihanuk->password = bcrypt($request->input('Password'));
		}
		$tagihanuk->status = $request->input('Status');
		$tagihanuk->save();
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){

		$data = tagihanukModel::find($id);
		$cabang=cabangmodel::where('Active','Y')->get();
		$gruptagihanuk = gruptagihanukModel::get();

		$konten = view("tagihanuk::edit",['data' =>$data ,'dataGruptagihanuk' => $gruptagihanuk,'cabang'=>$cabang])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}

}
