<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\pengaturanakun\Model\pengaturanakunModel;
use App\ModulesBackend\cabang\Model\cabangModel;
use App\ModulesBackend\gruppengaturanakun\Model\gruppengaturanakunModel;
use App\Custom\CustomClass;
class pengaturanakuncontroller  extends Controller
{
	
	function updateData(Request $request){
		$rules = [
		'Nama' => 'required',
		'Password' => 'required',
		];
		$message = [
		'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$kodecabang=$request->input('KodeCabang');
		$pengaturanakun = pengaturanakunModel::find($request->input('id'));
		// $pengaturanakun->name = $request->input('Nama');
		if($request->input('Password') != ''){
			$pengaturanakun->password = bcrypt($request->input('Password'));
		}
		$pengaturanakun->save();
		return response()->json(1, 200);
	}
	function editData(Request $request){
		$id=Auth::guard('Admin')->user()->id;
		$data = pengaturanakunModel::find($id);

		$konten = view("pengaturanakun::edit",['data' =>$data ])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	
	}
}
