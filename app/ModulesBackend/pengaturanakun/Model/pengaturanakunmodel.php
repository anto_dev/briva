<?php
namespace App\ModulesBackend\pengaturanakun\Model;

use Illuminate\Database\Eloquent\Model;

class pengaturanakunmodel extends Model
{
	protected $table = 'users';
	public $timestamps = false;
}