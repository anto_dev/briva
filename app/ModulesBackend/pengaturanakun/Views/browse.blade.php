
<div class="modal" id="modalcabang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-search"></i> Data Cabang</h4>
      </div>
      <div class="modal-body">
        <form id="frmbrowsecabang" method="post" action="javascript:return false;">
{{ csrf_field() }}
<div class="box-tools">
  <div class="col-md-6">
    <a class="btn btn-primary btn-sm btn-flat col-md-1" onclick="refresh_cabang()"><i class="fa fa-refresh"></i></a>
      <select class="form-control col-md-3 input-sm" name="jumlahdata" id="jumlahdata" onchange="loaddata_cabang()" style="width:50%">
        <option>10</option>
        <option>25</option>
        <option>50</option>
        <option>100</option>
      </select>
  </div>
  <div class="col-md-2">
    <select class="form-control input-sm" name="filter" id="filter" onchange="loaddata_cabang()">
     <option value="">-Pilih Filter-</option>
      <option value="kodecabang">Kode Cabang</option>
      <option value="namacabang">Nama Cabang</option>
      <option value="pimpinancabang">Pimpinan</option>
      <option value="admin">Admin</option>
    </select>
    <input type="hidden" name="browse" value="1">
  </div>
  <div class="col-md-4">
    <div class="input-group">
      <input type="text" class="form-control input-sm" name="value" onchange="loaddata_cabang()" id="value" placeholder="Cari Cabang...">
      <span class="input-group-btn">
        <button class="btn btn-sm" type="button" onclick="loaddata_cabang()"><i class="fa fa-search"></i></button>
      </span>
    </div>
  </div>
</div>
<div class="box-tools with-border">
<div class="col-md-12">
  
  
  <div class="col-md-10">
    <label>&nbsp;</label>
    <div class="pull-right">

    </div>
  </div>
</div>
</div>
</form>
<div class="box-body no-padding" id="data-cabang">

</div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  loaddata_cabang();
function loaddata_cabang(){
  $.ajax({
    url:"{{URL::to('cabang/data')}}",
    type:'POST',
    data:$("#frmbrowsecabang").serialize(),
    success:function(data){
      $("#data-cabang").html(data);
      customcheck();
      $(".pagination a").click(function(e){
        e.preventDefault();
        var page=$(this).attr('href');
        page=page.split("?");
        loadpage_cabang(page[1]);
      })
    }
  })
}
function loadpage_cabang($page){
  $.ajax({
    url:"{{URL::to('cabang/data')}}?"+$page,
    type:'POST',
    data:$("#frmbrowsecabang").serialize(),
    success:function(data){
      $("#data-cabang").html(data);
      customcheck();
      $(".pagination a").click(function(e){
        e.preventDefault();
        var page=$(this).attr('href');
        page=page.split("?");
        loadpage_cabang(page[1]);
      })
    }
  })
}
function refresh_cabang(){
  $("#frmbrowsecabang")[0].reset();
  loaddata_cabang();
}
function pilih_cabang(kodecabang){
  $("#modalcabang").modal('hide');
  $("#kodecabang").val(kodecabang);
  $("#kodecabang").change();
}
</script>