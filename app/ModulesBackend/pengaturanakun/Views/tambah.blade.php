<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('tambahuser')}}" class="form-vertical" method="post" id="frmTambahMenu">
		{{csrf_field()}}
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group" id="groupNama">
							<label for="Nama" class="control-label">Nama :</label>
							
							<input type="text" class="form-control input-sm" id="Nama" name="Nama">
						</div>
						<div class="form-group" id="groupUsername">
							<label for="Username" class="control-label">Username :</label>
							
							<input type="text" class="form-control input-sm" id="Username" name="Username" autocomplete="off">
						</div>
						<div class="form-group" id="groupPassword">
							<label for="Password" class="control-label">Password :</label>
							
							<input type="text" class="form-control input-sm" id="Password" name="Password" autocomplete="off">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" id="groupGrupUser">
							<label for="GrupUser" class="control-label">Grup User :</label>
							<select name="GrupUser" id="GrupUser" class="form-control input-sm">
								@foreach($dataGrupUser as $itemGrupUser)
								<option value="{{$itemGrupUser->id}}">{{$itemGrupUser->namaGrupUser}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group" id="groupStatus">
							<label for="Status" class="control-label">Status :</label>
							<select name="Status" id="Status" class="form-control input-sm">
								@foreach(Lang::get('globals.arrayStatus')  as $key => $itemStatus)
								<option value="{{$key}}">{{$itemStatus}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group" id="groupKodeCabang">
							<label for="KodeCabang" class="control-label">KodeCabang :</label>
							<select name="KodeCabang" id="KodeCabang" class="form-control input-sm">
								@foreach($cabang as $key => $itemKodeCabang)
								<option value="{{$itemKodeCabang->Outlet_Code}}">{{$itemKodeCabang->Outlet_Code}} - [{{$itemKodeCabang->Outlet_Name}}]</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatuser')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$('#frmTambahMenu').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('form')[0].reset();
	}
</script>