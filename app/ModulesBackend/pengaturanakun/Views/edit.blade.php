<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('editpengaturanakun')}}" class="form-vertical" method="post" id="frmEditpengaturanakun">
		{{csrf_field()}}
		<input type="hidden" name="id" value="{{$data->id}}">
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">

					<div class="col-md-6">
						<div class="form-group" id="groupNama">
							<label for="Nama" class="control-label">Nama :</label>
							
							<input type="text" class="form-control input-sm" id="Nama" name="Nama" value="{{$data->name}}" readonly="">
						</div>
						<div class="form-group" id="groupUsername">
							<label for="Username" class="control-label">Username :</label>
							
							<input type="text" class="form-control input-sm" id="Username" name="Username" autocomplete="off" value="{{$data->username}}" readonly="">
						</div>
						<div class="form-group" id="groupPassword">
							<label for="Password" class="control-label">Password :</label>
							<input type="text" class="form-control input-sm" id="Password" name="Password" autocomplete="off">
						</div>
					</div>
					
				</div>
			</div>
			<div class="box-footer" align="left">
				{{-- <a class="btn btn-default btn-sm btnKembali" id="ajaxlink" href="{{route('lihatpengaturanakun')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a> --}}
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$('#Icon').keyup(function(e){
		e.preventDefault();
		$('#iconPreview').attr('class','fa fa-'+$(this).val());
	});
	$('#frmEditpengaturanakun').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	function nextAction(data){
		$('.btnKembali').trigger('click');
	}
</script>