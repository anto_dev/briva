<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin','autorisasi']], function(){
Route::group(['prefix' => 'pengaturanakun', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','pengaturanakuncontroller@editData')->name('editpengaturanakun');
	Route::post('/','pengaturanakuncontroller@muatData')->name('lihatpengaturanakun');
	Route::get('/tambah','pengaturanakuncontroller@tambahData')->name('tambahpengaturanakun');
	Route::post('/tambah','pengaturanakuncontroller@simpanData')->name('tambahpengaturanakun');
	Route::get('/hapus/{id}','pengaturanakuncontroller@hapusData')->name('hapuspengaturanakun');
	Route::get('/edit/{id}','pengaturanakuncontroller@editData')->name('editpengaturanakun');
	Route::post('/edit','pengaturanakuncontroller@updateData')->name('editpengaturanakun');
});
});