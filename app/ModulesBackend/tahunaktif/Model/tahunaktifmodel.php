<?php
namespace App\ModulesBackend\tahunaktif\Model;

use Illuminate\Database\Eloquent\Model;

class tahunaktifmodel extends Model
{
	protected $table = 'tb_tahunaktif';
	public $timestamps = false;
}