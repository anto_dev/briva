<?php

// namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\ModulesBackend\tahunaktif\Model\tahunaktifmodel;
use Illuminate\Http\Request;

class tahunaktifcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data=DB::table('users')->where('id',Auth::guard('Admin')->user()->id)->first();
        $penelitian=DB::table('tb_penelitian as pn')->leftJoin('tb_skema as sk','sk.id','pn.idskema')->where('created_by',Auth::guard('Admin')->user()->id)->get();
        $konten=view("tahunaktif::view",['data'=>$data, 'penelitian'=>$penelitian])->render();

        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }
        return view('layouts.backend.app',['konten'=>$konten]);
    }
    function muatData(Request $request){
        $data=DB::table('tb_tahunaktif')->where('aktif','Y')->first();
        $periode=$data->tanggalmulai.' - '.$data->tanggalakhir;
        return response()->json(['data' => $periode]);
    }
    function updateData(Request $request){
        $periode=$request->input('periode');
        $tanggalawal=substr($request->input('periode'), 0,10);
        $tanggalakhir=substr($request->input('periode'), 13,10);
        $tanggalawal=date('Y-m-d',strtotime($tanggalawal));
        $tanggalakhir=date('Y-m-d',strtotime($tanggalakhir));
        $tahun=date('Y');
        try {
            DB::table('tb_tahunaktif')->where('tahun',$tahun)->where('aktif','Y')->update([
                'tanggalmulai'=>$tanggalawal,
                'tanggalakhir'=>$tanggalakhir
            ]);
            return response()->json('Berhasil simpan', 200);
        } catch (\Throwable $e) {
            return response()->json('Kesalahan '.$e, 409);
        }
    }
}
