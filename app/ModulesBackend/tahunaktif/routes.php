<?php 

Route::group(['prefix'=>'admin'], function(){
	Route::group(['prefix' => 'tahunaktif', 'middleware' => ['web', 'Admin']], function() {
		Route::group(['middleware' => ['autorisasi']], function() {
			Route::get('/', 'tahunaktifcontroller@index')->name('lihattahunaktif');
			Route::post('/','tahunaktifcontroller@muatData')->name('lihattahunaktif');
			Route::get('/tambah','tahunaktifcontroller@tambahData')->name('tambahtahunaktif');
			Route::post('/tambah', 'tahunaktifcontroller@simpanData')->name('tambahtahunaktif');
			Route::get('/edit/{id}','tahunaktifcontroller@editData')->name('edittahunaktif');
			Route::post('/update', 'tahunaktifcontroller@updateData')->name('edittahunaktif');
			Route::get('hapus/{id}','tahunaktifcontroller@hapusData')->name('hapustahunaktif');
		});
			Route::post('/generatetahunaktif', 'tahunaktifcontroller@generatetahunaktif')->name('generatetahunaktif');
			Route::get('/detailtahunaktif/{id}', 'tahunaktifcontroller@detailtahunaktif')->name('detailtahunaktif');
	});



	

})

?>