<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Sekolah Menengah Atas.
            <small class="pull-right">{{date('Y-m-d H:i:s')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Dari
          <address>
            <strong>Bagian Tata Usaha</strong><br>
            Sekolah Menengah Atas<br>
            Pematangsiantar, CA 94107<br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Kepada
          <address>
            <strong>{{$dataheader->nama}}</strong><br>
            {{$dataheader->namakelas}}<br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>No Transaksi {{$dataheader->notransaksi}}</b><br>
          <b>Tanggal :</b> {{$dataheader->created_at}}<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-condensed table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Periode</th>
					<th>Tagihan</th>
					<th>Denda</th>
					<th width="15%">Subtotal</th>
				</tr>
			</thead>
			<tbody>
				@php
					$grandtotal=0;
					$totaldenda=0;
					$totalpotongan=0
				@endphp
				@foreach ($datadetail as $key => $element)
				@php
					$indexbulan=substr($element->periode,-2);
					$periodenya=Helper::converStringBulan($indexbulan);
					$subtotal=$element->tagihan-$element->denda+$element->denda;
				@endphp
					<tr>
					<td>{{++$key}}</td>
					<td>{{$periodenya}}</td>
					<td>{{number_format($element->tagihan,0,',','.')}}</td>
					<td>{{number_format($element->denda,0,',','.')}}</td>
					<td align="right">{{number_format($subtotal,0,',','.')}}</td>
				</tr>
				@endforeach
				<tr class="bg-primary">
					<td colspan="4" align="right">
						Total SPP
					</td>
					<td align="right">
						{{number_format($dataheader->subtotal,0,',','.')}}
					</td>
				</tr>
				<tr class="bg-warning">
					<td colspan="4" align="right">
						Denda
					</td>
					<td align="right">
						{{number_format($dataheader->denda,0,',','.')}}
					</td>
				</tr>
				<tr class="bg-success">
					<td colspan="4" align="right">
						Potongan
					</td>
					<td align="right">
						({{number_format($dataheader->potongan,0,',','.')}})
					</td>
				</tr>
				<tr>
					<td colspan="4" align="right">
						<h4>Total Pembayaran</h4>
					</td>
					<td align="right">
						<h4>{{number_format($dataheader->totaltagihan,0,',','.')}}</h4>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Pembayaran dapat dilakukan melalui :</p>
          <img src="{{asset('/images/briva.png')}}" style="max-width: 100px;" alt="Briva">
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>