<section class="content-header">
  <h1 id="title-content">
    {{Helper::showTitle()}}
  </h1>
</section>
<section class="content">

  <div class="row">
    <div class="col-md-6">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <h2>Tahun Aktif Penelitian : {{date('Y')}}</h2>
              <b class="lblPeriode">Periode : 0000-00-00</b><br>
              <label class="label label-success">Status : Aktif</label>
            </li>
          </ul>
          <br><br>
          <div class="input-group" style="width: 100%;">
            <label>Periode Tanggal : </label>
            <div class="input-group">
              <input class="form-control input-sm" type="text" id="periode" readonly name="periode" placeholder="Masukkan periode pencarian data"></input>
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm" type="button" onclick="muatData()"><i class="fa fa-calendar"></i></button>
              </span>
            </div>
          </div>
          <br><br>
          <a id="btnUbahPeriode" class="btn btn-primary btn-block"><b><i class="btnUbahPeriode"></i>Ubah Periode</b></a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
  <!-- /.row -->

</section>

<div class="modal fade" id="modal-konfirmasi-tahunaktif">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Konfirmasi Perubahan Tahun Aktif</h4>
      </div>
      <div class="modal-body">
        <label>Simpan data?</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="btnSimpanPeriode">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function muatData(){
      $.ajax({
        url:"{{URL::to('')}}/admin/tahunaktif",
        method : 'POST',
        data:{
          _token : token
        },
        beforeSend : function(){
          callOverlay('box-body');
        },
        complete : function(){
          removeOverlay();
        },
        error : function(data){
          console.log(data)
          toastr['error'](data.statusText);
        },
        success : function(response){
          console.log(response)
          $(".lblPeriode").text(response.data);
        }
      })
    }
  $(document).ready(function(){
    muatData();
    var periode=0;
    $("#btnUbahPeriode").click(function(){
      periode = $("#periode").val();
      $("#modal-konfirmasi-tahunaktif").modal("show");
    })
    $("#btnSimpanPeriode").click(function(e){
      $("#modal-konfirmasi-tahunaktif").modal("hide");
      e.preventDefault();
      $.ajax({
        url:"{{URL::to('')}}/admin/tahunaktif/update",
        method : 'POST',
        data:{
          _token : token,
          periode:periode
        },
        beforeSend : function(){
          $('#btnUbahPeriode').addClass('disabled');
          $('.btnUbahPeriode').addClass('fa fa-spinner fa-spin')
        },
        complete : function(){
          $('#btnUbahPeriode').removeClass('disabled');
          $('.btnUbahPeriode').removeClass('fa fa-spinner fa-spin')
        },
        error : function(data){
          console.log(data)
          toastr['error'](data.statusText);
        },
        success : function(response){
          console.log(response)
          toastr['success'](response);
          muatData();
        }
      })
    })
  })
</script>