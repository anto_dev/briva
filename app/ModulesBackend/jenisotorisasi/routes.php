<?php 
Route::group(['prefix' => env('PREFIX_ADMIN')], function(){
	Route::group(['prefix' => 'jenisotorisasi', 'middleware' => ['web', 'Admin','autorisasi']], function() {
	Route::get('/','jenisotorisasicontroller@index')->name('lihatjenisotorisasi');
	Route::post('/','jenisotorisasicontroller@muatData')->name('lihatjenisotorisasi');
	Route::get('/tambah','jenisotorisasicontroller@tambahData')->name('tambahjenisotorisasi');
	Route::post('/tambah','jenisotorisasicontroller@simpanData')->name('tambahjenisotorisasi');
	Route::get('/hapus/{id}','jenisotorisasicontroller@hapusData')->name('hapusjenisotorisasi');
	Route::get('/edit/{id}','jenisotorisasicontroller@editData')->name('editjenisotorisasi');
	Route::post('/edit','jenisotorisasicontroller@updateData')->name('editjenisotorisasi');
});
});