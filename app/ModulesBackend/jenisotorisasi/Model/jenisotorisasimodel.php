<?php
namespace App\ModulesBackend\jenisotorisasi\Model;

use Illuminate\Database\Eloquent\Model;

class jenisotorisasimodel extends Model
{
	protected $table = 'tb_izin';
	public $timestamps = false;
}