<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\jenisotorisasi\Model\jenisotorisasiModel;
use App\Custom\CustomClass;
use App\ModulesBackend\user\Model\userModel;
use App\ModulesBackend\grupuser\Model\grupuserModel;
class jenisotorisasicontroller  extends Controller
{
	function index(Request $request){
		$konten=view("jenisotorisasi::view")->render();

		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten'=>$konten]);
	}
	function muatData(Request $request){
		
		$usedFilter[0] = 'tb_izin.namaIzin';
		$usedFilter[1] = 'tb_izin.inisial';

		$filter = 'id';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];	
		}

		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');
		
		$skippedData = ($page - 1) * $take;
		

		$totalJumlah = jenisotorisasiModel::where($filter , 'LIKE' ,'%'.$data.'%')
		->count();

		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = jenisotorisasiModel::where($filter , 'LIKE' ,'%'.$data.'%')
		->take($take)
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$data = jenisotorisasiModel::get();
		$konten = view("jenisotorisasi::tambah",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'JenisOtorisasi' => 'required',
			'Inisial' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$jenisotorisasi = new jenisotorisasiModel;
		$jenisotorisasi->namaIzin = $request->input('JenisOtorisasi');
		$jenisotorisasi->inisial = $request->input('Inisial');
		$jenisotorisasi->save();
		return response()->json(1, 200);
	}
	function updateData(Request $request){
		$rules = [
			'JenisOtorisasi' => 'required',
			'Inisial' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$jenisotorisasi = jenisotorisasiModel::find($request->input('id'));
		$jenisotorisasi->namaIzin = $request->input('JenisOtorisasi');
		$jenisotorisasi->inisial = $request->input('Inisial');
		$jenisotorisasi->save();
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){
		$data = jenisotorisasiModel::find($id);
		$konten = view("jenisotorisasi::edit",['data' =>$data ])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function hapusData(Request $request, $id){
		$cek = jenisotorisasiModel::find($id);
		if(count($cek) == 0 ){
			return response()->json('Data tidak tersedia', 403);
		}
		$cek->delete();
	}
}
