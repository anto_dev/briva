<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
				<form action="{{route('tambahjenisotorisasi')}}" class="form-vertical" method="post" id="frmTambahJenisOtorisasi">
					{{csrf_field()}}
					<div class="box box-primary" id="box-primary">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">

									<div class="form-group" id="groupJenisOtorisasi">
										<label for="JenisOtorisasi" class="control-label">Jenis Otorisasi :</label>

										<input type="text" class="form-control input-sm" id="JenisOtorisasi" name="JenisOtorisasi">
									</div>
								</div>
								<div class="col-md-6">

									<div class="form-group" id="groupInisial">
										<label for="Inisial" class="control-label">Inisial :</label>

										<input type="text" class="form-control input-sm" id="Inisial" name="Inisial">
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer" align="right">
							<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatjenisotorisasi')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
							<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
			</section>
			<script>
				$('#Icon').keyup(function(e){
					e.preventDefault();
					$('#iconPreview').attr('class','fa fa-'+$(this).val());
				});
				$('#frmTambahJenisOtorisasi').submit(function(e){
					e.preventDefault();
					konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
				})
				function nextAction(data){
					$('form')[0].reset();
				}
			</script>