<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
				<form action="{{route('editjenisotorisasi')}}" class="form-vertical" method="post" id="frmEditjenisotorisasi">
					{{csrf_field()}}
					<input type="hidden" name="id" value="{{$data->id}}">
					<div class="box box-primary" id="box-primary">
						<div class="box-body">
							<div class="row">

								<div class="col-md-6">

									<div class="form-group" id="groupJenisOtorisasi">
										<label for="JenisOtorisasi" class="control-label">Jenis Otorisasi :</label>
										
										<input type="text" class="form-control input-sm" id="JenisOtorisasi" name="JenisOtorisasi" value="{{$data->namaIzin}}">
									</div>
								</div>
								<div class="col-md-6">

									<div class="form-group" id="groupInisial">
										<label for="Inisial" class="control-label">Inisial :</label>
										
										<input type="text" class="form-control input-sm" id="Inisial" name="Inisial" value="{{$data->inisial}}">
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer" align="right">
							<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatjenisotorisasi')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
							<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
	$('#Icon').keyup(function(e){
		e.preventDefault();
		$('#iconPreview').attr('class','fa fa-'+$(this).val());
	});
	$('#frmEditjenisotorisasi').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	function nextAction(data){
		window.location.replace('{{route('lihatjenisotorisasi')}}');
	}
</script>