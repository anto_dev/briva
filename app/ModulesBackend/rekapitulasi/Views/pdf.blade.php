<style type="text/css">
	footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color: lightblue; height: 30px;font-size: 8px; }
</style>
<h3><center>Rekapitulasi Pendapatan {{$filter}}<br>Periode {{$periode}}</center></h3>
<div class="table-responsive">
	<table border="1" cellspacing="0" cellpadding="1" width="100%">
		<tbody>
			@if ($filter=="Global")
			<tr style="padding: 2px;">
				<td colspan="3"><b>1. Global</b></td>
			</tr>
			<tr bgcolor="#eaeaea">
				<td> No</td>
				<td> Jenis Pembayaran</td>
				<td align="center"> Nominal</td>
			</tr>
			@foreach ($data as $key2 => $el)
			<tr>
				<td width="5%"> {{++$key2}}</td>
				<td> {{$el->keterangan}}</td>
				<td align="right"> Rp.{{number_format($el->credit,0,',','.')}}</td>
			</tr>
			@endforeach
			@else
			@php
			$nourut=0;
			@endphp
			@foreach ($data as $key => $val)
			<tr style="padding: 2px;">
				<td colspan="3"><b>{{++$nourut}}. {{$val->Nama}}</b></td>
			</tr>
			<tr bgcolor="#eaeaea">
				<td> No</td>
				<td> Jenis Pembayaran</td>
				<td align="center"> Nominal</td>
			</tr>
			@foreach ($detail[$key] as $key2 => $el)
			<tr>
				<td width="5%"> {{++$key2}}</td>
				<td> {{$el->keterangan}}</td>
				<td align="right"> Rp.{{number_format($el->credit,0,',','.')}}</td>
			</tr>
			@endforeach
			@endforeach
			@endif
			
		</tbody>
	</table>
</div>
<br>
<footer><i>printed at {{date('Y-m-d H:i:s')}}</i></footer>