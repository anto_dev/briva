<section class="content-header">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Table</li>
        </ol>
    </nav>
</section>
<section class="content">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card" id="box-primary">
            <div class="card-body">
                <h6 class="card-title">
                    {{Helper::showTitle()}}
                </h6>
                <div class="row" id="xaxa">

                    <div class="col-md-12">
                        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                            <div class="d-flex align-items-center flex-wrap text-nowrap">
                                <button type="button" id="refresh" class="btn btn-outline-info btn-icon-text mr-2 d-none d-md-block">
                                    <i class="btn-icon-prepend" data-feather="download"></i>
                                    Refresh
                                </button>
                                <button type="button" id="downloadexcell" data-url="{{url(env('PREFIX_ADMIN').'/rekapitulasi/downloadexcel')}}" class="btn btn-outline-success btn-icon-text mr-2 mb-2 mb-md-0">
                                    <i class="btn-icon-prepend" data-feather="file"></i>
                                    Excell
                                </button>
                                <button id="downloadpdf" data-url="{{url(env('PREFIX_ADMIN').'/rekapitulasi/pdf')}}" type="button" class="btn btn-danger btn-icon-text mb-2 mb-md-0">
                                    <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                                    PDF
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%;">
                                    <label>Periode Tanggal : </label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" id="periode" name="periode" readonly="" placeholder="Masukkan periode pencarian data"></input>
                                        <span class="input-group-append">
                                            <div class="btn-group">
                                                <select id="filter" class="input-sm form-control" name="filter">
                                                    <option value="2">Prodi</option>
                                                    <option value="1">Fakultas</option>
                                                    <option value="0">Global</option>
                                                </select>
                                                <button type="reset" id="categoryID-button" class="btn btn-primary" onclick="muatData()">Generate</button>
                                            </div>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="box-body">
                    <div class="box table-responsive table-condensed" id="box-table" class="col-md-12">
                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th width="3%">
                                        No
                                    </th>
                                    <th>
                                        Keterangan
                                    </th>
                                    <th align="right">
                                        Credit
                                    </th>

                                </tr>
                            </thead>
                            <tbody id="data">
                                <td colspan="12"></td>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">                                
                                        <label class="label label-default">esc : beranda</label>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><br>
                <div class="box-footer">

                    
                </div>
            </div>

        </div>
    </section>
    <style>
    /*input[type='checkbox']{
        appearance: none;
        }*/
    </style>
    <div class="modal fade" id="modal-detail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function(){
            var d = new Date();
            var m = d.getMonth();
            var bulan = d.getMonth()+1;
            var tgl=d.getDate();
            var tahun=d.getFullYear()-1;
            $('body #periode').daterangepicker({
                startDate: +tgl+'-'+bulan+'-'+tahun,
                endDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
                locale: {
                    format: 'DD-MM-YYYY'
                }
            })
            $('#refresh').click(function(){
                $('#take').val(5);
                $('#filter').val('');
                muatData();
            })
            $("tbody").on("click","#detail", function(e){
                e.preventDefault();
                $("#modal-detail").modal("show");
            })
            $('#downloadexcell').click(function(){
                var link=$(this).data('url');
                var filter=$("#filter").val();
                periode=$("#periode").val();
                var query = {
                    filter: filter,
                    periode: periode
                }
                var url = link+"?" + $.param(query)
                console.log(url);
                window.location = url;
            });
            $('#downloadpdf').click(function(){
                var link=$(this).data('url');
                var filter=$("#filter").val();
                periode=$("#periode").val();
                var query = {
                    filter: filter,
                    periode: periode
                }
                var url = link+"?" + $.param(query)
                console.log(url);
                window.location = url;
            });
        });
        function muatData(page = 1){
            var filter = $("#filter").val();
            var take = $('#take').val();
            periode=$("#periode").val();
            $.ajax({
                url : '{{url(env('PREFIX_ADMIN'))}}/rekapitulasi',
                type : 'POST',
                data : {
                    _token : token,
                    filter : filter,
                    page : page,
                    take : take,
                    periode:periode,
                },
                beforeSend : function(){
                    callOverlay('box-table');
                },
                complete : function(){
                    removeOverlay();
                },
                success : function(response){
                    var urut=0;
                    var urut2=0;
                    var tbodyHtml = '';
                    if(filter=="0"){
                        tbodyHtml+='<tr class="bg-primary"><td colspan="3">Rekapitulasi</td></tr>'
                        $.each(response.data, function(index2,el2){
                            urut2++;
                            tbodyHtml += '<tr>'
                            tbodyHtml += '<td></td>'
                            tbodyHtml += '<td><b>'+urut2+'. '+el2.keterangan+'</b></td>'
                            tbodyHtml += '<td align="right">'+formatter.format(el2.credit)+'</td>'

                            tbodyHtml += '</tr>'
                        })
                    }else{
                        $.each(response.data, function(index, el){
                            urut++;
                            urut2=0;
                            tbodyHtml+='<tr class="bg-primary"><td>'+urut+'</td><td colspan="3">'+el.Nama+'</td></tr>'
                            $.each(response.detail[index], function(index2,el2){
                                urut2++;
                                tbodyHtml += '<tr>'
                                tbodyHtml += '<td></td>'
                                tbodyHtml += '<td><b>'+urut2+'. '+el2.keterangan+'</b></td>'
                                tbodyHtml += '<td align="right">'+formatter.format(el2.credit)+'</td>'

                                tbodyHtml += '</tr>'
                            })
                        })
                    }
                    $('.content table #data').html(tbodyHtml);
                    if(response.data.length == 0){
                        $('.content table #data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
                    }
                    // createSession('rekapitulasi',page,take,filter,data);
                },
                error : function(data){
                    toastr['error'](data.statusText);
                }
            })
        }
    </script>