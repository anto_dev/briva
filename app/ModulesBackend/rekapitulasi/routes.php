<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'rekapitulasi', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','rekapitulasicontroller@lihat')->name('lihatrekapitulasi');
	Route::post('/','rekapitulasicontroller@muatData')->name('lihatrekapitulasi');
	Route::get('/tambah','rekapitulasicontroller@tambahData')->name('tambahrekapitulasi');
	Route::post('/tambah','rekapitulasicontroller@simpanData')->name('tambahrekapitulasi');
	Route::get('/hapus/{id}','rekapitulasicontroller@hapusData')->name('hapusrekapitulasi');
	Route::get('/edit/{id}','rekapitulasicontroller@editData')->name('editrekapitulasi');
	Route::post('/edit','rekapitulasicontroller@updateData')->name('editrekapitulasi');
});
		Route::get('/rekapitulasi/downloadexcel/{param?}', 'rekapitulasicontroller@downloadexcel')->name('downloadexcelrekapitulasi');
		Route::get('/rekapitulasi/pdf/{param?}', 'rekapitulasicontroller@downloadpdf')->name('downloadpdfrekapitulasi');
});