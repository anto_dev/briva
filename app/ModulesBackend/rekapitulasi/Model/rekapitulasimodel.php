<?php
namespace App\ModulesBackend\rekapitulasi\Model;

use Illuminate\Database\Eloquent\Model;

class rekapitulasimodel extends Model
{
	protected $table = 'rekapitulasi';
	public $timestamps = false;
}