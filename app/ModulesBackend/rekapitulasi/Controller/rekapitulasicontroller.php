<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\rekapitulasi\Model\rekapitulasiModel;
use App\Custom\CustomClass;
class rekapitulasicontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('rekapitulasi::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		$usedFilter[0] = 'customer_code';
		$usedFilter[2] = 'a.majors';
		$usedFilter[1] = 'b.FakultasID';
		$tanggalawal=substr($request->input('periode'), 0,10);
		$tanggalakhir=substr($request->input('periode'), 13,10);
		$tanggalawal=date('Y-m-d',strtotime($tanggalawal));
		$tanggalakhir=date('Y-m-d',strtotime($tanggalakhir));
		$filter = 'sequence';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];   
		}

		if ($request->input('filter')=="1") {
			$data=DB::connection('sisfo_pmb')->table('fakultas')->select('FakultasID','Nama')->get();
			foreach ($data as $key => $value) {
				$dq[$key]=DB::select("SELECT v_statements.category_id,prodi.FakultasID,fakultas.Nama as filter,bills_category.keterangan,sum(v_statements.credit) as credit FROM v_statements LEFT JOIN sisfo.prodi ON prodi.ProdiID=v_statements.majors LEFT JOIN sisfo.fakultas ON fakultas.FakultasID=prodi.FakultasID LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id WHERE prodi.FakultasID='".$value->FakultasID."' AND DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY prodi.FakultasID,v_statements.category_id,fakultas.Nama,bills_category.keterangan");
			}
		}elseif ($request->input('filter')=="2") {
			$data=DB::connection('sisfo_pmb')->table('prodi')->where('NA','N')->select('ProdiID','Nama')->get();
			foreach ($data as $key => $value) {
				$dq[$key]=DB::select("SELECT v_statements.category_id,v_statements.majors,prodi.Nama as filter,bills_category.keterangan,sum(v_statements.credit) as credit FROM v_statements LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id LEFT JOIN sisfo.prodi ON prodi.ProdiID=v_statements.majors WHERE v_statements.majors='".$value->ProdiID."' AND DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY v_statements.majors,v_statements.category_id,prodi.Nama,bills_category.keterangan");
			}
		}else{
			$key=0;
			$dq=[];
			$data=DB::select("SELECT bills_category.category_id,keterangan,keterangan as filter,sum(credit) as credit FROM v_statements LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id WHERE DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY bills_category.category_id,keterangan");
		}
		$detail=$dq;

		return response()->json(['data' => $data,'detail'=>$detail]);
	}
	function tambahData(Request $request){
		$konten = view("rekapitulasi::tambah")->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$periode=$request->input('periode');
		dd($periode);
	}
	public function downloadexcel(Request $request){
		$tanggalawal=substr($request->input('periode'), 0,10);
		$tanggalakhir=substr($request->input('periode'), 13,10);
		
		if (Helper::validateDate($tanggalawal,'d-m-Y') && Helper::validateDate($tanggalakhir,'d-m-Y')) {
			$tanggalawal=date('Y-m-d',strtotime($tanggalawal));
			$tanggalakhir=date('Y-m-d',strtotime($tanggalakhir));
			if ($request->input('filter')=="1") {
				$filter="Fakultas";
				$data=DB::connection('sisfo_pmb')->table('fakultas')->select('FakultasID','Nama')->get();
				foreach ($data as $key => $value) {
					$dq[$key]=DB::select("SELECT v_statements.category_id,prodi.FakultasID,fakultas.Nama as filter,bills_category.keterangan,sum(v_statements.credit) as credit FROM v_statements LEFT JOIN sisfo.prodi ON prodi.ProdiID=v_statements.majors LEFT JOIN sisfo.fakultas ON fakultas.FakultasID=prodi.FakultasID LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id WHERE prodi.FakultasID='".$value->FakultasID."' AND DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY prodi.FakultasID,v_statements.category_id,fakultas.Nama,bills_category.keterangan");
				}
			}elseif ($request->input('filter')=="2") {
				$filter="Prodi";
				$data=DB::connection('sisfo_pmb')->table('prodi')->where('NA','N')->select('ProdiID','Nama')->get();
				foreach ($data as $key => $value) {
					$dq[$key]=DB::select("SELECT v_statements.category_id,v_statements.majors,prodi.Nama as filter,bills_category.keterangan,sum(v_statements.credit) as credit FROM v_statements LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id LEFT JOIN sisfo.prodi ON prodi.ProdiID=v_statements.majors WHERE v_statements.majors='".$value->ProdiID."' AND DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY v_statements.majors,v_statements.category_id,prodi.Nama,bills_category.keterangan");
				}
			}else{
				$filter="Global";
				$key=0;
				$dq=[];
				$data=DB::select("SELECT bills_category.category_id,keterangan,keterangan as filter,sum(credit) as credit FROM v_statements LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id WHERE DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY bills_category.category_id,keterangan");
			}
			if (count($data)==0) {
				return response()->json('Data tidak tersedia', 403);
			}
			// dd($dq);
			foreach ($data as $key => $value) {
				foreach ($dq[$key] as $key2 => $el) {
					$excelData[] = array(
						"No"=> ++$key,
						"Filter"=> $el->filter,
						"Category"=> $el->keterangan,
						"Credit"=> number_format($el->credit,0,',','.')
					);
				}
				++$key;
			}
			$periode=$tanggalawal.' s.d '.$tanggalakhir;
			return Excel::create('LAPORAN DATA STATEMENT BRIVA', function($excel) use ($excelData,$periode) {

				$excel->sheet('rekapitulasi', function($sheet) use ($excelData,$periode)
				{
					$sheet->mergeCells('A1:D1');
					$sheet->mergeCells('A2:D2');
					$sheet->row(1, function ($row) {
						$row->setFontFamily('Tahoma');
						$row->setFontSize(11);

					});
					$sheet->setColumnFormat(array(
						'D' => '0'
					));
					$sheet->getStyle('A1')->getAlignment()->applyFromArray(
						array('horizontal' => 'center')
					);
					$sheet->getStyle('A2')->getAlignment()->applyFromArray(
						array('horizontal' => 'center')
					);
					$sheet->getStyle('E')->getAlignment()->applyFromArray(
						array('horizontal' => 'right')
					);
					$sheet->getStyle('N')->getAlignment()->applyFromArray(
						array('horizontal' => 'right')
					);
					$styleArray = array(
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						)
					);

					$sheet->getStyle()->applyFromArray($styleArray);
					$sheet->row(1, array('Rekapitulasi '));
					$sheet->row(2, array('PER : '.$periode));
					$sheet->setWidth('A', 6);
					$sheet->setWidth('B', 50);
					$sheet->setWidth('C', 25);
					foreach ($excelData as $dt) {
						$sheet->appendRow($dt);
					}
					$headings = array('No', 'Filter', 'Category','Credit');

					$sheet->prependRow(3, $headings);
					$sheet->cells('A3:D3', function($cells) {
						$cells->setBackground('#009933'); 
						$cells->setFontColor('#ffffff'); 
					});
				});
			})->download('xlsx');
		}else{
			return response('Format periode tanggal tidak valid',409);
		}
		
	}
	function downloadpdf(Request $request){
		$startDate=substr($request->input('periode'), 0,10);
		$endDate=substr($request->input('periode'), 13,10);
		
		if (Helper::validateDate($startDate,'d-m-Y') && Helper::validateDate($endDate,'d-m-Y')) {
			$tanggalawal=date('Y-m-d',strtotime($startDate));
			$tanggalakhir=date('Y-m-d',strtotime($endDate));
			if ($request->input('filter')=="1") {
				$filter="Fakultas";
				$data=DB::connection('sisfo_pmb')->table('fakultas')->select('FakultasID','Nama')->get();
				foreach ($data as $key => $value) {
					$dq[$key]=DB::select("SELECT v_statements.category_id,prodi.FakultasID,fakultas.Nama as filter,bills_category.keterangan,sum(v_statements.credit) as credit FROM v_statements LEFT JOIN sisfo.prodi ON prodi.ProdiID=v_statements.majors LEFT JOIN sisfo.fakultas ON fakultas.FakultasID=prodi.FakultasID LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id WHERE prodi.FakultasID='".$value->FakultasID."' AND DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY prodi.FakultasID,v_statements.category_id,fakultas.Nama,bills_category.keterangan");
				}
			}elseif ($request->input('filter')=="2") {
				$filter="Program Studi";
				$data=DB::connection('sisfo_pmb')->table('prodi')->where('NA','N')->select('ProdiID','Nama')->get();
				foreach ($data as $key => $value) {
					$dq[$key]=DB::select("SELECT v_statements.category_id,v_statements.majors,prodi.Nama as filter,bills_category.keterangan,sum(v_statements.credit) as credit FROM v_statements LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id LEFT JOIN sisfo.prodi ON prodi.ProdiID=v_statements.majors WHERE v_statements.majors='".$value->ProdiID."' AND DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY v_statements.majors,v_statements.category_id,prodi.Nama,bills_category.keterangan");
				}
			}else{
				$filter="Global";
				$key=0;
				$dq=[];
				$data=DB::select("SELECT bills_category.category_id,keterangan,keterangan as filter,sum(credit) as credit FROM v_statements LEFT JOIN bills_category ON bills_category.category_id=v_statements.category_id WHERE DATE(trx_date)>='".$tanggalawal."' AND DATE(trx_date)<='".$tanggalakhir."' GROUP BY bills_category.category_id,keterangan");
			}
			if (count($data)==0) {
				return response()->json('Data tidak tersedia', 403);
			}
			$periode=$startDate.' s.d '.$endDate;
			$pdf = PDF::loadView('rekapitulasi::pdf',['data'=>$data,'detail'=>$dq,'filter'=>$filter,'periode'=>$periode]);
			$pdf->setPaper('A4');
			return $pdf->download('REKAPITULASI_'.$filter.'.pdf');
		}else{
			return response('Format tanggal tidak valid',409);
		}
	}
}
