<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<div class="box box-primary" id="box-primary">
		<div class="box-header with-border">
			<div class="row">
				<div class="col-md-4">
					<button class="btn btn-success btn-sm" id="refresh"><i class="fa fa-refresh"></i></button>
					@autorisasi('tambahinformasi')
					<a class="btn btn-primary btn-sm" id="ajaxlink" href="{{route('tambahinformasi')}}"><i class="fa fa-plus"></i></a>
					@endautorisasi
					@autorisasi('hapusinformasi')
					<button class="btn btn-danger btn-sm" id="hapusCheckedkData" data-url="{{url(env('PREFIX_ADMIN').'/informasi/hapus')}}"><i class="fa fa-trash"></i></button>
					@endautorisasi
				</div>
				<div class="col-md-8">
					<span class="col-md-4">
						<select name="filter" id="filter" class="form-control input-sm">
							<option value="">{{Lang::get('globals.filterSearch')}}</option>
							<option value="0">Nama</option>
						</select>
					</span>
					<span class="col-md-8">
						<div class="input-group">
							<input class="form-control input-sm" type="text" id="data" placeholder="{{Lang::get('globals.placeHolderSearch')}}"></input>
							<span class="input-group-btn">
								<button class="btn btn-default btn-sm" type="button" onclick="muatData()"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</span>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="box table-responsive" id="box-table" class="col-md-12">
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th width="2%">
								<input type="checkbox" id="checkAll" />
							</th>
							<th width="3%">
								No
							</th>
							<th width="10%">
								Fakultas
							</th>
							<th width="12%">
								Program Studi
							</th>
							<th width="12%">
								Akreditasi
							</th>
							<th width="22%">
								Masa Berlaku Akreditasi sampai
							</th>
							<th width="12%">
								Download Uang Kuliah
							</th>
							<th>
								Link Website
							</th>
							@autorisasi(editinformasi|hapusinformasi)
							<th width="10%">
								Action
							</th>
							@endautorisasi
						</tr>
					</thead>
					<tbody id="data">
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<div class="col-md-4">
				<table width="100%">
					<tr>
						<td width="40%">
							{{Lang::get('globals.labelJumlahTampil')}}: 
						</td>
						<td width="20%">
							<select id="take" class="form-control input-sm" onchange="muatData()">
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="50">50</option>
								<option value="100">100</option>
								
							</select>
						</td>
						<td>
							&nbsp; Total : <span id="totalRecord"></span> Data
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<ul class="pagination pagination-sm" style="margin: 0px">
					</ul>
				</div>	
			</div>
		</div>
	</div>
</section>
<style>
	/*input[type='checkbox']{
		appearance: none;
	}*/
</style>
<script>
	$(document).ready(function(){
		var session = readSession();
		if(session){
			filter = session.filter;
			take = session.take;
			page = session.page;
			data = session.data;
			$('#take').val(take);
			$('#data').val(data);
			$('#filter').val(filter);
			muatData(page);
		}
		else{
			muatData();
		}
		$('#refresh').click(function(){
			$('#take').val(5);
			$('#data').val('');
			$('#filter').val('');
			muatData();
		})
	});
	function muatData(page = 1){
		var filter = $('#filter').val();
		var data = $('#data').val();
		var take = $('#take').val();
		$.ajax({
			url : '{{url(env('PREFIX_ADMIN').'/informasi')}}',
			type : 'POST',
			data : {
				_token : token,
				filter : filter,
				data : data,
				page : page,
				take : take
			},
			beforeSend : function(){
				callOverlay('box-table');
			},
			complete : function(){
				removeOverlay();
			},
			success : function(response){
				var activePage = response.activePage;
				var totalPage = response.totalPage;
				var totalRecord = response.totalRecord;
				var status ="";
				var urut=0;
				var tbodyHtml = '';
				$.each(response.data, function(index, el){
					urut++;
					if(el.NA == 'Y'){ 
						status = '<label class="label label-danger">Tidak Aktif</label>';
					}else{
						status = '<label class="label label-warning text-dark">Aktif</label>';
					}
					tbodyHtml += '<tr>'
					tbodyHtml += '<td><input type="checkbox" id="checkRow" data-id="'+el.id+'"/></td>'
					tbodyHtml += '<td>'+urut+'</td>'
					tbodyHtml += '<td>'+el.FakultasID+'</td>'
					tbodyHtml += '<td>'+el.ProdiID+'</td>'
					tbodyHtml += '<td>'+el.akreditasi+'</td>'
					tbodyHtml += '<td>'+el.tgl_akreditasi+'</td>'
					tbodyHtml += '<td> <a target ="__blank" href="'+el.nominal_uk+'"><span class="badge bg-info">Download</span></a> </td>'
					tbodyHtml += '<td> <a target ="__blank" href="'+el.link_website+'"><span class="badge bg-secondary">Link Website</span></a> </td>'
					@autorisasi(editinformasi|hapusinformasi)
					tbodyHtml += '<td>'
					@autorisasi('editinformasi')
					tbodyHtml += '<a href="{{url(env('PREFIX_ADMIN').'/informasi/edit')}}/'+el.id+'" id="ajaxlink" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>'
					@endautorisasi
					@autorisasi('hapusinformasi')
					tbodyHtml += '<button class="btn btn-danger btn-sm" data-url={{url(env('PREFIX_ADMIN').'/informasi/hapus')}} data-id="'+el.id+'" data-page="'+page+'" id="hapusData"><i class="fa fa-trash"></i></button>'
					@endautorisasi
					tbodyHtml += '</td>'
					@endautorisasi
					tbodyHtml += '</tr>' 
				})
				loadPagination('pagination',totalPage, activePage, totalRecord)
				$('.content table #data').html(tbodyHtml);
				if(response.data.length == 0){
					$('.content table #data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
				}
                createSession('informasi',page,take,filter,data);
			},
			error : function(data){
				toastr['error'](data.statusText);
			}
		})
	}
</script>