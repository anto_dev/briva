<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('tambahinformasi')}}" class="form-vertical" method="post" id="frmTambahinformasi">
		{{csrf_field()}}
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="box-body ">
						<div class="col-md-12">
							<div class="form-group" id="groupFakultas">
								<label for="email">Pilih Fakultas </label>
								<select name="faculty_id" id="faculty_id" class="form-control select2 setFakultas">
								</select>
							</div>
							<div class="form-group" id="groupProdi">
								<label for="email">Pilih Program Studi </label>
								<select name="prodi_id" id="prodi_id" class="form-control select2 setProdi">
								</select>
							</div>
							<div class="form-group" id="groupPeriodeBayar">
								<label for="Akreditasi" class="control-label">Akreditasi</label>
									<input type="text" class="form-control input-sm" id="Akreditasi" name="Akreditasi" autocomplete="off">
							</div>
							<div id="groupPeriodeUjian" class="form-group">
								<label>Akreditasi Berlaku sampai dengan</label>
								<div class="input-group">
									<input type="text" name="TanggalAkreditasi" id="TanggalAkreditasi" class="form-control input-sm tanggalakreditasi" title="" readonly="" autocomplete="off">
									<span class="input-group-btn">
										<button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<div class="form-group" id="groupTelitiBayarProdi">
								<label for="LinkUK" class="control-label">Download Link Uang Kuliah</label>
								<input type="text" class="form-control input-sm" id="LinkUK" name="LinkUK" autocomplete="off">
							</div>
							<div class="form-group" id="groupTelitiBayarProdi">
								<label for="LinkWebsite" class="control-label">Link Website</label>
								<input type="text" class="form-control input-sm" id="LinkWebsite" name="LinkWebsite" autocomplete="off">
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatinformasi')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$(document).ready(function(){


		$('#frmTambahinformasi').submit(function(e){
			e.preventDefault();
			var formData = new FormData(this);
			
			$.ajax({
				url:"{{ route('tambahinformasi') }}",
				method:"POST",
				data:formData,
				dataType:'JSON',
				contentType: false,
				cache: false,
				processData: false,
				beforeSend : function(){
					removeError();
					callOverlay('box-primary');
				},
				complete : function(){
					removeOverlay();
				},
				error : function(data){
					//console.log(data.responseText)
					if(data.status == 409){
						toastr['error'](data.responseText);
					}else if(data.status == 500){
						toastr['error'](data.responseText);
					}else if(data.status == 400){
						showError(data.responseJSON);
						toastr['error']('File upload tidak boleh kosong');
					}else{
						toastr['error'](data.responseText);
					}
				},
				success : function(data){
					//console.log(data)
					toastr['success']('Data {{Lang::get('globals.notifSuksesSimpan')}}');
					nextAction(data);
				}
			})
		});

		$("#faculty_id").select2({
			ajax: {
				url:"{{URL::to('')}}/faculty",
				dataType: "JSON",
				delay: 250,
				data: function(params) {
					return {
						NamaPanjang: params.term
					};
				},
				processResults: function(data) {
					var results = [];
					$.each(data, function(index, item) {
						results.push({
							id: item.FakultasID,
							text: item.NamaPanjang
						})
					});
					// console.log(results)
					return {
						results: results
					};
					
				},
				cache: true
			}
		});
		
		$("#faculty_id").change(function(e){
			var faculty=$(this).val();
			$("#prodi_id").val("")
			getProdi(faculty)
		})
	})
	
	function getProdi(faculty){
		console.log(faculty)
		$("#prodi_id").select2({
			ajax: {
				url:"{{URL::to('')}}/prodi/"+faculty,
				dataType: "JSON",
				delay: 250,
				data: function(params) {
					return {
						NamaPanjang: params.term
					};
				},
				processResults: function(data) {
					var results = [];
					$.each(data, function(index, item) {
						results.push({
							id: item.ProdiID,
							text: item.Nama
						})
					});
					console.log(results)
					return {
						results: results
					};
					
				},
				cache: true
			}
		});
	}
	function showError(responseJSON){
		console.log(responseJSON)
		$.each(responseJSON, function(index, el){
			$('#group'+index).addClass('has-error');
			$.each(el, function(index1, el1){
				$('#group'+index).append('<span class="help-block with-errors">'+el1+'</span>');
			})
		})
	}
	function removeError(){
		$('body .has-error').removeClass('has-error');
		$('body .with-errors').remove();
	}

	$(document).ready(function(){
		setPeriodeY();
	})

	function nextAction(data){
		$('form')[0].reset();
		setPeriodeY();
	}
	function setPeriodeY(){
		$('.tanggalakreditasi').datepicker({
			minDate: new Date(),
			startDate: new Date(),
			autoClose:true,
			locale: {
				format: 'yyyy-MM-dd'
			}
		})
	}
</script>