<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'informasi', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','informasicontroller@lihat')->name('lihatinformasi');
	Route::post('/','informasicontroller@muatData')->name('lihatinformasi');
	Route::get('/tambah','informasicontroller@tambahData')->name('tambahinformasi');
	Route::post('/tambah','informasicontroller@simpanData')->name('tambahinformasi');
	Route::get('/hapus/{id}','informasicontroller@hapusData')->name('hapusinformasi');
	Route::get('/edit/{id}','informasicontroller@editData')->name('editinformasi');
	Route::post('/edit','informasicontroller@updateData')->name('editinformasi');
});
});