<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\informasi\Model\informasiModel;
use App\Custom\CustomClass;
class informasicontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('informasi::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		
		$usedFilter[0] = 'tb_informasiweb.FakultasID';

		$filter = 'tb_informasiweb.FakultasID';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];	
		}

		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');
		
		$skippedData = ($page - 1) * $take;
		$totalJumlah = informasiModel::where($filter , 'LIKE' ,'%'.$data.'%')->orderBy('id','desc')
		->count();
		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = informasiModel::where($filter , 'LIKE' ,'%'.$data.'%')->orderBy('update_at','desc')
		->take($take)
		->select('tb_informasiweb.*')
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$data = informasiModel::get();
		$konten = view("informasi::tambah",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'faculty_id' => 'required',
			'prodi_id' => 'required',
			'Akreditasi' => 'required',
			'TanggalAkreditasi' => 'required',
			'LinkUK' => 'required',
			'LinkWebsite' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}

		$informasi = new informasiModel;
		$informasi->FakultasID = $request->input('faculty_id');
		$informasi->ProdiID = $request->input('prodi_id');
		$informasi->akreditasi = $request->input('Akreditasi');
		$informasi->tgl_akreditasi = $request->input('TanggalAkreditasi');
		$informasi->nominal_uk = $request->input('LinkUK');
		$informasi->link_website = $request->input('LinkWebsite');
		$informasi->update_at=date('Y-m-d H:i:s');
		$informasi->created_by=Auth::guard('Admin')->user()->username;
		
		$informasi->save();
		return response()->json(1, 200);
	}
	function updateData(Request $request){
		$rules = [
			'faculty_id' => 'required',
			'prodi_id' => 'required',
			'Akreditasi' => 'required',
			'TanggalAkreditasi' => 'required',
			'LinkUK' => 'required',
			'LinkWebsite' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}

		informasiModel::where('id',$request->input('id'))->update([
			'FakultasID' =>$request->input('faculty_id'),
			'ProdiID' =>$request->input('prodi_id'),
			'akreditasi' =>$request->input('Akreditasi'),
			'nominal_uk' =>$request->input('LinkUK'),
			'link_website' =>$request->input('LinkWebsite')
		]);
		
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){

		$data = informasiModel::where('id',$id)->first();
		$konten = view("informasi::edit",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function hapusData(Request $request, $id){
		$cek = informasiModel::where('id',$id);

		if($cek->count() == 0 ){
			return response()->json('Data tidak tersedia', 403);
		}
		$cek->delete();
	}
}
