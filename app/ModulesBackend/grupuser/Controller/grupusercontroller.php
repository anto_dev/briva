<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\grupuser\Model\grupuserModel;
use App\Custom\CustomClass;
class grupusercontroller  extends Controller
{
	public function index(Request $request)
	{

		$konten=view("grupuser::view")->render();

		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten'=>$konten]);
	}
	function muatData(Request $request){

		$usedFilter[0] = 'namaGrupUser';

		$filter = 'id';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];   
		}

		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');

		$skippedData = ($page - 1) * $take;


		$totalJumlah = grupuserModel::where($filter , 'LIKE' ,'%'.$data.'%')->count();

		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = grupuserModel::where($filter , 'LIKE' ,'%'.$data.'%')->orderBy('id','desc')
		->take($take)
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$data = grupuserModel::get();
		$konten = view("grupuser::tambah",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'GrupUser' => 'required',
			'Status'=>'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$grupuser = new grupuserModel;
		$grupuser->namaGrupUser = $request->input('GrupUser');
		$grupuser->status = $request->input('Status');
		$grupuser->save();
		return response()->json(1, 200);
	}
	function updateData(Request $request){
		$rules = [
			'GrupUser' => 'required',
			'Status'=>'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$grupuser = grupuserModel::find($request->input('id'));
		$grupuser->namaGrupUser = $request->input('GrupUser');
		$grupuser->status = $request->input('Status');
		$grupuser->save();
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){
		$data = grupuserModel::find($id);
		$konten = view("grupuser::edit",['data' =>$data ])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function hapusData(Request $request, $id){
		$cek = grupuserModel::find($id);
		if(empty($cek)){
			return response()->json('Data tidak tersedia', 403);
		}
		$cek->delete();
	}
}
