<?php 
Route::group(['prefix' => env('PREFIX_ADMIN')], function() {
    Route::group(['prefix' => 'grupuser', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','grupusercontroller@index')->name('lihatgrupuser');
	Route::post('/','grupusercontroller@muatData')->name('lihatgrupuser');
	Route::get('/tambah','grupusercontroller@tambahData')->name('tambahgrupuser');
	Route::post('/tambah','grupusercontroller@simpanData')->name('tambahgrupuser');
	Route::get('/hapus/{id}','grupusercontroller@hapusData')->name('hapusgrupuser');
	Route::get('/edit/{id}','grupusercontroller@editData')->name('editgrupuser');
	Route::post('/edit','grupusercontroller@updateData')->name('editgrupuser');
});
});