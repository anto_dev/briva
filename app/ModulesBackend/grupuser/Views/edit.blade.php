<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
				<form action="{{route('editgrupuser')}}" class="form-vertical" method="post" id="frmEditgrupuser">
					{{csrf_field()}}
					<input type="hidden" name="id" value="{{$data->id}}">
					<div class="box box-primary" id="box-primary">
						<div class="box-body">
							<div class="row">

								<div class="col-md-9">

									<div class="form-group" id="groupGrupUser">
										<label for="GrupUser" class="control-label">Grup User :</label>

										<input type="text" class="form-control input-sm" id="GrupUser" name="GrupUser" value="{{$data->namaGrupUser}}">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group" id="groupStatus">
										<label>
											Status :
										</label>
										<select name="Status" id="Status" class="form-control input-sm">
											<option value="N" @if ($data->status=='N')
												selected 
												@endif>Tidak Aktif</option>
												<option value="Y" @if ($data->status=='Y')
													selected 
													@endif>Aktif</option>
												</select>
											</div>	
										</div>
									</div>
								</div>
								<div class="box-footer" align="right">
									<a class="btn btn-default btn-sm btnKembali" id="ajaxlink" href="{{route('lihatgrupuser')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
									<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<script>
			$('#frmEditgrupuser').submit(function(e){
				e.preventDefault();
				konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
			})
			function nextAction(data){
				$('.btnKembali').trigger('click');
			}
		</script>