<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
				<form action="{{route('tambahgrupuser')}}" class="form-vertical" method="post" id="frmTambahGrupUser">
					{{csrf_field()}}
					<div class="box box-primary" id="box-primary">
						<div class="box-body">
							<div class="row">
								<div class="col-md-9">
									<div class="form-group" id="groupGrupUser">
										<label for="GrupUser" class="control-label">Grup User :</label>

										<input type="text" class="form-control input-sm" id="GrupUser" name="GrupUser">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group" id="groupStatus">
										<label>
											Status :
										</label>
										<select name="Status" id="Status" class="form-control input-sm">
											<option value="N">Tidak Aktif</option>
											<option value="Y">Aktif</option>
										</select>
									</div>	
								</div>
							</div>
							<div class="box-footer" align="right">
								<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatgrupuser')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
								<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
							</div>
						</div>
					</form>
				</div>
			</form>
		</div>
	</div>
</div>
			</section>
			<script>
				$('#frmTambahGrupUser').submit(function(e){
					e.preventDefault();
					konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
				})
				function nextAction(data){
					$('form')[0].reset();
				}
			</script>