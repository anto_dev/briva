
<table class="table table-bordered table-striped">
	<thead>
		<tr>

		@if(!isset($browse))
			<th width="1%">
				<input type="checkbox" class="minimal" name="" id="checkall-baris">
			</th>
		@endif
			<th>
				Kode Cabang
			</th>
			<th>
				Nama Cabang
			</th>
			<th>
				Kota
			</th>
			<th>
				Telepon / HP
			</th>
			<th>
				Pimpinan
			</th>
			<th>
				Admin
			</th>
			<th width="10%">
				Action
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $dt)
		<tr>

		@if(!isset($browse))
			<td>
				<input type="checkbox" class="minimal" name="" id="check-baris" value="{{$dt->id}}">
			</td>
		@endif
			<td>
				{{$dt->kodecabang}}
			</td>
			<td>
				{{$dt->namacabang}}
			</td>
			<td>
				{{$dt->kota}}
			</td>
			<td>
				{{$dt->telp}} / {{$dt->hp}}
			</td>
			<td>
				{{$dt->pimpinancabang}}
			</td>
			<td>
				{{$dt->admin}}
			</td>
			<td>

		@if(!isset($browse))
				<a class="btn btn-sm btn-warning btn-flat menulink" href="{{URL::to('cabang/edit')}}/{{$dt->id}}" title="Edit Cabang"><i class="fa fa-pencil"></i></a>
				<a class="btn btn-sm btn-danger btn-flat" onclick="hapus_cabang('{{$dt->id}}')"><i class="fa fa-trash"></i></a>
		@else

		<button class="btn btn-sm btn-default btn-flat col-md-12" onclick="pilih_cabang('{{$dt->kodecabang}}')">Pilih</button>
		@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $data->links() }}