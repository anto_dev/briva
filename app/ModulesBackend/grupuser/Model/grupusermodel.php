<?php
namespace App\ModulesBackend\grupuser\Model;

use Illuminate\Database\Eloquent\Model;

class grupusermodel extends Model
{
	protected $table = 'tb_grupuser';
	public $timestamps = false;
}