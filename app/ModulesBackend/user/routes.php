<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin','autorisasi']], function(){
Route::group(['prefix' => 'user', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','usercontroller@lihat')->name('lihatuser');
	Route::post('/','usercontroller@muatData')->name('lihatuser');
	Route::get('/tambah','usercontroller@tambahData')->name('tambahuser');
	Route::post('/tambah','usercontroller@simpanData')->name('tambahuser');
	Route::get('/hapus/{id}','usercontroller@hapusData')->name('hapususer');
	Route::get('/edit/{id}','usercontroller@editData')->name('edituser');
	Route::post('/edit','usercontroller@updateData')->name('edituser');
});
});