<?php
namespace App\ModulesBackend\user\Model;

use Illuminate\Database\Eloquent\Model;

class usermodel extends Model
{
	protected $table = 'users';
	public $timestamps = false;
}