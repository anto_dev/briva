<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					Data table
				</h6>
			</div>
			<div class="card-body">
			<form action="{{route('edituser')}}" class="form-vertical" method="post" id="frmEdituser">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{$data->id}}">
				<div class="box box-primary" id="box-primary">
					<div class="box-body">
						<div class="row">

							<div class="col-md-6">
								<div class="form-group" id="groupNama">
									<label for="Nama" class="control-label">Nama :</label>

									<input type="text" class="form-control input-sm" id="Nama" name="Nama" value="{{$data->name}}">
								</div>
								<div class="form-group" id="groupUsername">
									<label for="Username" class="control-label">Username :</label>

									<input type="text" class="form-control input-sm" id="Username" name="Username" autocomplete="off" value="{{$data->username}}" disabled="">
								</div>
								<div class="form-group" id="groupPassword">
									<label for="Password" class="control-label">Password :</label>
									<input type="password" class="form-control input-sm" id="Password" name="Password" autocomplete="off">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" id="groupGrupUser">
									<label for="GrupUser" class="control-label">Grup User :</label>
									<select name="GrupUser" id="GrupUser" class="form-control input-sm">
										@foreach($dataGrupUser as $itemGrupUser)
										<option value="{{$itemGrupUser->id}}" @if($data->grupUser == $itemGrupUser->id) selected @endif>{{$itemGrupUser->namaGrupUser}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group" id="groupStatus">
									<label for="Status" class="control-label">Status :</label>
									<select name="Status" id="Status" class="form-control input-sm">
										@foreach(Lang::get('globals.arrayStatus')  as $key => $itemStatus)
										<option value="{{$key}}" @if($data->status == $key) selected @endif>{{$itemStatus}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer" align="right">
						<a class="btn btn-default btn-sm btnKembali" id="ajaxlink" href="{{route('lihatuser')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
						<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	</div>
</section>
<script>
	$('#Icon').keyup(function(e){
		e.preventDefault();
		$('#iconPreview').attr('class','fa fa-'+$(this).val());
	});
	$('#frmEdituser').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	function nextAction(data){
		$('.btnKembali').trigger('click');
	}
</script>