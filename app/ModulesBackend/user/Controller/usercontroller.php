<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\user\Model\userModel;
use App\ModulesBackend\grupuser\Model\grupuserModel;
use App\Custom\CustomClass;
class usercontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('user::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		
		$usedFilter[0] = 'users.name';
		$usedFilter[1] = 'users.username';
		$usedFilter[2] = 'tb_grupuser.namaGrupUser';

		$filter = 'users.id';
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];	
		}

		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');
		
		$skippedData = ($page - 1) * $take;
		

		$totalJumlah = userModel::where($filter , 'LIKE' ,'%'.$data.'%')
		->join('tb_grupuser','tb_grupuser.id','=','users.grupUser')
		->orderBy('id','desc')
		->count();

		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = userModel::where($filter , 'LIKE' ,'%'.$data.'%')
		->join('tb_grupuser','tb_grupuser.id','=','users.grupUser')
		->orderBy('id','desc')
		->take($take)
		->select('users.*','tb_grupuser.namaGrupUser as grupUser')
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$data = userModel::get();
		$grupUser = grupuserModel::get();
		$konten = view("user::tambah",['data' =>$data, 'dataGrupUser' => $grupUser])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$rules = [
			'GrupUser' => 'required',
			'Username' => 'required|unique:users,username|min:8',
			'Password' => 'required',
			'Nama' => 'required',
			'GrupUser' => 'required',
			'Status' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		
		$message = [
			'required' => Lang::get('globals.required').' :attribute',
			'unique' => ':attribute '.Lang::get('globals.unique'),
			'exists' => ':attribute '.Lang::get('globals.exists')
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$idmodul='';
		$cekotorisasi=DB::table('tb_otorisasi')->where('grupUser',$request->input('GrupUser'))->get();
		if (!empty($cekotorisasi)) {
			foreach ($cekotorisasi as $key => $value) {
				if ($key==0) {
					$idmodul=$value->idModul;
				}else{
					$idmodul=$idmodul.','.$value->idModul;
				}
			}
		}
		$avatar='avatar5.png';
		$user = new userModel;
		$user->name = $request->input('Nama');
		$user->username = $request->input('Username');
		$user->grupUser = $request->input('GrupUser');
		$user->password = bcrypt($request->input('Password'));
		$user->status = $request->input('Status');
		$user->idmodul = $idmodul;
		$user->avatar = $avatar;
		// $user->type = $type;
		$user->save();
		return response()->json(1, 200);
	}
	function updateData(Request $request){
		$rules = [
			'Nama' => 'required',
			'GrupUser' => 'required',
			'Status' => 'required',
			'Password' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$user = userModel::find($request->input('id'));
		$user->name = $request->input('Nama');
		$user->grupUser = $request->input('GrupUser');
		// $user->type = $type;
		if($request->input('Password') != ''){
			$user->password = bcrypt($request->input('Password'));
		}
		$user->status = $request->input('Status');
		$user->save();
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){

		$data = userModel::find($id);
		$grupUser = grupuserModel::get();

		$konten = view("user::edit",['data' =>$data ,'dataGrupUser' => $grupUser])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function hapusData(Request $request, $id){
		$cek = userModel::find($id);
		if(empty($cek)){
			return response()->json('Data tidak tersedia', 403);
		}
		$cek->delete();
	}
}
