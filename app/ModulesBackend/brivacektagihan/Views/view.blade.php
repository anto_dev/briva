<section class="content-header">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Table</li>
        </ol>
    </nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
        <div class="card" id="box-primary">
            <div class="card-body">
                <h6 class="card-title">
                    {{Helper::showTitle()}}
                </h6>
			<div class="row">               
				<div class="col-md-12">
					<div class="form-group" id="groupdata">
						<label>Kode Briva</label>
						<div class="input-group col-6">
							<input class="form-control" id="data" name="data" autocomplete="off"></input>
							<span class="input-group-btn">
								<button class="btn btn-success form-control" type="button" onclick="muatData()">
									 Tampilkan
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<div class="box table-responsive table-condensed" id="box-table" class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							
						</div>
						<div class="panel-body">
							{{-- <div class="alert alert-success" role="alert">
								<h4 class="alert-heading">Well done!</h4>
								<p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
								<hr>
								<p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
							</div> --}}

							<div class="alert alert-info notifempty" style="display: none;">
								<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"> <strong>Maaf! NPM</strong> yang anda cari tidak ditemukan!
							</div>
							<div class="row data">
								<div class="col-lg-12">
									<div class="box-body">
										<div class="small-box bg-aqua">
											<div class="inner">
												<label>Kode BRIVA :</label>
												<h3 id="kodebriva"></h3>
												<dl>
													{{-- <dt>Nama Member</dt> --}}
													<dd id="namamember"></dd>
												</dl>
											</div>
											<div class="icon">
												<i class="ion ion-pie-graph"></i>
											</div>
										</div>
										<dl>
										</dl>
										<div class="pointin">
											<h4>Detail Informasi Tagihan BRIVA</h4>
											<table class="table table-condensed table-hover table-bordered">
												<thead>
													<tr>
														<th>Nama</th>
														<th>Keterangan</th>
														<th>Amount</th>
														<th>Expired Date</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody id="datatagihan">

												</tbody>
											</table>
										</div>
									</div> 
								</div>                           

							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="col-md-4">

					</div>
					<div class="col-md-8">

					</div>
				</div>
			</div>
			</div>
		</section>
		<style>
			input[type='checkbox']{
				appearance: none;
			}
		</style>
		<script>
			$(document).ready(function(){
				$(".data").hide();
				$('body #data').keypress(function (e) {
					var key = e.which;
 if(key == 13)  // the enter key code
 {
 	muatData();
 }
});
			});
			function muatData(page = 1){
				var data = $('#data').val();
				$.ajax({
					url : '{{url(env('PREFIX_ADMIN'))}}/brivacektagihan',
					type : 'POST',
					data : {
						_token : token,
						data : data
					},
					beforeSend : function(){
						callOverlay('box-table');
					},
					complete : function(){
						removeOverlay();
					},
					success : function(response){
						console.log(response.data)
						console.log(response.data.data.CustCode)
						var status='';
						if(response.data.data.statusBayar=='Y'){
							status='<span class="badge badge-success">Sudah dibayar</span>';
						}else{
							status='<span class="badge badge-primary text-white">Dapat dibayar</span>';
						}
						$('.notiflanding').css('display','none');
							$('.notifempty').hide();
						$(".data").show();
						$("#kodebriva").text(response.data.data.CustCode);
						var tbodyHtml='';
						tbodyHtml += '<tr>'
						tbodyHtml += '<td>'+response.data.data.Nama+'</td>'
						tbodyHtml += '<td>'+response.data.data.Keterangan+'</td>'
						tbodyHtml += '<td>Rp. '+formatter.format(response.data.data.Amount)+'</td>'
						tbodyHtml += '<td>'+response.data.data.expiredDate+'</td>'
						tbodyHtml += '<td>'+status+'</td>'
						tbodyHtml += '</tr>'
						console.log(tbodyHtml)
						$('.pointin table #datatagihan').html(tbodyHtml);
						if(response.data.length == 0){
							$('.data').hide();
							$('.notifempty').show();
						}
					},
					error : function(data){
						toastr['error'](data.responseText);
						$('.data').hide();
							$('.notifempty').show();
					}
				})
			}
		</script>