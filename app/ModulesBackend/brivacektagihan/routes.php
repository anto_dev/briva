<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'brivacektagihan', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','brivacektagihancontroller@lihat')->name('lihatbrivacektagihan');
	Route::post('/','brivacektagihancontroller@muatData')->name('lihatbrivacektagihan');
	Route::get('/tambah','brivacektagihancontroller@tambahData')->name('tambahbrivacektagihan');
	Route::post('/tambah','brivacektagihancontroller@simpanData')->name('tambahbrivacektagihan');
	Route::get('/hapus/{id}','brivacektagihancontroller@hapusData')->name('hapusbrivacektagihan');
	Route::get('/edit/{id}','brivacektagihancontroller@editData')->name('editbrivacektagihan');
	Route::post('/edit','brivacektagihancontroller@updateData')->name('editbrivacektagihan');
});
});