<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\brivacektagihan\Model\brivacektagihanModel;
use App\Custom\CustomClass;
class brivacektagihancontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('brivacektagihan::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		$rules = [
			'data' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response('Kesalahan! kode briva tidak valid', 400);
		}
		$data = $request->input('data');
		$response=Helper::q_briva_cek($data);
		$status=$response['status'];
		if ($status) {
			return response()->json(['data' => $response],200);
		}else{
			return response()->json($response['errDesc'],409);
		}
	}
	function tambahData(Request $request){
		$data = brivacektagihanModel::get();
		$konten = view("brivacektagihan::tambah",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$PMBID=$request->input('PMBID');
		$cek=brivacektagihanModel::where('PMBID',$PMBID)->first();
		if (empty($cek)) {
			return response('Data tidak ditemukan',409);
		}
		$kodebriva=$cek->kodebriva;
		$response=Helper::q_briva_status($kodebriva);
		$status=$response['status'];
		if ($status) {
			$message=$response['responseDescription'];
			$code=200;
		}else{
			$message=$response['errDesc'];
			$code=409;
		}
		return response($message, $code);
	}
	function updateData(Request $request){
		$rules = [
			'id' => 'required|exists:pmb,PMBID',
			'Nominal' => 'required|numeric',
			'Keterangan' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$id=$request->input('id');
		$cek=brivacektagihanModel::where('PMBID',$id)->first();
		$kodebriva=$cek->kodebriva;
		$nama=$cek->Nama;
		$nominal=$request->input('Nominal');
		$keterangan=$request->input('Keterangan');
		$briva=Helper::q_briva_update($kodebriva,$nama,$nominal,$keterangan);
		brivacektagihanModel::where('PMBPeriodID',$request->input('id'))->update([
			'UPemb' => $keterangan,
			'JumlahPUBank' => $nominal,
			'TotalSetoranMhsw' => $nominal,
			'response' => 3
		]);
		DB::table('log_briva')->insert([
			'reference'=>'update',
			'payload'=>json_encode($briva),
			'created_ip'=>$request->ip()
		]);
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){

		$data = brivacektagihanModel::where('PMBID',$id)->first();
		$konten = view("brivacektagihan::edit",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function hapusData(Request $request, $id){
		$cek = brivacektagihanModel::where('PMBID',$id)->first();
		if($cek->count() == 0 ){
			return response()->json('Data tidak tersedia', 403);
		}
		$kodebriva=$cek->kodebriva;
		$briva=Helper::q_briva_delete($kodebriva);
		DB::table('log_briva')->insert([
			'reference'=>'delete',
			'payload'=>json_encode($briva),
			'created_ip'=>$request->ip()
		]);
		$status=$briva['status'];
		if(!$status){
			$message=$briva['errDesc'];
			return response($message,409);
		}else{
			brivacektagihanModel::where('PMBID',$id)->update([
				'response'=>0,
				'kodebriva'=>'',
				'UPemb'=>null,
				'JumlahPUBank'=>null,
				'TotalSetoranMhsw'=>null
			]);
			return true;
		}
		// $cek->delete();
	}
}
