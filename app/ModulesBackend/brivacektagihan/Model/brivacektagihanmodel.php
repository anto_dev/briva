<?php
namespace App\ModulesBackend\brivacektagihan\Model;

use Illuminate\Database\Eloquent\Model;

class brivacektagihanmodel extends Model
{
	protected $table = 'pmb';
	public $timestamps = false;
}