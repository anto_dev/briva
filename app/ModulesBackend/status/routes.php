<?php 

Route::group(['prefix'=>'admin'], function(){

	Route::group(['prefix' => 'status', 'middleware' => ['web', 'Admin','autorisasi']], function() {
		Route::get('/', 'statuscontroller@index')->name('status');
		Route::post('/','statuscontroller@muatData')->name('lihatstatus');
		Route::get('/tambah','statuscontroller@tambahData')->name('tambahstatus');
		Route::post('/tambah', 'statuscontroller@simpanData')->name('simpanstatus');
		Route::get('/edit/{id}','statuscontroller@editData')->name('editstatus');
		Route::post('/update', 'statuscontroller@updateData')->name('updatestatus');
		Route::get('hapus/{id}','statuscontroller@hapusData')->name('hapusstatus');

	});



	

})

?>