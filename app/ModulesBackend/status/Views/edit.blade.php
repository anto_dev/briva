<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('updatestatus')}}" class="form-vertical" method="post" id="frmEditStatus">
		{{csrf_field()}}
		<input type="hidden" name="id" value="{{$data->id}}">
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="col-md-9">
						<div class="form-group" id="group-KodeStatus">
							<label>
								Kode Status :
							</label>
							<input class="form-control" name="KodeStatus" type="text" id="KodeStatus" value="{{$data->kodestatus}}" readonly="" />
						</div>
						<div class="form-group" id="groupNamaStatus">
							<label>
								Nama Status :
							</label>
							<input class="form-control" name="NamaStatus" type="text" id="NamaStatus" value="{{$data->namastatus}}" />
						</div>
						
					</div>
					
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default" id="ajaxlink" href="{{route('lihatstatus')}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$('#frmEditStatus').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	function nextAction(data){
		window.location.replace('{{route('lihatstatus')}}');
	}
</script>