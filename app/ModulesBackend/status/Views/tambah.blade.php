<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('simpanstatus')}}" class="form-vertical" method="post" id="frm-tambah-modul">
		{{csrf_field()}}
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="col-md-9">
						<div class="form-group" id="groupKodeStatus">
							<label>
								Kode Status :
							</label>
							<input class="form-control" name="KodeStatus" type="text" id="KodeStatus" />
						</div>
						<div class="form-group" id="groupNamaStatus">
							<label>
								Nama Status :
							</label>
							<input class="form-control" name="NamaStatus" type="text" id="NamaStatus">
						</div>
						
					</div>
					
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default" id="ajaxlink" href="{{route('status')}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$('#KodeStatus').keyup(function(e){
		e.preventDefault();
		var str=$("#KodeStatus").val();
		$('#KodeStatus').val(str.toUpperCase());
	});
	$('#frm-tambah-modul').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('form')[0].reset();
	}
</script>