<section class="content-header">
    <h1 id="title-content">
       {{Helper::showTitle()}}
    </h1>
</section>
<section class="content">
    <div class="box box-primary" id="box-primary">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-4">
                    <a class="btn btn-success" id="refresh"><i class="fa fa-refresh"></i></a>
                   
                    <a class="btn btn-primary" id="ajaxlink" href="{{route('tambahstatus')}}"><i class="fa fa-plus"></i></a>
                    
                    <button class="btn btn-danger" id="hapusCheckedkData" data-url="{{url(env('PREFIX_ADMIN').'/status/hapus')}}"><i class="fa fa-trash"></i></button>
                    
                </div>
                <div class="col-md-8">
                    <span class="col-md-4">
                        <select name="filter" id="filter" class="form-control">
                            <option value=""></option>
                            <option value="0">Nama status</option>
                            <option value="1">Kode status</option>
                        </select>
                    </span>
                    <span class="col-md-8">
                        <div class="input-group">
                            <input class="form-control" type="text" id="data" placeholder=""></input>
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="muatData()"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="box table-responsive" id="box-table" class="col-md-12">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" id="checkAll" />
                            </th>
                            <th width="10%">
                                Kode Status
                            </th>
                            <th width="40%">
                                Nama Status
                            </th>
                            
                            <th width="7%">
                                Action
                            </th>
                           
                        </tr>
                    </thead>
                    <tbody id="data">

                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            <div class="col-md-4">
                <table width="100%">
                    <tr>
                        <td width="40%">
                            {{Lang::get('globals.labelJumlahTampil')}}: 
                        </td>
                        <td width="20%">
                            <select id="take" class="form-control input-sm" onchange="muatData()">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </td>
                        <td>
                            &nbsp; Total : <span id="totalRecord"></span> Data
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-8">
                <div class="pull-right">
                    <ul class="pagination" style="margin: 0px">
                    </ul>
                </div>  
            </div>
        </div>
    </div>
</section>
<style>
    input[type='checkbox']{
        appearance: none;
    }
</style>
<script>
    $(document).ready(function(){
        muatData();
        $('#refresh').click(function(){
            $('#take').val(5);
            $('#data').val('');
            $('#filter').val('');
            muatData();
        })
    });
    function muatData(page = 1){
        var filter = $('#filter').val();
        var data = $('#data').val();
        var take = $('#take').val();
        $.ajax({
            url : '{{url(env('PREFIX_ADMIN'))}}/status',
            type : 'POST',
            data : {
                _token : token,
                filter : filter,
                data : data,
                page : page,
                take : take
            },
            beforeSend : function(){
                callOverlay('box-table');
            },
            complete : function(){
                removeOverlay();
            },
            success : function(response){
                var activePage = response.activePage;
                var totalPage = response.totalPage;
                var totalRecord = response.totalRecord;

                var tbodyHtml = '';
                $.each(response.data, function(index, el){
                    tbodyHtml += '<tr>'
                    tbodyHtml += '<td><input type="checkbox" id="checkRow" data-id="'+el.kode+'"/></td>'
                    tbodyHtml += '<td>'+el.kodestatus+'</td>'
                    tbodyHtml += '<td>'+el.namastatus+'</td>'
                    tbodyHtml += '<td>'
                    tbodyHtml += '<a href="{{url(env('PREFIX_ADMIN').'/status/edit')}}/'+el.id+'" id="ajaxlink" class="col-md-6 btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>'
                    tbodyHtml += '<button class="col-md-6 btn btn-danger btn-sm" data-url={{url(env('PREFIX_ADMIN').'/status/hapus')}} data-id="'+el.id+'" data-page="'+page+'" id="hapusData"><i class="fa fa-trash"></i></button>'
                    
                    tbodyHtml += '</td>'
                    
                    tbodyHtml += '</tr>'
                })
                loadPagination('pagination',totalPage, activePage, totalRecord)
                $('.content table #data').html(tbodyHtml);
                if(response.data.length == 0){
                    $('.content table #data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
                }
            },
            error : function(data){
                toastr['error'](data.statusText);
            }
        })
    }
</script>