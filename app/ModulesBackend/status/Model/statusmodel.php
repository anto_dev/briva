<?php
namespace App\ModulesBackend\status\Model;

use Illuminate\Database\Eloquent\Model;

class statusmodel extends Model
{
	protected $table = 'tb_status';
	public $timestamps = false;
}