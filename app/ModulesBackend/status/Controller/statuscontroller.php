<?php

// namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\ModulesBackend\status\Model\statusmodel;
use Illuminate\Http\Request;

class statuscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $konten=view("status::view")->render();

        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }
        return view('layouts.backend.app',['konten'=>$konten]);
    }
    function muatData(Request $request){



        $filter = 'tb_status.id';
        if($request->input('filter') != ''){
            $filter = $usedFilter[$request->input('filter')];   
        }

        $data = $request->input('data');
        $take = $request->input('take');
        $page = $request->input('page');
        
        $skippedData = ($page - 1) * $take;
        

        $totalJumlah = statusmodel::where($filter , 'LIKE' ,'%'.$data.'%');
        // $totalJumlah = Aksesdata::allowedData($totalJumlah);
        $totalJumlah = $totalJumlah->select('tb_status.*')
        ->count();

        $totalPage = $totalJumlah / $take;

        $totalPage = ceil($totalPage);

        $data = statusmodel::where($filter , 'LIKE' ,'%'.$data.'%');
        // $data = Aksesdata::allowedData($data);
        $data = $data->select('tb_status.*')
        ->take($take)
        ->skip($skippedData)
        ->get();

        return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    function tambahData(Request $request){
        $konten = view("status::tambah")->render();
        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => $request->route()->getAction()['as']],200);
        }
        return view('layouts.backend.app',['konten' => $konten,'title' => $request->route()->getAction()['as']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpanData(Request $request)
    {
         $rules=[
        'KodeStatus' => 'required|unique:tb_status,kodestatus',
        'NamaStatus' => 'required',
        ];
        $message = [
            'unique' => ' :attribute '.Lang::get('globals.unique'),
            'required' => Lang::get('globals.required').' :attribute'
        ];
        $validator = Validator::make($request->input(), $rules, $message);
        if($validator->fails()){
            $error = $validator->errors()->messages();
            return response()->json($error, 400);
        }
        $status = new statusmodel;
        $status->kodestatus = $request->input('KodeStatus');
        $status->namastatus = $request->input('NamaStatus');
        $status->save();

        return response()->json(1, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateData(Request $request)
    {
        $rules = [
        'NamaStatus' => 'required',
        ];
        $message = [
            'required' => Lang::get('globals.required').' :attribute'
        ];
        $validator = Validator::make($request->input(), $rules, $message);
        if($validator->fails()){
            $error = $validator->errors()->messages();
            return response()->json($error, 400);
        }
        $status = statusmodel::find($request->input('id'));
        $status->namastatus = $request->input('NamaStatus');
        $status->save();
        return response()->json(1, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editData(Request $request, $id)
    {
        $data = statusmodel::find($id);
        $konten = view("status::edit",['data' =>$data ])->render();
        
        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }
        return view('layouts.backend.app',['konten' => $konten,'title' => ($request->route()->getAction()['as'])]);
    }

   function hapusData(Request $request, $id){
        $cek = statusmodel::find($id);

        if($cek->count() == 0 ){
            return response()->json('Data tidak tersedia', 403);
        }
        $cek->delete();
    }

}
