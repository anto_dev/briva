<?php
namespace App\ModulesBackend\modul\Model;

use Illuminate\Database\Eloquent\Model;

class modulmodel extends Model
{
	protected $table = 'tb_modul';
	public $timestamps = false;
}