<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
	<form action="{{route('tambahmodul')}}" class="form-vertical" method="post" id="frm-tambah-modul">
		{{csrf_field()}}
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group" id="groupNamaModul">
							<label for="NamaModul" class="control-label">Nama Modul :</label>
							
							<input type="text" class="form-control input-sm" id="NamaModul" name="NamaModul">
						</div>
						<div class="form-group" id="groupFolder">
							<label for="Folder" class="control-label">Folder :</label>
							<input type="text" class="form-control input-sm" id="Folder" name="Folder">
						</div>
						<div class="form-group" id="groupDefaultLink">
							<label for="DefaultLink" class="control-label">Default Link :</label>
							<input type="text" class="form-control input-sm" id="DefaultLink" name="DefaultLink">
						</div>

						<div class="form-group" id="groupIcon">
							<label for="Icon" class="control-label">Icon :</label>
							<div class="input-group">
								<input type="text" class="form-control input-sm" id="Icon" name="Icon">
								<span class="input-group-addon">
									<i class="fa fa-times" id="iconPreview"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">

						<div class="form-group" id="groupParent">
							<label for="Parent" class="control-label">Parent :</label>
							<select name="Parent" id="Parent" class="form-control input-sm">
								<option value="">-{{Lang::get('globals.placeholderSelect')}} Parent-</option>
								<option value="0">ROOT</option>
								@foreach($data as $item)
								<option value="{{$item->id}}">{{$item->namaModul}} - <span class="label label-success">{{$item->folder}}</span></option>
								@endforeach
							</select>
						</div>
						

						<div class="form-group" id="groupUrutan">
							<label for="Urutan" class="control-label">Urutan :</label>
							<input type="text" class="form-control input-sm" id="Urutan" name="Urutan">
						</div>
						<div class="form-group" id="groupMenu">
							<label for="Menu" class="control-label">Menu :</label>
							<select name="Menu" id="Menu" class="form-control input-sm">
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
							</select>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group" id="groupStatus">
									<label for="Status" class="control-label">Status :</label>
									<select name="Status" id="Status" class="form-control input-sm">
										@foreach(Lang::get('globals.arrayStatus')  as $key => $itemStatus)
										<option value="{{$key}}">{{$itemStatus}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group" id="groupAkses">
									<label for="Akses" class="control-label">Akses :</label>
									<select name="Akses" id="Akses" class="form-control input-sm">
										@foreach(Lang::get('globals.arrayAkses')  as $key => $itemAkses)
										<option value="{{$key}}">{{$itemAkses}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatmodul')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</section>
<script>
	$('#Icon').keyup(function(e){
		e.preventDefault();
		$('#iconPreview').attr('class','fa fa-'+$(this).val());
	});
	$('#frm-tambah-modul').submit(function(e){
		e.preventDefault();
		konfirmasi('Simpan data ini?','Simpan', simpan,'confirmSubmit' , $(this));
	})
	function nextAction(data){
		$('form')[0].reset();
	}
</script>