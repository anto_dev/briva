<section class="content-header">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Table</li>
        </ol>
    </nav>
</section>
<section class="content">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card" id="box-primary">
            <div class="card-header">
                <h6 class="card-title">
                    Data table
                </h6>
                <div class="row">                    
                    <div class="col-md-12">
                        <div class="d-flex align-items-center flex-wrap text-nowrap">
                            <button type="button" id="refresh" class="btn btn-info btn-icon-text mr-2 d-none d-md-block">
                                Refresh
                            </button>
                            @autorisasi('tambahmodul')
                            <a type="button" id="ajaxlink" href="{{route('tambahmodul')}}" class="btn btn-primary btn-icon-text mr-2 mb-2 mb-md-0">
                                Tambah
                            </a>
                            @endautorisasi
                            @autorisasi('hapuscheckmodul')
                            <button id="hapusCheckedkData" data-url="{{url(env('PREFIX_ADMIN').'/modul/hapus')}}" type="button" class="btn btn-danger btn-icon-text mb-2 mb-md-0">
                                Hapus Cek
                            </button>
                            @endautorisasi
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label>Filter:</label>
                                <select name="filter" id="filter" class="form-control">
                                    <option value="">{{Lang::get('globals.placeholderSelect')}} - Data Modul</option>
                                    <option value="0">Nama</option>
                                    <option value="1">Folder</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <label>Data Pencarian:</label>
                                <div class="input-group date timepicker" id="datetimepickerExample" data-target-input="nearest">
                                    <input type="text" class="form-control" data-target="#datetimepickerExample">
                                    <div class="input-group-append" data-target="#datetimepickerExample" data-toggle="datetimepicker">
                                        <div class="input-group-text"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="table-responsive table-condensed" id="box-table">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th width="2%">
                                            <input type="checkbox" id="checkAll" />
                                        </th>
                                        <th width="3%">
                                            No
                                        </th>
                                        <th width="20%">
                                            Nama
                                        </th>
                                        <th width="20%">
                                            Folder
                                        </th>
                                        <th width="20%">
                                            Parent
                                        </th>
                                        <th width="10%">
                                            Status
                                        </th>
                                        @autorisasi(editmodul|hapusmodul)
                                        <th width="7%">
                                            Action
                                        </th>
                                        @endautorisasi
                                    </tr>
                                </thead>
                                <tbody id="data">
                                    <td colspan="12"></td>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">                                
                                            <label class="label label-default">esc : beranda</label>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <table>
                                <tr>
                                    <td>{{Lang::get('globals.labelJumlahTampil')}}:  &nbsp;</td>
                                    <td>
                                        <select id="take" class="form-control input-sm" onchange="muatData()">
                                            <option value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </td>
                                    <td>
                                        &nbsp; Total : <span id="totalRecord"></span> Data
                                    </td>
                                </tr>
                            </table>



                        </div>
                        <div class="col-sm-12 col-md-7 pull-right">
                            <ul class="pagination pagination-sm" style="margin: 0px;float: right !important;">
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <style>
    /*input[type='checkbox']{
        appearance: none;
        }*/
    </style>
    <script>
        $(document).ready(function(){
            var session = readSession();
            if(session){
                filter = session.filter;
                take = session.take;
                page = session.page;
                data = session.data;
                $('#take').val(take);
                $('#data').val(data);
                $('#filter').val(filter);
                muatData(page);
            }
            else{
                muatData();
            }
            $('#refresh').click(function(){
                $('#take').val(5);
                $('#data').val('');
                $('#filter').val('');
                muatData();
            })
        });
        function muatData(page = 1){
            var filter = $('#filter').val();
            var data = $('#data').val();
            var take = $('#take').val();
            $.ajax({
                url : '{{url(env('PREFIX_ADMIN'))}}/modul',
                type : 'POST',
                data : {
                    _token : token,
                    filter : filter,
                    data : data,
                    page : page,
                    take : take
                },
                beforeSend : function(){
                    callOverlay('xaxa');
                },
                complete : function(){
                    removeOverlay();
                },
                success : function(response){
                    var activePage = response.activePage;
                    var totalPage = response.totalPage;
                    var totalRecord = response.totalRecord;
                    var urut=0;
                    var tbodyHtml = '';
                    $.each(response.data, function(index, el){
                        urut++;
                        tbodyHtml += '<tr>'
                        tbodyHtml += '<td><input type="checkbox" id="checkRow" data-id="'+el.id+'"/></td>'
                        tbodyHtml += '<td>'+urut+'</td>'
                        tbodyHtml += '<td>'+el.namaModul+'</td>'
                        tbodyHtml += '<td>'+el.folder+'</td>'
                        
                        if(el.parent == null)
                            tbodyHtml += '<td>ROOT</td>'
                        else
                            tbodyHtml += '<td>'+el.parent+'</td>'                        
                        tbodyHtml += '<td>'+lbl[el.status]+'</td>'
                        @autorisasi(editmodul|hapusmodul)
                        tbodyHtml += '<td>'
                        @autorisasi('editmodul')
                        tbodyHtml += '<a href="{{url(env('PREFIX_ADMIN').'/modul/edit')}}/'+el.id+'" id="ajaxlink" class="btn btn-warning btn-sm mr-1">Edit</a>'
                        @endautorisasi
                        @autorisasi('hapusmodul')
                        tbodyHtml += '<button class="btn btn-danger btn-sm" data-url={{url(env('PREFIX_ADMIN').'/modul/hapus')}} data-id="'+el.id+'" data-page="'+page+'" id="hapusData">Hapus</button>'
                        @endautorisasi
                        tbodyHtml += '</td>'
                        @endautorisasi
                        tbodyHtml += '</tr>'
                    })
                    loadPagination('pagination',totalPage, activePage, totalRecord)
                    $('.content table #data').html(tbodyHtml);
                    if(response.data.length == 0){
                        $('.content table #data').html('<tr><td colspan="8"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
                    }
                        // createSession('modul',page,take,filter,data);
                    },
                    error : function(data){
                        toastr['error'](data.statusText);
                    }
                })
        }
    </script>