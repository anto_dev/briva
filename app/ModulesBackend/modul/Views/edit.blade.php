<section class="content-header">
	<nav class="page-breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Tables</a></li>
			<li class="breadcrumb-item active" aria-current="page">Data Table</li>
		</ol>
	</nav>
</section>
<section class="content">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card" id="box-primary">
			<div class="card-header">
				<h6 class="card-title">
					{{Helper::showTitle()}}
				</h6>
			</div>
			<div class="card-body">
				<form action="{{route('editmodul')}}" class="form-vertical" method="post" id="frmEditmodul">
					{{csrf_field()}}
					<input type="hidden" name="id" value="{{$data->id}}">
					<div class="box box-primary" id="box-primary">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group" id="groupNamaModul">
										<label for="NamaModul" class="control-label">Nama Modul :</label>
										
										<input type="text" class="form-control input-sm" id="NamaModul" name="NamaModul" value="{{$data->namaModul}}">
									</div>
									<div class="form-group" id="groupFolder">
										<label for="Folder" class="control-label">Folder :</label>
										<input type="text" class="form-control input-sm" id="Folder" name="Folder" value="{{$data->folder}}">
									</div>
									<div class="form-group" id="groupDefaultLink">
										<label for="DefaultLink" class="control-label">Default Link :</label>
										<input type="text" class="form-control input-sm" id="DefaultLink" name="DefaultLink"  value="{{$data->defaultLink}}">
									</div>

									<div class="form-group" id="groupIcon">
										<label for="Icon" class="control-label">Icon :</label>
										<div class="input-group">
											<input type="text" value="{{$data->icon}}" class="form-control input-sm" id="Icon" name="Icon">
											<span class="input-group-addon">
												<i class="fa fa-{{$data->icon}}" id="iconPreview"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-6">

									<div class="form-group" id="groupParent">
										<label for="Parent" class="control-label">Parent :</label>
										<select name="Parent" id="Parent" class="form-control input-sm">
											<option value="">-{{Lang::get('globals.placeholderSelect')}} Parent-</option>
											<option value="0" @if($data->parent == 0) selected @endif>ROOT</option>
											@foreach($dataParent as $item)
											<option value="{{$item->id}}" @if($data->parent == $item->id) selected @endif>{{$item->namaModul}} - <span class="label label-success">{{$item->folder}}</span></option>
											@endforeach
										</select>
									</div>

									<div class="form-group" id="groupUrutan">
										<label for="Urutan" class="control-label">Urutan :</label>
										<input type="text" class="form-control input-sm" id="Urutan" name="Urutan" value="{{$data->urutan}}">
									</div>
									
									<div class="form-group" id="groupMenu">
										<label for="Menu" class="control-label">Menu :</label>
										<select name="Menu" id="Menu" class="form-control">
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
										</select>
									</div>

									<div class="row">
										<div class="col-lg-6">								
											<div class="form-group" id="groupStatus">
												<label for="Status" class="control-label">Status :</label>
												<select name="Status" id="Status" class="form-control input-sm">
													@foreach(Lang::get('globals.arrayStatus')  as $key => $itemStatus)
													<option value="{{$key}}" @if($data->status == $key) selected @endif>{{$itemStatus}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group" id="groupAkses">
												<label for="Akses" class="control-label">Akses :</label>
												<select name="Akses" id="Akses" class="form-control input-sm">
													@foreach(Lang::get('globals.arrayAkses')  as $key => $itemAkses)
													<option value="{{$key}}" @if($data->akses == $key) selected @endif>{{$itemAkses}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer" align="right">
							<a class="btn btn-default btn-sm" id="ajaxlink" href="{{route('lihatmodul')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
							<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
	$('#Icon').keyup(function(e){
		e.preventDefault();
		$('#iconPreview').attr('class','fa fa-'+$(this).val());
	});
	$('#frmEditmodul').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	function nextAction(data){
		window.location.replace('{{route('lihatmodul')}}?session={{Helper::getSession()}}');
	}
</script>