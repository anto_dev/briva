<?php

// namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\modulesBackend\modul\Model\modulmodel;
use Illuminate\Http\Request;

class modulcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $konten=view("modul::view")->render();

        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }
        return view('layouts.backend.app',['konten'=>$konten]);
    }
    function muatData(Request $request){

        $usedFilter[0] = 'tb_modul.namamodul';
        $usedFilter[1] = 'tb_modul.folder';

        $filter = 'tb_modul.id';
        if($request->input('filter') != ''){
            $filter = $usedFilter[$request->input('filter')];   
        }

        $data = $request->input('data');
        $take = $request->input('take');
        $page = $request->input('page');
        
        $skippedData = ($page - 1) * $take;
        

        $totalJumlah = modulmodel::where($filter , 'LIKE' ,'%'.$data.'%');
        // $totalJumlah = Aksesdata::allowedData($totalJumlah);
        $totalJumlah = $totalJumlah->leftJoin('tb_modul As B','B.id','=','tb_modul.parent')
        ->select('tb_modul.*','B.namamodul as parent')
        ->count();

        $totalPage = $totalJumlah / $take;

        $totalPage = ceil($totalPage);

        $data = modulmodel::where($filter , 'LIKE' ,'%'.$data.'%');
        // $data = Aksesdata::allowedData($data);
        $data = $data->leftJoin('tb_modul As B','B.id','=','tb_modul.parent')
        ->orderBy('tb_modul.parent')
        ->orderBy('tb_modul.urutan')
        ->select('tb_modul.*','B.namamodul as parent')
        ->take($take)
        ->skip($skippedData)
        ->get();

        return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    function tambahData(Request $request){
        $data = modulmodel::orderBy('tb_modul.parent')
        ->orderBy('tb_modul.urutan')
        ->get();
        $konten = view("modul::tambah",['data' =>$data])->render();
        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => $request->route()->getAction()['as']],200);
        }
        return view('layouts.backend.app',['konten' => $konten,'title' => $request->route()->getAction()['as']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpanData(Request $request)
    {
        $rules = [
            'NamaModul' => 'required',
            'Folder' => 'required|unique:tb_modul,folder',
            'DefaultLink' => 'required',
            'Icon' => 'required',
            'Parent' => 'required',
            'Urutan' => 'required|numeric',
            'Status' => 'required',
            'Akses' => 'required',
        ];
        $message = [
            'numeric' => ':attribute '.Lang::get('globals.numeric'),
            'required' => Lang::get('globals.required').' :attribute',
            'unique' => ' :attribute '.Lang::get('globals.unique')
        ];
        $validator = Validator::make($request->input(), $rules, $message);
        if($validator->fails()){
            $error = $validator->errors()->messages();
            return response()->json($error, 400);
        }
        $modul = new modulmodel;
        $modul->namamodul = $request->input('NamaModul');
        $modul->title = $request->input('NamaModul');
        $modul->folder = $request->input('Folder');
        $modul->defaultLink = $request->input('DefaultLink');
        $modul->icon = $request->input('Icon');
        $modul->parent = $request->input('Parent');
        $modul->urutan = $request->input('Urutan');
        $modul->menu = $request->input('Menu');
        $modul->status = $request->input('Status');
        $modul->akses = $request->input('Akses');
        $modul->save();
        return response()->json(1, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateData(Request $request)
    {
        $rules = [
            'NamaModul' => 'required',
            'DefaultLink' => 'required',
            'Icon' => 'required',
            'Parent' => 'required',
            'Urutan' => 'required|numeric',
            'Status' => 'required',
            'Akses' => 'required',
        ];
        $message = [
            'numeric' => ':attribute '.Lang::get('globals.numeric'),
            'required' => Lang::get('globals.required').' :attribute'
        ];
        $validator = Validator::make($request->input(), $rules, $message);
        if($validator->fails()){
            $error = $validator->errors()->messages();
            return response()->json($error, 400);
        }
        $modul = modulmodel::find($request->input('id'));
        $modul->namamodul = $request->input('NamaModul');
        $modul->title = $request->input('NamaModul');
        $modul->folder = $request->input('Folder');
        $modul->defaultLink = $request->input('DefaultLink');
        $modul->icon = $request->input('Icon');
        $modul->parent = $request->input('Parent');
        $modul->urutan = $request->input('Urutan');
        $modul->menu = $request->input('Menu');
        $modul->status = $request->input('Status');
        $modul->akses = $request->input('Akses');
        $modul->save();
        return response()->json(1, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editData(Request $request, $id)
    {
        $data = modulmodel::find($id);
        $dataParent = modulmodel::orderBy('tb_modul.parent')
        ->orderBy('tb_modul.urutan')
        ->get();
        $konten = view("modul::edit",['data' =>$data ,'dataParent' => $dataParent])->render();
        
        if($request->ajax()){
            return response()->json(['konten' => $konten,'title' => ($request->route()->getAction()['as'])],200);
        }
        return view('layouts.backend.app',['konten' => $konten,'title' => ($request->route()->getAction()['as'])]);
    }

   function hapusData(Request $request, $id){
        $cek = modulmodel::find($id);

        if($cek->count() == 0 ){
            return response()->json('Data tidak tersedia', 403);
        }
        $cek->delete();
    }

}
