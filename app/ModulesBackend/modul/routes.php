<?php 
Route::group(['prefix'=>env('PREFIX_ADMIN')], function(){

	Route::group(['prefix' => 'modul', 'middleware' => ['web', 'Admin','autorisasi']], function() {
		Route::get('/', 'ModulController@index')->name('lihatmodul');
		Route::post('/','modulController@muatData')->name('lihatmodul');
		Route::get('/tambah','modulController@tambahData')->name('tambahmodul');
		Route::post('/tambah', 'modulController@simpanData')->name('tambahmodul');
		Route::get('/edit/{id}','modulController@editData')->name('editmodul');
		Route::post('/update', 'modulController@updateData')->name('editmodul');
		Route::get('hapus/{id}','modulController@hapusData')->name('hapusmodul');

	});



	

})




?>