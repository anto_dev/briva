<?php
namespace App\ModulesBackend\tagihanbriva\Model;

use Illuminate\Database\Eloquent\Model;

class tagihanbrivamodel extends Model
{
	protected $table = 'tagihan_briva';
	public $timestamps = false;
}