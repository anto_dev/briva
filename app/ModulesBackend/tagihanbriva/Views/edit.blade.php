<section class="content-header">
	<h1 id="title-content">
		{{Helper::showTitle()}}
	</h1>
</section>
<section class="content">
	<form action="{{route('edittagihanbriva')}}" class="form-vertical" method="post" id="frmEdittagihanbriva">
		{{csrf_field()}}
		<input type="hidden" name="id" value="{{$data->PMBID}}">
		<div class="box box-primary" id="box-primary">
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group" id="groupPMBID">
							<label for="PMBID" class="control-label">PMBID :</label>
							<input type="text" class="form-control input-sm" id="PMBID" name="PMBID" readonly value="{{$data->PMBID}}" autocomplete="off">
						</div>
						<div class="form-group" id="groupNIK">
							<label for="NIK" class="control-label">NIK :</label>
							<input type="text" class="form-control input-sm" id="NIK" name="NIK" readonly value="{{$data->NIK}}" autocomplete="off">
						</div>
						<div class="form-group" id="groupNama">
							<label for="Nama" class="control-label">Nama :</label>
							<input type="text" class="form-control input-sm" id="Nama" name="Nama" readonly value="{{$data->Nama}}" autocomplete="off">
						</div>
						<div class="form-group" id="groupkodebriva">
							<label for="kodebriva" class="control-label">Kode Briva :</label>
							<input type="text" class="form-control input-sm" id="kodebriva" name="kodebriva" readonly value="{{$data->kodebriva}}" autocomplete="off">
						</div>
						<div class="form-group" id="groupKeterangan">
							<label for="Keterangan" class="control-label">Keterangan :</label>
							<input type="text" class="form-control input-sm" value="{{$data->UPemb}}" id="Keterangan" name="Keterangan" autocomplete="off">
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="form-group" id="groupNominal">
									<label for="Nominal" class="control-label">Nominal :</label>
									<input type="number" class="form-control input-sm" value="{{$data->Nominal}}" id="Nominal" name="Nominal" autocomplete="off">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group" id="groupStatus">
									<label for="Status" class="control-label">Status :</label>
									<input type="text" class="form-control input-sm" value="{{$data->response}}" id="Status" name="Status" autocomplete="off">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer" align="right">
				<a class="btn btn-default btn-sm btnKembali" id="ajaxlink" href="{{route('lihattagihanbriva')}}?session={{Helper::getSession()}}">{!! Lang::get('globals.labelKembali') !!}</a>
				<button class="btn btn-primary btn-sm" type="submit">{!! Lang::get('globals.labelSimpan') !!}</button>
			</div>
		</div>
	</form>
</section>
<script>
	$(document).ready(function(){
		
	})
	$('#frmEdittagihanbriva').submit(function(e){
		e.preventDefault();
		konfirmasi('Edit data ini?','Edit', edit,'confirmSubmit' , $(this), 'warning');
	})
	function nextAction(data){
		// $('.btnKembali').trigger('click');
	}
</script>