<section class="content-header">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Table</li>
        </ol>
    </nav>
</section>
<section class="content">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card" id="box-primary">
            <div class="card-body">
                <h6 class="card-title">
                    {{Helper::showTitle()}}
                </h6>
                <div class="row" id="xaxa">
                    
                    <div class="col-md-12">
                        <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                            <div class="d-flex align-items-center flex-wrap text-nowrap">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col">
                                <label>Filter:</label>
                                <select name="filter" id="filter" class="form-control">
                                    <option value="">{{Lang::get('globals.placeholderSelect')}} - Filter Data</option>
                                    <option value="0">Kode Briva</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Data Pencarian:</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" name="data" id="data" placeholder="{{Lang::get('globals.placeHolderSearch')}}">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" onclick="muatData()" type="button">Cari</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group" style="width: 100%;">
                                    <label>Periode Tanggal : </label>
                                    <div class="input-group">
                                        <input class="form-control input-sm" type="text" id="periode" name="periode" placeholder="Masukkan periode pencarian data"></input>
                                        <span class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="muatData()">Cari</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            <div class="box-body">
                <div class="box table-responsive table-condensed" id="box-table" class="col-md-12">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th width="2%">
                                    <input type="checkbox" id="checkAll" />
                                </th>
                                <th width="3%">
                                    No
                                </th>
                                <th>
                                    IDKEU
                                </th>
                                <th>
                                    MhswID
                                </th>
                                <th>
                                    Keterangan
                                </th>
                                <th>
                                    Total
                                </th>
                                <th>
                                    Tanggal
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody id="data">
                            <tr><td colspan="12">Belum ada data ditampilkan</td></tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="12">                                
                                    <label class="label label-default">esc : beranda</label>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div><br>
            <div class="box-footer">
                
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <table>
                            <tr>
                                <td>{{Lang::get('globals.labelJumlahTampil')}}:  &nbsp;</td>
                                <td>
                                    <select id="take" class="form-control input-sm" onchange="muatData()">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </td>
                                <td>
                                    &nbsp; Total : <span id="totalRecord"></span> Data
                                </td>
                            </tr>
                        </table>
                        
                        
                        
                    </div>
                    <div class="col-sm-12 col-md-7 pull-right">
                        <ul class="pagination pagination-sm" style="margin: 0px;float: right !important;">
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </div>
</section>
<style>
    /*input[type='checkbox']{
        appearance: none;
    }*/
</style>
<script>
	$(document).ready(function(){
		var d = new Date();
            var m = d.getMonth();
            var bulan = d.getMonth()+1;
            var tgl=d.getDate();
            $('body #periode').daterangepicker({
                startDate: +tgl+'-'+bulan+'-2015',
                endDate: +tgl+'-'+bulan+'-'+d.getFullYear(),
                locale: {
                    format: 'DD-MM-YYYY'
                }
            })
		var session = readSession();
		if(session){
			filter = session.filter;
			take = session.take;
			page = session.page;
			data = session.data;
			$('#take').val(take);
			$('#data').val(data);
			$('#filter').val(filter);
			muatData(page);
		}
		else{
			muatData();
		}
		$('#refresh').click(function(){
			$('#take').val(5);
			$('#data').val('');
			$('#filter').val('');
			muatData();
		})
	});
	function muatData(page = 1){
		var filter = $('#filter').val();
		var data = $('#data').val();
		var take = $('#take').val();
		var periode = $('#periode').val();
		$.ajax({
			url : '{{url(env('PREFIX_ADMIN').'/tagihanbriva')}}',
			type : 'POST',
			data : {
				_token : token,
				filter : filter,
				data : data,
				page : page,
				take : take,
				periode:periode
			},
			beforeSend : function(){
				callOverlay('box-table');
			},
			complete : function(){
				removeOverlay();
			},
			success : function(response){
				var activePage = response.activePage;
				var totalPage = response.totalPage;
				var totalRecord = response.totalRecord;
				var status ="";
				var urut=0;
				var tbodyHtml = '';
				$.each(response.data, function(index, el){
					urut++;
					if(el.response == 5){ 
						status = '<label class="label label-success">Dibayar</label>';
					}else if(el.response == 3){ 
						status = '<label class="label label-warning">Digenerate</label>';
					}else{
						status = '<label class="label label-danger text-dark">Dalam Antrian</label>';
					}
					tbodyHtml += '<tr>'
					tbodyHtml += '<td><input type="checkbox" id="checkRow" data-id="'+el.id+'"/></td>'
					tbodyHtml += '<td>'+urut+'</td>'
					tbodyHtml += '<td>'+el.IDKEU+'</td>'
					tbodyHtml += '<td>'+el.MhswID+'</td>'
					tbodyHtml += '<td>'+el.Keterangan+'</td>'
					tbodyHtml += '<td align="right">'+formatter.format(el.Total)+'</td>'
					tbodyHtml += '<td>'+el.TanggalBuat+'</td>'
					tbodyHtml += '<td>'+ status +'</td>'
					@autorisasi(edittagihanbriva|hapustagihanbriva)
					tbodyHtml += '<td>'
					@autorisasi('edittagihanbriva')
					tbodyHtml += '<a href="{{url(env('PREFIX_ADMIN').'/tagihanbriva/edit')}}/'+el.id+'" id="ajaxlink" class="btn btn-outline-primary btn-icon-text mr-2 d-none d-md-block btn-sm">Detail</a>'
					@endautorisasi
					tbodyHtml += '</td>'
					@endautorisasi
					tbodyHtml += '</tr>' 
				})
				loadPagination('pagination',totalPage, activePage, totalRecord)
				$('.content table #data').html(tbodyHtml);
				if(response.data.length == 0){
					$('.content table #data').html('<tr><td colspan="12"><center>{{Lang::get('globals.labelDataNoData')}}</center></td></tr>');
				}
                // createSession('tagihanbriva',page,take,filter,data);
			},
			error : function(data){
				toastr['error'](data.statusText);
			}
		})
	}
</script>