<?php 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ModulesBackend\tagihanbriva\Model\tagihanbrivaModel;
use App\Custom\CustomClass;
class tagihanbrivacontroller  extends Controller
{
	function lihat(Request $request){
		$konten = view('tagihanbriva::view')->render();
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten]);
	}
	function muatData(Request $request){
		$filter = 'biayauk.ID';
		$usedFilter[0] = 'biayauk.MhswID';
		$tanggalawal=substr($request->input('periode'), 0,10);
        $tanggalakhir=substr($request->input('periode'), 13,10);
        $tanggalawal = \DateTime::createFromFormat('d-m-Y',$tanggalawal);
        $tanggalawal = $tanggalawal->sub(new \DateInterval('P0D'));
        $tanggalakhir = \DateTime::createFromFormat('d-m-Y',$tanggalakhir);
        $tanggalakhir = $tanggalakhir->sub(new \DateInterval('P0D'));
		if($request->input('filter') != ''){
			$filter = $usedFilter[$request->input('filter')];	
		}
		$data = $request->input('data');
		$take = $request->input('take');
		$page = $request->input('page');
		
		$skippedData = ($page - 1) * $take;
		$totalJumlah = DB::connection('sisfo')->table('biayauk')->where('response',1)->where($filter , 'LIKE' ,'%'.$data.'%')->whereDate('TanggalBuat','>=',$tanggalawal)->whereDate('TanggalBuat','<=',$tanggalakhir)->orderBy('TanggalBuat','desc')
		->count();
		$totalPage = $totalJumlah / $take;

		$totalPage = ceil($totalPage);

		$data = DB::connection('sisfo')->table('biayauk')->where('response',1)->where($filter , 'LIKE' ,'%'.$data.'%')->whereDate('TanggalBuat','>=',$tanggalawal)->whereDate('TanggalBuat','<=',$tanggalakhir)->orderBy('TanggalBuat','desc')
		->take($take)
		->select('biayauk.*')
		->skip($skippedData)
		->get();

		return response()->json(['data' => $data , 'totalPage' => $totalPage , 'activePage' => $page,'totalRecord' => $totalJumlah]);
	}
	function tambahData(Request $request){
		$data = tagihanbrivaModel::get();
		$konten = view("tagihanbriva::tambah",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function simpanData(Request $request){
		$id=$request->input('id');
		$cek=tagihanbrivaModel::where('id',$id)->first();
		if (empty($cek)) {
			return response('Data tidak ditemukan',409);
		}
		$kodebriva=$cek->kode_briva;
		$response=Helper::q_briva_status($kodebriva);
		$status=$response['status'];
		if ($status) {
			$message=$response['responseDescription'];
			$code=200;
		}else{
			$message=$response['errDesc'];
			$code=409;
		}
		return response($message, $code);
	}
	function updateData(Request $request){
		$rules = [
			'id' => 'required|exists:pmb,PMBID',
			'Nominal' => 'required|numeric',
			'Keterangan' => 'required'
		];
		$message = [
			'required' => Lang::get('globals.required').' :attribute'
		];
		$validator = Validator::make($request->input(), $rules, $message);
		if($validator->fails()){
			$error = $validator->errors()->messages();
			return response()->json($error, 400);
		}
		$id=$request->input('id');
		$cek=tagihanbrivaModel::where('PMBID',$id)->first();
		$kodebriva=$cek->kodebriva;
		$nama=$cek->Nama;
		$nominal=$request->input('Nominal');
		$keterangan=$request->input('Keterangan');
		$briva=Helper::q_briva_update($kodebriva,$nama,$nominal,$keterangan);
		tagihanbrivaModel::where('PMBPeriodID',$request->input('id'))->update([
			'UPemb' => $keterangan,
			'JumlahPUBank' => $nominal,
			'TotalSetoranMhsw' => $nominal,
			'response' => 3
		]);
		DB::table('log_briva')->insert([
			'reference'=>'update',
			'payload'=>json_encode($briva),
			'created_ip'=>$request->ip()
		]);
		return response()->json(1, 200);
	}
	function editData(Request $request, $id){

		$data = tagihanbrivaModel::where('id',$id)->first();
		$konten = view("tagihanbriva::edit",['data' =>$data])->render();
		
		if($request->ajax()){
			return response()->json(['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])],200);
		}
		return view('layouts.backend.app',['konten' => $konten,'title' => Autorisasi::getJudul($request->route()->getAction()['as'])]);
	}
	function hapusData(Request $request, $id){
		$cek = tagihanbrivaModel::where('id',$id)->first();
		if($cek->count() == 0 ){
			return response()->json('Data tidak tersedia', 403);
		}
		$kodebriva=$cek->kode_briva;
		$briva=Helper::q_briva_delete($kodebriva);
		DB::table('log_briva')->insert([
			'reference'=>'delete',
			'payload'=>json_encode($briva),
			'created_ip'=>$request->ip()
		]);
		$status=$briva['status'];
		if(!$status){
			$message=$briva['errDesc'];
			return response($message,409);
		}else{
			if($cek->keterangan=="PMB REG"){
				DB::table('pmbformjual')->where('kodebriva',$kodebriva)->update([
					'response'=>1,
					'kodebriva'=>null
				]);
			}else{
				DB::table('pmbformjual')->where('kodebriva',$kodebriva)->update([
					'response'=>1,
					'kodebriva'=>null,
					'UPemb'=>null,
					'JumlahPUBank'=>null,
					'TotalSetoranMhsw'=>null
				]);
			}
			tagihanbrivaModel::where('id',$id)->delete();
			return true;
		}
		// $cek->delete();
	}
}
