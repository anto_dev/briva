<?php 
Route::group(['prefix' => env('PREFIX_ADMIN'), 'middleware' => ['web','Admin']], function(){
Route::group(['prefix' => 'tagihanbriva', 'middleware' => ['web','Admin','autorisasi']], function(){
	Route::get('/','tagihanbrivacontroller@lihat')->name('lihattagihanbriva');
	Route::post('/','tagihanbrivacontroller@muatData')->name('lihattagihanbriva');
	Route::get('/tambah','tagihanbrivacontroller@tambahData')->name('tambahtagihanbriva');
	Route::post('/tambah','tagihanbrivacontroller@simpanData')->name('tambahtagihanbriva');
	Route::get('/hapus/{id}','tagihanbrivacontroller@hapusData')->name('hapustagihanbriva');
	Route::get('/edit/{id}','tagihanbrivacontroller@editData')->name('edittagihanbriva');
	Route::post('/edit','tagihanbrivacontroller@updateData')->name('edittagihanbriva');
});
});