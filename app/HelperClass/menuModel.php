<?php

namespace App\HelperClass;

use Illuminate\Database\Eloquent\Model;

class menuModel extends Model
{
	protected $table = 'tb_menu';
	public $timestamp = 'false';
}
