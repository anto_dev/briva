<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Blade;
use App\Tb_User as Autorisasi;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('autorisasi', function ($izin) {
            $izinawal = $izin;
            $izin = explode('|', $izin);
            if(count($izin) == 1){
                return "<?php if(Autorisasi::izinMasuk($izinawal)) : ?>";
            }
            $syntaxIzin = "<?php if(";
            foreach ($izin as $key => $value) {
                if($key == 0)
                    $syntaxIzin .=" Autorisasi::izinMasuk('".$value."') ";   
                else
                    $syntaxIzin .=" || Autorisasi::izinMasuk('".$value."') ";
            }
            $syntaxIzin .="): ?>";
            return $syntaxIzin;
        });
        Blade::directive('endautorisasi', function() {
                return '<?php endif; ?>';
        });

         Validator::extend('select', function ($attribute, $value, $parameters, $validator) {
            return $value == 'select';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
