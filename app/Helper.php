<?php
namespace App;
use DB;
use Auth;
use Request;
class Helper
{
    static function getSession(){
        if(isset($_GET['session'])){
            return $_GET['session'];
        }
    }
    static function showTitle(){
        $title='[]';
        $modul=null;
        $data=Request::route()->getAction()['as'];
        $prefix=substr($data, 0,1);
        if ($prefix=='l') {
            $modul=substr($data, 5);
            $action='Lihat';
        }elseif ($prefix=='e') {
            $modul=substr($data, 4);
            $action='Edit';
        }elseif ($prefix=='t') {
            $modul=substr($data, 6);
            $action='Tambah';
        }elseif ($prefix=='d') {
            $title='Detail';
        }
        $modul=DB::table('tb_modul')->where('defaultLink',$modul)->select('title')->first();
        if (!empty($modul)) {
            $title=$action.' '.$modul->title;
        }
        return $title;
    }
    static function getPeriodeAdmin(){
        if (session('sessionperiodeadmin')!==null) {
            return session('sessionperiodeadmin');
        }else{
            session(['sessionperiodeadmin' => 'ok']);
            return 'ok';
        }
    }
    static function converStringBulan($param){
        $bulan=[
            '01'=>'Januari',
            '02'=>'Februari',
            '03'=>'Maret',
            '04'=>'April',
            '05'=>'Mei',
            '06'=>'Juni',
            '07'=>'Juli',
            '08'=>'Agustus',
            '09'=>'September',
            '10'=>'Oktober',
            '11'=>'November',
            '12'=>'Desember'
        ];
        return $bulan[$param];
    }
    static function converIndexBulan($param){
        $bulan=[
            'Januari'=>'01',
            'Februari'=>'02',
            'Maret'=>'03',
            'April'=>'04',
            'Mei'=>'05',
            'Juni'=>'06',
            'Juli'=>'07',
            'Agustus'=>'08',
            'September'=>'09',
            'Oktober'=>'10',
            'November'=>'11',
            'Desember'=>'12'
        ];
        return $bulan[$param];
    }
    static function converStringGender($param){
        $gender=[
            'M'=>'Laki-laki',
            'F'=>'Perempuan'
        ];
        return $gender[$param];
    }
    static function convertYmd($date){
        $ymd=date('Y-m-d',strtotime($date));
        return $ymd;
    }
    static function convertHis($time){
        $his=date('H:i:s',strtotime($time));
        return $his;
    }
    static function getUser($id){
        $data=DB::table('users')->where('id',$id)->first();
        return $data;
    }
    static function getTahunAkademik(){
        $data=DB::table('tb_tahunakademik')->where('status','Y')->first();
        $tahunakademik=$data->idtahunakademik;
        return $tahunakademik;
    }
    static function getPeriodeGanjil(){
        $data=DB::table('tb_tahunakademik')->where('status','Y')->first();
        $periodeganjil=substr($data->idtahunakademik, 0,4);
        return $periodeganjil;
    }
    static function getPeriodeGenap(){
        $data=DB::table('tb_tahunakademik')->where('status','Y')->first();
        $periodegenap=substr($data->idtahunakademik, -4);
        return $periodegenap;
    }
    static function getRateSpp($stambuk){
        $data=DB::table('tb_ratespp')->where('idtahunakademik',$stambuk)->where('status','Y')->first();
        $ratespp=$data->rate;
        return $ratespp;
    }
    static function getArrayTahunAkademik(){
        $datatahunakademik=DB::table('tb_tahunakademik')->orderBy('idtahunakademik','desc')->get();
        return $datatahunakademik;
    }
    static function convertPeriode($param){
        $periode = \DateTime::createFromFormat('d-m-Y',$param);
        $periode = $tanggalawal->sub(new \DateInterval('P0D'));
        return $periode;
    }
    static function validateDate($date, $format = 'Y-m-d') {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    static function getCounter($tahun,$jenis,$prefix=4){
        $cek=DB::table('counter')->where('tahun',$tahun)->where('jenis',$jenis)->first();
        if (empty($cek)) {
            $counter=1;
            DB::table('counter')->insert([
                'tahun'=>$tahun,
                'jenis'=>$jenis,
                'counter'=>$counter
            ]);
            $counter=sprintf('%0'.$prefix.'d',$counter);
        }else{
            $last=$cek->counter;
            $last=$last+1;
            $counter=sprintf('%0'.$prefix.'d',$last);
            DB::table('counter')->where('tahun',$tahun)->where('jenis',$jenis)->update([
                'counter'=>$last,
            ]);
        }
        return $counter;
    }
    static function getCounterPmb($type){
        $cek=DB::table('counter')->where('jenis',$type)->first();
        $tahun=date('y');
        if(empty($cek)){
            $counter=1;
            DB::table('counter')->insert([
                'tahun'=>$tahun,
                'jenis'=>'RG',
                'counter'=>$counter
            ]);
        }else{
            $last=$cek->counter;
            $counter=$last+1;
            DB::table('counter')->where('tahun',$tahun)->where('jenis',$type)->update([
                'counter'=>$counter
            ]);
        }
        return $noreg='K42-'.$tahun.'-'.sprintf('%04d',$counter);
    }
    static function getShortcut($id){
        $data=DB::table('tb_modul')->where('tb_modul.status',1)->where('tb_modul.akses',$id)->where('tb_modul.menu',1)->where('tb_modul.featured',1)
        ->join('tb_shortcut','tb_shortcut.idmodul','tb_modul.id')
        ->get();
        return $data;
    }
    static function convertToChar($data){
        return chr($data);
    }
    static function getPengaturanweb($param){
        $data=DB::table('tb_pengaturanweb')->where('type','1')->select($param)->first();
        $res=$data->$param;
        return $res;
    }
    static function terbilang($x){
        $_this = new self;
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = $_this->terbilang($x - 10). " belas";
        } else if ($x <100) {
            $temp = $_this->terbilang($x/10)." puluh". $_this->terbilang($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . $_this->terbilang($x - 100);
        } else if ($x <1000) {
            $temp = $_this->terbilang($x/100) . " ratus" . $_this->terbilang($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . $_this->terbilang($x - 1000);
        } else if ($x <1000000) {
            $temp = $_this->terbilang($x/1000) . " ribu" . $_this->terbilang($x % 1000);
        } else if ($x <1000000000) {
            $temp = $_this->terbilang($x/1000000) . " juta" . $_this->terbilang($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = $_this->terbilang($x/1000000000) . " milyar" . $_this->terbilang(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = $_this->terbilang($x/1000000000000) . " trilyun" . $_this->terbilang(fmod($x,1000000000000));
        }     
        return ucwords($temp);
    }
    static function auhtorization()
    {
        $url    = 'https://partner.api.bri.co.id/oauth/client_credential/accesstoken?grant_type=client_credentials';
        $ch = curl_init($url);
        $data=[
            "client_id"=>"zZLjDbAYeT5FqtKv9tUA0dK7shcW6upZ",
            "client_secret"=>"a2csxAP7aC6ynAMv"
        ];
        $requestBody=($data);
        // dd($requestBody);
        $header = array(
            'Content-Type:application/x-www-form-urlencoded'
        );
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=zZLjDbAYeT5FqtKv9tUA0dK7shcW6upZ&client_secret=a2csxAP7aC6ynAMv");
        
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        return $result;
    }
    static function setExpiredDate(){
        $date=date('Y-m-d H:i:s');
        $expiredDate=date('Y-m-d H:i:s', strtotime($date . ' +30 days'));
        return $expiredDate;
    }
    static function q_mailer($receiver,$nama,$prodi,$kodebriva,$nominal,$pesan,$subject,$mailview){
        \Session::put('receiver',$receiver);
        \Session::put('nama',$nama);
        \Session::put('prodi',$prodi);
        \Session::put('kodebriva',$kodebriva);
        \Session::put('nominal',$nominal);
        \Session::put('subject',$subject);
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send($mailview, [], function($message)
        {
            $message
            ->from('pmb@uhn.ac.id')
            ->to(\Session::get('receiver'))
            ->subject(\Session::get('subject'));
        });
        return true;
    }
    static function q_briva_create($kodebriva,$nama,$nominal,$keterangan){
        $kodebayar=$kodebriva;
        $expired_at=Self::setExpiredDate();
        $cek_token=\Session::get('token_date');
        if($cek_token==date('Y-m-d')){
            $token=\Session::get('access_token');
        }else{
            $res=Self::auhtorization();
            $result=json_decode($res);
            $token=$result->access_token;
        }
        $institutionCode=env('BRIVA_ID');
        $brivaNo=env('BRIVA_NO');
        $url=env('BRIVA_URL');
        try {
            $data=[
                "institutionCode"=>$institutionCode,
                "brivaNo"=>$brivaNo,
                "custCode"=>$kodebayar,
                "nama"=>$nama,
                'amount'=>"$nominal",
                "keterangan"=>$keterangan,
                "expiredDate"=>$expired_at
            ];
            $requestBody=json_encode($data);
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva";
            $payload='path='.$path.'&verb=POST&token=Bearer '.$token.'&timestamp='.$timestamp.'&body={"institutionCode":"'.$institutionCode.'","brivaNo":"'.$brivaNo.'","custCode":"'.$kodebayar.'","nama":"'.$nama.'","amount":"'.$nominal.'","keterangan":"'.$keterangan.'","expiredDate":"'.$expired_at.'"}';
            $key='a2csxAP7aC6ynAMv';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Content-Type:"."application/json",
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            $verb = "POST";
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $verb); 
            curl_setopt($chPost, CURLOPT_POSTFIELDS, $requestBody);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            $status=$result['status'];
            if($status){
                
            }
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            $res=$result;
        } catch (\Throwable $e) {
            $res=$e;
        }
        return $res;
    }
    static function q_briva_update($kodebriva,$nama,$nominal,$keterangan){
        $kodebayar=$kodebriva;
        $expired_at=Self::setExpiredDate();
        $res=Self::auhtorization();
        $result=json_decode($res);
        $token=$result->access_token;
        $institutionCode=env('BRIVA_ID');
        $brivaNo=env('BRIVA_NO');
        $url=env('BRIVA_URL');
        try {
            $data=[
                "institutionCode"=>$institutionCode,
                "brivaNo"=>$brivaNo,
                "custCode"=>$kodebayar,
                "nama"=>$nama,
                'amount'=>"$nominal",
                "keterangan"=>$keterangan,
                "expiredDate"=>$expired_at
            ];
            $requestBody=json_encode($data);
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva";
            $payload='path='.$path.'&verb=PUT&token=Bearer '.$token.'&timestamp='.$timestamp.'&body={"institutionCode":"'.$institutionCode.'","brivaNo":"'.$brivaNo.'","custCode":"'.$kodebayar.'","nama":"'.$nama.'","amount":"'.$nominal.'","keterangan":"'.$keterangan.'","expiredDate":"'.$expired_at.'"}';
            $key='a2csxAP7aC6ynAMv';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Content-Type:"."application/json",
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            $verb = "PUT";
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $verb); 
            curl_setopt($chPost, CURLOPT_POSTFIELDS, $requestBody);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            $status=$result['status'];
            if($status){
                
            }
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            $res=$result;
        } catch (\Throwable $e) {
            $res=$e;
        }
        return $res;
    }
    static function q_briva_report($start_date,$end_date){
        $expired_at=Self::setExpiredDate();
        $cek_token=\Session::get('token_date');
        if($cek_token==date('Y-m-d')){
            $token=\Session::get('access_token');
        }else{
            $res=Self::auhtorization();
            $result=json_decode($res);
            $token=$result->access_token;
        }
        $institutionCode=env('BRIVA_ID');
        $brivaNo=env('BRIVA_NO');
        $url=env('BRIVA_URL');
        try {
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva/report/".$institutionCode.'/'.$brivaNo.'/'.$start_date.'/'.$end_date;
            $key='a2csxAP7aC6ynAMv';
            $payload='path='.$path.'&verb=GET&token=Bearer '.$token.'&timestamp='.$timestamp.'&body=';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            $verb = "GET";
            $url.='/report/'.$institutionCode.'/'.$brivaNo.'/'.$start_date.'/'.$end_date;
            // dd($url);
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $verb);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            $status=$result['status'];
            if($status){
                
            }
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            $res=$result;
        } catch (\Throwable $e) {
            $res=$e;
        }
        return $res;
    }
    static function q_briva_status($kodebayar){
        $expired_at=Self::setExpiredDate();
        $cek_token=\Session::get('token_date');
        if($cek_token==date('Y-m-d')){
            $token=\Session::get('access_token');
        }else{
            $res=Self::auhtorization();
            $result=json_decode($res);
            $token=$result->access_token;
        }
        $institutionCode=env('BRIVA_ID');
        $brivaNo=env('BRIVA_NO');
        $url=env('BRIVA_URL');
        try {
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva/status/".$institutionCode.'/'.$brivaNo.'/'.$kodebayar;
            $key='a2csxAP7aC6ynAMv';
            $payload='path='.$path.'&verb=GET&token=Bearer '.$token.'&timestamp='.$timestamp.'&body=';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            $verb = "GET";
            $url.='/status/'.$institutionCode.'/'.$brivaNo.'/'.$kodebayar;
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $verb);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            $status=$result['status'];
            if($status){
                
            }
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            $res=$result;
        } catch (\Throwable $e) {
            $res=$e;
        }
        return $res;
    }
    static function q_briva_cek($kodebayar){
        $expired_at=Self::setExpiredDate();
        $cek_token=\Session::get('token_date');
        if($cek_token==date('Y-m-d')){
            $token=\Session::get('access_token');
        }else{
            $res=Self::auhtorization();
            $result=json_decode($res);
            $token=$result->access_token;
        }
        $institutionCode=env('BRIVA_ID');
        $brivaNo=env('BRIVA_NO');
        $url=env('BRIVA_URL');
        try {
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva/".$institutionCode.'/'.$brivaNo.'/'.$kodebayar;
            $key='a2csxAP7aC6ynAMv';
            $payload='path='.$path.'&verb=GET&token=Bearer '.$token.'&timestamp='.$timestamp.'&body=';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            $verb = "GET";
            $url.='/'.$institutionCode.'/'.$brivaNo.'/'.$kodebayar;
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $verb);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            $status=$result['status'];
            if($status){
                
            }
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            $res=$result;
        } catch (\Throwable $e) {
            $res=$e;
        }
        return $res;
    }
    static function q_briva_delete($kodebriva){
        $res=Self::auhtorization();
        $result=json_decode($res);
        $token=$result->access_token;
        $institutionCode=env('BRIVA_ID');
        $brivaNo=env('BRIVA_NO');
        $url=env('BRIVA_URL');
        $kodebayar=$kodebriva;
        try {
            $data=[
                "institutionCode"=>$institutionCode,
                "brivaNo"=>$brivaNo,
                "custCode"=>$kodebayar
            ];
            $requestBody=($data);
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva";
            $payload='path='.$path.'&verb=DELETE&token=Bearer '.$token.'&timestamp='.$timestamp.'&body=institutionCode='.$institutionCode.'&brivaNo='.$brivaNo.'&custCode='.$kodebayar.'';
            $key='a2csxAP7aC6ynAMv';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Content-Type:"."application/x-www-form-urlencoded",
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            // $verb = "DELETE";
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            curl_setopt($chPost, CURLOPT_POSTFIELDS, "institutionCode=".$institutionCode."&brivaNo=".$brivaNo."&custCode=".$kodebayar."");
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            $res=$result;
        } catch (\Throwable $e) {
            $res=$e;
        }
        return $res;
    }
    static function q_bills_category($param){
        $cek_category=DB::table('bills_category_detail')->where('IDBipot',$param)->select('Urutan','category_id')->first();
        if(empty($cek_category)){
            $res['urutan']=null;
            $res['category_id']=null;
        }else{
            $res['urutan']=$cek_category->Urutan;
            $res['category_id']=$cek_category->category_id;
        }
        return $res;
    }
    static function q_label_bills_category($param){
        $data=DB::table('bills_category')->where('category_id',$param)->first();
        if(empty($data)){
            $keterangan='-';
        }else{
            $keterangan=$data->keterangan;
        }
        return $keterangan;
    }
    static function setdatetime($param){
        if($param == '0000-00-00 00:00:00'){
            $param = '0001-01-01 00:00:00';
        }
        return $param;
    }
    static function q_data_prodi($param){
        $data=DB::connection('sisfo_pmb')->table('prodi')->where('ProdiID',$param)->select('Nama')->first();
        if (empty($data)) {
            $nm_prodi='-';
        }else{
            $nm_prodi=$data->Nama;
        }
        return $nm_prodi;
    }
}
