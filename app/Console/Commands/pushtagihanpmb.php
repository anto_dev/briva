<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Helper;
use Illuminate\Support\Facades\Log;

class pushtagihanpmb extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'push:tagihanpmb';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Proses push payment pmb briva';
    
    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        //
        $institutionCode=env('BRIVA_ID');
        $coroprateID=env('BRIVA_NO');
        $prefix_='22'.date('y');
        $pmbperiode=DB::connection('sisfo_pmb')->table('pmbperiod')->where('NA','N')->first();
        dd($pmbperiode);
        if(!empty($pmbperiode)){
            $periode=$pmbperiode->PMBPeriodID;
            $data_pmb=DB::connection('sisfo_pmb')->table('pmb')->where('response',1)->where('KwitansiPU','process')->where('JumlahPUBank','>',0)->where('PMBPeriodID',$periode)->get();
            // dd(($data_pmb));
            foreach ($data_pmb as $key => $value) {
                try {
                    DB::beginTransaction();
                    DB::connection('sisfo_pmb')->beginTransaction();
                    $PMBID=$value->PMBID;
                    $cek_invoice=DB::table('invoices_pmb')->where('PMBID',$PMBID)->first();
                    if (empty($cek_invoice)) {
                        $fakultas_arr=explode('-',$PMBID);
                        $fakultas_id=$fakultas_arr[2];
                        $data_formulir=DB::connection('sisfo_pmb')->table('pmbformulir')->where('FakultasID',$fakultas_id)->first();
                        if(!empty($data_formulir)){
                            $PMBID=$data_formulir->PMBID;
                            $kodebayar=$prefix_.$PMBID.$fakultas_arr[3];
                            dd($kodebayar);
                            $ins=DB::table('invoices_pmb')->insert([
                                'PMBID'=>$PMBID,
                                'company_id'=>$institutionCode,
                                'corporate_code'=>$coroprateID,
                                'customer_code'=>$kodebayar,
                                'years'=>$value->PMBPeriodID,
                                'orders'=>0,
                                'category_id'=>0,
                                'invoice_number'=>$PMBID,
                                'amount'=>$value->Jumlah,
                                'status'=>'process',
                                'description'=>$keterangan,
                                'major'=>$value->ProdiID,
                                'response'=>$value->response
                            ]);
                            DB::table('log_pmb')->insert([
                                'reference'=>'created',
                                'payload'=>json_encode($ins),
                                'created_ip'=>'172.27.1.67'
                            ]);
                            $briva=Helper::q_briva_create($kodebayar,$value->Nama,$value->Jumlah,$keterangan);
                            $status_briva=$briva['status'];
                            if($status_briva){
                                DB::connection('sisfo_pmb')->table('pmbformjual')->where('PMBID',$PMBID)->where('Jumlah',$value->Jumlah)->update([
                                    'response'=>3,
                                    'kodebriva'=>$kodebayar
                                ]);
                            }else{
                                echo json_encode($status_briva);
                            }
                            DB::table('log_briva')->insert([
                                'reference'=>'created',
                                'payload'=>json_encode($briva),
                                'created_ip'=>'172.27.1.67'
                            ]);
                            Log::info('OK');
                            DB::commit();
                            DB::connection('sisfo_pmb')->commit();
                        }
                        
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                    Log::info($th);
                    DB::rollBack();
                    DB::connection('sisfo_pmb')->rollBack();
                }
            }
        }
        
        echo 'Cron kita sudah jalan!';
    }
}
