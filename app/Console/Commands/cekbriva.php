<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Helper;

class cekbriva extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'cek:briva';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Proses cek payment briva';
    
    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        //
        $startDate=date('Ymd');
        $endDate=date('Ymd');
        $response=Helper::q_briva_report($startDate,$endDate);
        $status=$response['status'];
        DB::beginTransaction();
        if ($status) {
            $data=$response['data'];
            foreach ($data as $key => $value) {
                $custCode=$value['custCode'];
                $brivaNo=$value['brivaNo'];
                $nama=$value['nama'];
                $keterangan=$value['keterangan'];
                $amount=$value['amount'];
                $paymentDate=$value['paymentDate'];
                $tellerid=$value['tellerid'];
                $no_rek=$value['no_rek'];
                try {
                    $cek=DB::table('pmbformjual')->where('response',3)->where('kodebriva',$custCode)->first();
                    if(empty($cek)){

                    }else{
                        $PMBFormJualID=$cek->PMBFormJualID;
                        $category_bills=DB::table('bills_category')->where('name','PMB REG')->first();
                        $id_category=$category_bills->id;
                        $prodi_id=$cek->ProdiID;
                        $institutionCode=env('BRIVA_ID');
                        DB::table('statements')->insert([
                            'company_id'=>$institutionCode,
                            'accounts'=>$no_rek,
                            'category_id'=>$id_category,
                            'majors'=>$prodi_id,
                            'trx_date'=>date('Y-m-d',strtotime($paymentDate)),
                            'datetime'=>$paymentDate,
                            'invoice_number'=>$PMBFormJualID,
                            'corporate_code'=>$brivaNo,
                            'customer_code'=>$custCode,
                            'debit'=>0,
                            'credit'=>$amount,
                            'teller_id'=>$tellerid,
                            'status'=>5,
                            'created_at'=>date('Y-m-d H:i:s')
                        ]);
                        DB::table('pmbformjual')->where('PMBFormJualID',$PMBFormJualID)->update([
                            'response'=>5
                        ]);
                    }
                    DB::commit();
                } catch (\Throwable $e) {
                    DB::rollBack();
                    continue;
                }
            }
        }else{
            dd($response);
        }
        echo 'Cron kita sudah jalan!';
    }
}
