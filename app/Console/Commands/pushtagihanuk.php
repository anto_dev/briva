<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Helper;
use Illuminate\Support\Facades\Log;

class pushtagihanuk extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'push:brivatagihanuk';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Proses push tagihan uk briva';
    
    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        //
        $institutionCode='EOUG186526C';
        $coroprateID='77678';
        $sekarang=date('Y-m-d');
        $tahunid=2015;
        try {
            $data_akademik=DB::connection('sisfo')->table('biayauk')->join('mhsw','mhsw.MhswID','biayauk.MhswID')->where('biayauk.response',1)->where('biayauk.Total','>',0)->where('biayauk.TahunID','>=',$tahunid)->select('biayauk.*','mhsw.Nama','mhsw.ProdiID')->get();
            dd(($data_akademik));
            foreach ($data_akademik as $key => $value) {
                try {
                    $keteranganset='';
                    $IDKEU=$value->IDKEU;
                    $MhswID=$value->MhswID;
                    $Keterangan=trim($value->Keterangan);
                    DB::beginTransaction();
                    DB::connection('sisfo')->beginTransaction();
                    $status_briva=false;
                    $cek_invoice=DB::table('invoices')->where('npm',$MhswID)->where('Status','process')->where('response',1)->first();
                    // dd($cek_invoice);
                    if(empty($cek_invoice)){
                        $ins=DB::table('invoices')->insert([
                            'IDKEU'=>$value->IDKEU,
                            'company_id'=>$institutionCode,
                            'corporate_code'=>$coroprateID,
                            'npm'=>$MhswID,
                            'years'=>$value->TahunID,
                            'orders'=>0,
                            'category_id'=>0,
                            'invoice_number'=>$value->IDKEU,
                            'amount'=>$value->Total,
                            'status'=>$value->Status,
                            'description'=>$value->Keterangan,
                            'major'=>$value->ProdiID,
                            'response'=>$value->response
                        ]);
                        $detail=DB::connection('sisfo')->table('detilbiayauk')->where('IDKEU',$IDKEU)->where('MhswID',$MhswID)->where('Status',"process")->get();
                        // dd($detail);
                        foreach ($detail as $key2 => $el) {
                            $IDBiaya=$el->IDBiaya;
                            $CicilanID=$el->CicilanID;
                            if (is_numeric($CicilanID)) {
                                if ($CicilanID == 5) {
                                    $keteranganset=Helper::q_label_bills_category($CicilanID);
                                    //  dd($keteranganset);
                                }elseif($Keterangan=='PINDAH' || $Keterangan=='AKTIF KEMBALI'){
                                    $keteranganset=$Keterangan;
                                }else{
                                    if (strpos($keteranganset,$CicilanID)===false) {
                                        $keteranganset.='CCL '.$CicilanID.' ';
                                    }
                                }
                            }else{
                                $keteranganset=$Keterangan;
                            }
                            // dd($keteranganset);
                            $IDBiaya=$el->IDBiaya;
                            $res=Helper::q_bills_category($IDBiaya);
                            $cek_bills=DB::table('bills')->where('IDKEU',$IDKEU)->where('CicilanID',$CicilanID)->where('MhswID',$MhswID)->where('IDBiaya',$IDBiaya)->first();
                            if(empty($cek_bills)){
                                $updated_at = Helper::setdatetime($el->updated_at);
                                DB::table('bills')->insert([
                                    'IDKEU'=>$el->IDKEU,
                                    'CicilanID'=>$el->CicilanID,
                                    'MhswID'=>$el->MhswID,
                                    'TahunID'=>$el->TahunID,
                                    'BipotID'=>$el->BipotID,
                                    'IDBiaya'=>$IDBiaya,
                                    'TotalBiaya'=>$el->TotalBiaya,
                                    'Status'=>$el->Status,
                                    'LoginBuat'=>$el->LoginBuat,
                                    'TglBuat'=>$el->TglBuat,
                                    'NA'=>$el->NA,
                                    'updated_at'=>$updated_at
                                ]);
                            }else{
                                
                            }
                            $up=DB::table('invoices')->where('IDKEU',$IDKEU)->update([
                                'category_id'=>$res['category_id'],
                                'orders'=>$res['urutan']
                            ]);
                        }
                        $briva=Helper::q_briva_create($MhswID,$value->Nama,$value->Total,trim($keteranganset));
                        $status_briva=$briva['status'];
                        DB::table('log_briva')->insert([
                            'reference'=>'created',
                            'payload'=>json_encode($briva),
                            'created_ip'=>'172.27.1.67'
                        ]);
                    }else{
                        $IDKEU_prev=$cek_invoice->IDKEU;
                        // dd($IDKEU);
                        if ($IDKEU_prev==$IDKEU) {
                            $detail=DB::connection('sisfo')->table('detilbiayauk')->where('IDKEU',$IDKEU)->where('MhswID',$MhswID)->where('Status',"process")->get();
                            // dd('ada');
                            $status_up=false;
                            foreach ($detail as $key2 => $el) {
                                $IDBiaya=$el->IDBiaya;
                                $res=Helper::q_bills_category($IDBiaya);
                                $orders_prev=$cek_invoice->orders;
                                $curr_urutan=$res['urutan'];
                                $curr_category_id=$res['category_id'];
                                if ($curr_urutan<=$orders_prev) {
                                    $status_up=true;
                                }
                            }
                            if($status_up){
                                DB::table('invoices')->where('npm',$MhswID)->where('response',1)->delete();
                                $briva_delete=Helper::q_briva_delete($MhswID);
                                $ins=DB::table('invoices')->insert([
                                    'IDKEU'=>$value->IDKEU,
                                    'company_id'=>$institutionCode,
                                    'corporate_code'=>$coroprateID,
                                    'npm'=>$MhswID,
                                    'years'=>$value->TahunID,
                                    'orders'=>$curr_urutan,
                                    'category_id'=>$curr_category_id,
                                    'invoice_number'=>$value->IDKEU,
                                    'amount'=>$value->Total,
                                    'status'=>$value->Status,
                                    'description'=>$value->Keterangan,
                                    'major'=>$value->ProdiID,
                                    'response'=>$value->response
                                ]);
                                foreach ($detail as $key2 => $el) {
                                    $IDBiaya=$el->IDBiaya;
                                    if (is_numeric($el->CicilanID)) {
                                        if ($el->CicilanID == 5) {
                                            $keteranganset=Helper::q_label_bills_category($el->CicilanID);
                                            //  dd($keteranganset);
                                        }elseif($Keterangan=='PINDAH' || $Keterangan=='AKTIF KEMBALI'){
                                            $keteranganset=$Keterangan;
                                        }else{
                                            if (strpos($keteranganset,$el->CicilanID)===false) {
                                                $keteranganset.='CCL '.$el->CicilanID.' ';
                                            }
                                        }
                                    }else{
                                        $keteranganset=$Keterangan;
                                    }
                                    // dd($keteranganset);
                                    $updated_at = Helper::setdatetime($el->updated_at);
                                    DB::table('bills')->insert([
                                        'IDKEU'=>$el->IDKEU,
                                        'CicilanID'=>$el->CicilanID,
                                        'MhswID'=>$el->MhswID,
                                        'TahunID'=>$el->TahunID,
                                        'BipotID'=>$el->BipotID,
                                        'IDBiaya'=>$IDBiaya,
                                        'TotalBiaya'=>$el->TotalBiaya,
                                        'Status'=>$el->Status,
                                        'LoginBuat'=>$el->LoginBuat,
                                        'TglBuat'=>$el->TglBuat,
                                        'NA'=>$el->NA,
                                        'updated_at'=>$updated_at
                                    ]);
                                }
                                $briva=Helper::q_briva_create($MhswID,$value->Nama,$value->Total,trim($keteranganset));
                                $status_briva=$briva['status'];
                                DB::table('log_briva')->insert([
                                    'reference'=>'created',
                                    'payload'=>json_encode($briva),
                                    'created_ip'=>'172.27.1.67'
                                ]);
                            }else{
                                Log::info('IDKEU Previous '.$IDKEU_prev.' != IDKEU Incoming '.$IDKEU);
                            }
                        }
                    }
                    
                    if($status_briva){
                        DB::connection('sisfo')->table('biayauk')->where('MhswID',$MhswID)->where('IDKEU',$IDKEU)->update([
                            'response'=>3
                        ]);
                    }else{
                        echo json_encode($status_briva);
                    }
                    
                    DB::commit();
                    DB::connection('sisfo')->commit();
                } catch (\Throwable $th) {
                    //throw $th;
                    DB::rollBack();
                    DB::connection('sisfo')->rollBack();
                    // dd($th);
                    Log::error($th);
                    continue;
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th);
            Log::error($th);
        }
    }
    public function auhtorization()
    {
        $url    = 'https://partner.api.bri.co.id/oauth/client_credential/accesstoken?grant_type=client_credentials';
        $ch = curl_init($url);
        $data=[
            "client_id"=>"zZLjDbAYeT5FqtKv9tUA0dK7shcW6upZ",
            "client_secret"=>"a2csxAP7aC6ynAMv"
        ];
        $requestBody=($data);
        // dd($requestBody);
        $header = array(
            'Content-Type:application/x-www-form-urlencoded'
        );
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=zZLjDbAYeT5FqtKv9tUA0dK7shcW6upZ&client_secret=a2csxAP7aC6ynAMv");
        
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        return $result;
    }
}
