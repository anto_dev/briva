<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Helper;
use Illuminate\Support\Facades\Log;

class getstatements extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'get:statements';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Proses get statements briva';
    
    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        //
        // $startDate=date('Y-m-d');
        // $endDate=date('Y-m-d');
        $startDate='20220212';
        $endDate='20220212';
        $response=Helper::q_briva_report($startDate,$endDate);
        // dd($response);
        $status=$response['status'];
        if ($status) {
            $data=$response['data'];
            // dd($data);
            foreach ($data as $key => $value) {
                $custCode=$value['custCode'];
                $brivaNo=$value['brivaNo'];
                $nama=$value['nama'];
                $keterangan=$value['keterangan'];
                $amount=$value['amount'];
                $paymentDate=$value['paymentDate'];
                $tellerid=$value['tellerid'];
                $no_rek=$value['no_rek'];
                try {
                    DB::beginTransaction();
                    DB::connection('sisfo')->beginTransaction();
                    $cek=DB::table('invoices')->where('response',1)->where('npm',$custCode)->where('amount',$amount)->first();
                    // var_dump($cek);
                    // dd($cek);
                    if(empty($cek)){
                        
                    }else{
                        $IDKEU=$cek->IDKEU;
                        $id_category = $cek->category_id;
                        $prodi_id=$cek->major;
                        $institutionCode=env('BRIVA_ID');
                        DB::table('statements')->insert([
                            'company_id'=>$institutionCode,
                            'accounts'=>$no_rek,
                            'category_id'=>$id_category,
                            'majors'=>$prodi_id,
                            'trx_date'=>date('Y-m-d',strtotime($paymentDate)),
                            'datetime'=>$paymentDate,
                            'invoice_number'=>$IDKEU,
                            'corporate_code'=>$brivaNo,
                            'customer_code'=>$custCode,
                            'debit'=>0,
                            'credit'=>$amount,
                            'teller_id'=>$tellerid,
                            'status'=>5,
                            'created_at'=>date('Y-m-d H:i:s')
                        ]);
                        DB::connection('sisfo')->table('biayauk')->where('IDKEU',$IDKEU)->where('MhswID',$custCode)->where('Total',$amount)->where('response',3)->update([
                            'response'=>5,
                            'RekeningID'=>$no_rek,
                            'Status'=>'sudah bayar',
                            'TglSetor'=>$paymentDate,
                            'KeteranganBayar'=>'BRI '.$tellerid,
                            'LoginProses'=>'briva',
                            'TanggalProses'=>$paymentDate
                        ]);
                        DB::connection('sisfo')->table('detilbiayauk')->where('IDKEU',$IDKEU)->where('MhswID',$custCode)->update([
                            'Status'=>'sudah bayar',
                            'updated_at'=>$paymentDate
                        ]);
                        DB::table('invoices')->where('IDKEU',$IDKEU)->where('npm',$custCode)->update([
                            'Status'=>'sudah bayar',
                            'response'=>5
                        ]);
                        $briva_delete=Helper::q_briva_delete($custCode);
                        DB::table('log_briva')->insert([
                            'reference'=>'delete',
                            'payload'=>json_encode($briva_delete),
                            'created_ip'=>'172.27.1.67'
                        ]);
                    }
                    DB::commit();
                    DB::connection('sisfo')->commit();
                } catch (\Throwable $e) {
                    Log::error($e);
                    DB::rollBack();
                    DB::connection('sisfo')->rollBack();
                    continue;
                }
            }
        }else{
            Log::error($response);
        }
        echo 'Cron kita sudah jalan!';
    }
}
