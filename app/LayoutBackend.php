<?php 

namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use URL;

/**
 * 
 */
class LayoutBackend
{
	
	static function renderMenu($parent = 0, $resultItem = ""){
		$grupuser=Auth::guard('Admin')->user()->grupUser;
		$qakses=DB::table('tb_grupuser')->where('id',$grupuser)->first();
		$akses=$qakses->akses;
		$data=DB::table('tb_menuopen')->where('idgrupuser',$grupuser)->first();
		if (empty($data)) {
			return response()->view('errors.custom', [], 500);
		}
		$arrmodul=explode(',', $data->idmodul);
		$items = DB::table('tb_modul')
		->where('tb_modul.parent', $parent)
		->where('tb_modul.status','1')
		->where('tb_modul.menu','1')
		->whereIn('tb_modul.akses',['0','2','1'])
		->whereIn('tb_modul.id',$arrmodul)
		->orderBy('tb_modul.parent')
		->orderBy('tb_modul.urutan')
		->select('tb_modul.*')
		->distinct()
		->get();
		foreach($items as $item){
			if(LayoutBackend::checkChild($item->id)){
				$resultItem .= '<li class="nav-item">
				<a class="nav-link collapsed" data-toggle="collapse" href="#'.$item->defaultLink.'" role="button" aria-expanded="false" aria-controls="'.$item->folder.'">
				<span>'.$item->namaModul.'</span>
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down link-arrow"><polyline points="6 9 12 15 18 9"></polyline></svg>
				</a>
				<div class="collapse" id="'.$item->defaultLink.'">
				<ul class="nav sub-menu">
				';
				$resultItem .= LayoutBackend::renderMenu($item->id);
				$resultItem .='</ul></div></li>';   
			}
			else{
				$resultItem .= '<li class="nav-item">
					<a href="'.url(env('PREFIX_ADMIN').'/'.$item->defaultLink).'" class="no-session nav-link" id="ajaxlink" role="button" aria-expanded="false" aria-controls="'.$item->folder.'">
					<span>'.$item->namaModul.'</span>
					</a>
					</li>';
			}
		}
		return $resultItem;
	}

	static function checkChild($id = 0){
		$cek = DB::table('tb_modul')->where('parent',$id)->where('status','1')->get();
		if(count($cek) > 0){
			return true;
		}
		return false;
	}

	static function getSetting(){
		$setting = DB::table('tb_pengaturanweb')->first();
		return $setting;
	}
}


?>