<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class Admin 

{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard('Admin')->check()) {
            return redirect()->route('auth');
        }
        return $next($request);
    }
}
