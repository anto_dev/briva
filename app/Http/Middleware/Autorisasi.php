<?php

namespace App\Http\Middleware;

use Closure;
use App\Tb_User as User;
use Lang;
use Auth;
class Autorisasi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $izin = ($request->route()->getAction()['as'] !== null) ? $request->route()->getAction()['as'] : null;
        if(Auth::guard('Admin')->user() === null){
            return response(Lang::get('globals.403'),403);
        }
        if($izin === null){
            return response(Lang::get('globals.400'),400);   
        }
        if(User::izinMasuk($izin)){
            return $next($request);
        }
        return response(Lang::get('globals.403'),403);
    }
}
