<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModulesBackend\datamember\model\datamembermodel;
use DB;
use Helper;
use Validator;
use Image;
use Mail;
use Lang;
class apilinkcontroller extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(){
        return view('formmail');
    }
    public function dosen(Request $request){
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://localhost/sdid/frontend/public/api.php/0.1/Referensi/doseninternal",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n  \"token\": \"0a6502b6d35c9c839520a1bc55ca0f44\",\n  \"id_pengguna\": \"c9c36aed-631b-4e9f-815a-13bcf91615d5\"\n}",
            CURLOPT_COOKIE => "PHPSESSID=tgdlq1gonnt3sl1bf5b38ghsu4",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
            $res['message']=$err;
        } else {
            $res['message']='OK';
        }
        return $response;
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function auhtorization()
    {
        $url    = 'https://partner.api.bri.co.id/oauth/client_credential/accesstoken?grant_type=client_credentials';
        $ch = curl_init($url);
        $data=[
            "client_id"=>"zZLjDbAYeT5FqtKv9tUA0dK7shcW6upZ",
            "client_secret"=>"a2csxAP7aC6ynAMv"
        ];
        $requestBody=($data);
        // dd($requestBody);
        $header = array(
            'Content-Type:application/x-www-form-urlencoded'
        );
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=zZLjDbAYeT5FqtKv9tUA0dK7shcW6upZ&client_secret=a2csxAP7aC6ynAMv");
        
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        return $result;
    }
    function BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret) {
        echo $payloads = "path=$path&verb=$verb&token=Bearer $token&timestamp=$timestamp&body=$payload";
        $signPayload = hash_hmac('sha256', $payloads, $secret, true);
        return base64_encode($signPayload);
    }
    public function qsimpanpendaftaran(Request $request)
    {
        
        // dd($token);
        $rules=[
            'NIK'=>'required',
            'NoHandphone'=>'required',
            'Nama' => 'required',
            'Email' => 'required|email|unique:pmbformjual,Email',
            'Fakultas' => 'required'
        ];
        $message = [
            'unique' => ' :attribute '.Lang::get('globals.unique'),
            'required' => Lang::get('globals.required').' :attribute'
        ];
        $validator = Validator::make($request->input(), $rules, $message);
        if($validator->fails()){
            $error = $validator->errors()->messages();
            return response()->json($error, 400);
        }
        $nik=$request->input('NIK');
        $noTelp=$request->input('NoHandphone');
        $Nama=$request->input('Nama');
        $email=$request->input('Email');
        $faculty_id=$request->input('Fakultas');
        $prodi_id=$request->input('Prodi');
        $qprodi=DB::table('prodi')->where('ProdiID',$prodi_id)->first();
        if (empty($qprodi)) {
            return response('Data prodi tidak ditemukan',409);
        }
        $prodi=$qprodi->Nama;
        $formulir_id=DB::table('pmbformulir')->where('FakultasID',$faculty_id)->first();
        if (empty($formulir_id)) {
            return response('Data formulir tidak ditemukan',409);
        }
        $formulir_idpmb=$formulir_id->PMBFormulirID;
        $nominal=$formulir_id->Harga;
        $noreg=$this->getCounter('RG');
        $institutionCode="EOUG186526C";
        $brivaNo="77678";
        $kodebayar='22'.date('y').'99'.substr($noreg,-4);
        $keterangan="PMB Test";
        $expired_at="2022-02-28 12:36:04";
        $periode=DB::table('pmbperiod')->where('NA','N')->first();
        if (empty($periode)) {
            return response('Data periode tidak ditemukan',409);
        }
        $periodepmb=$periode->PMBPeriodID;
        try {
            $res=$this->auhtorization();
            $result=json_decode($res);
            $token=$result->access_token;
            $url = 'https://partner.api.bri.co.id/v1/briva';
            $data=[
                "institutionCode"=>$institutionCode,
                "brivaNo"=>$brivaNo,
                "custCode"=>$kodebayar,
                "nama"=>$Nama,
                'amount'=>"$nominal",
                "keterangan"=>$keterangan,
                "expiredDate"=>$expired_at
            ];
            $requestBody=json_encode($data);
            $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
            $path = "/v1/briva";
            $payload='path='.$path.'&verb=POST&token=Bearer '.$token.'&timestamp='.$timestamp.'&body={"institutionCode":"'.$institutionCode.'","brivaNo":"'.$brivaNo.'","custCode":"'.$kodebayar.'","nama":"'.$Nama.'","amount":"'.$nominal.'","keterangan":"'.$keterangan.'","expiredDate":"'.$expired_at.'"}';
            $key='a2csxAP7aC6ynAMv';
            $payraw=$payload;
            $sign4=hash_hmac('SHA256',$payraw,$key,true);
            $base64sign=base64_encode($sign4);
            $request_headers = array(
                "Content-Type:"."application/json",
                "Authorization:Bearer " . $token,
                "BRI-Timestamp:" . $timestamp,
                "BRI-Signature:" . $base64sign,
            );
            $verb = "POST";
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL, $url);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, $verb); 
            curl_setopt($chPost, CURLOPT_POSTFIELDS, $requestBody);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            
            $resultPost = curl_exec($chPost);
            $result=json_decode($resultPost,true);
            $status=$result['status'];
            if($status){
                
            }
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);
            DB::table('log_briva')->insert([
                'reference'=>'created',
                'payload'=>json_encode($result),
                'created_ip'=>$request->ip()
            ]);
            //close cURL resource
            \Session::put('receiver',$email);
            $param_url=base64_encode($noreg);
            \Session::put('noreg',$param_url);
            \Session::put('nama',$Nama);
            \Session::put('prodi',$prodi);
            \Session::put('kodebriva',$brivaNo . $kodebayar);
            \Session::put('nominal',number_format($nominal,2,',','.'));
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('mailPMB', [], function($message)
            {
                $message
                ->from('pmb@uhn.ac.id')
                ->to(\Session::get('receiver'))
                ->subject('Pendaftaran Mahasiswa Baru');
            });
            DB::table('pmbformjual')->insert([
                'PMBFormJualID'=>$noreg,
                'PMBFormulirID'=>$formulir_idpmb,
                'KodeID'=>'SISFO',
                'ProdiID'=>$prodi_id,
                'Tanggal'=>date('Y-m-d'),
                'PMBPeriodID'=>$periodepmb,
                'BuktiSetoran'=>'',
                'Nama'=>$Nama,
                'LoginBuat'=>str_replace(" ", "", $Nama),
                'TanggalBuat'=>date('Y-m-d'),
                'Keterangan'=>'',
                'Jumlah'=>$nominal,
                'CetakanKe'=>0,
                'NA'=>'N',
                'Batal'=>'N',
                'OK'=>'N',
                'noTelp'=>$noTelp,
                'Email'=>$email,
                'NIK'=>$nik,
                'active_account'=>'N',
                'kodebriva'=>$kodebayar,
                'response'=>3,
                'email_sent'=>'Y'
            ]);
            DB::table('tagihan_briva')->insert([
                'kode_briva'=>$kodebayar,
                'invoice_number'=>$noreg,
                'nama'=>$Nama,
                'amount'=>$nominal,
                'response'=>3,
                'keterangan'=>'PMB REG',
                'created_ip'=>$request->ip()
            ]);
            return response('Silahkan aktivasi akun anda melalui email yang sudah didaftarkan',200);
        } catch (\Throwable $e) {
            return response($e,409);
        }
    }
    public function qgetFaculty(Request $request){
        $NamaPanjang=$request->input('NamaPanjang');
        $data=DB::connection('mysqlSisfo')->table('fakultas')->where('NamaPanjang','LIKE','%'.$NamaPanjang.'%')->get();
        return $data;
    }
    public function qgetProdi(Request $request,$param){
        $NamaPanjang=$request->input('NamaPanjang');
        $data=DB::table('prodi')->where('NA','N')->where('FakultasID',$param)->where('Nama','LIKE','%'.$NamaPanjang.'%')->get();
        return $data;
    }
            /**
            * Store a newly created resource in storage.
            *
            * @param  \Illuminate\Http\Request  $request
            * @return \Illuminate\Http\Response
            */
            public function getCounter($type)
            {
                //
                $cek=DB::connection('mysqlSisfo')->table('counter')->where('jenis',$type)->first();
                $tahun=date('y');
                if(empty($cek)){
                    $counter=1;
                    DB::connection('mysqlSisfo')->table('counter')->insert([
                        'tahun'=>$tahun,
                        'jenis'=>'RG',
                        'counter'=>$counter
                    ]);
                }else{
                    $last=$cek->counter;
                    $counter=$last+1;
                    DB::connection('mysqlSisfo')->table('counter')->where('tahun',$tahun)->where('jenis',$type)->update([
                        'counter'=>$counter
                    ]);
                }
                return $noreg='K42-'.$tahun.'-'.sprintf('%04d',$counter);
                
            }
            
            /**
            * Display the specified resource.
            *
            * @param  int  $id
            * @return \Illuminate\Http\Response
            */
            public function prosesaktivasi($param)
            {
                $paramid=base64_decode($param);
                $cek=DB::table('pmbformjual')->where('PMBFormJualID',$paramid)->select('active_account')->first();
                if(empty($cek)){
                    return view("aktivasi",["data"=>"Akun anda tidak ditemukan",'status'=>'N']);
                }
                if($cek->active_account=="Y"){
                    return view("aktivasi",["data"=>"Akun anda sudah diaktivasi",'status'=>'N']);
                }
                return view("aktivasi",["data"=>"Akun anda berhasil diaktivasi",'status'=>'Y','id'=>$param]);
            }
            public function informasi(){
                // $data=DB::table('tb_informasiweb')->get();
                $data = 'haholongi inang mi';
                return view("informasi",['data'=>$data]);
            }
            /**
            * Show the form for editing the specified resource.
            *
            * @param  int  $id
            * @return \Illuminate\Http\Response
            */
            public function edit($id)
            {
                //
            }
            
            /**
            * Update the specified resource in storage.
            *
            * @param  \Illuminate\Http\Request  $request
            * @param  int  $id
            * @return \Illuminate\Http\Response
            */
            public function update(Request $request, $id)
            {
                //
            }
            
            /**
            * Remove the specified resource from storage.
            *
            * @param  int  $id
            * @return \Illuminate\Http\Response
            */
            public function destroy($id)
            {
                //
            }

        }
        