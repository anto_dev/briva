<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Lang;
use Validator;  
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('Admin');
    }

    function showLoginAdmin(Request $request){
        return view('auth.loginadmin');
    }
    function showLogin(Request $request){
        return view('login');
    }

    function prosesLoginAdmin(Request $request){
        $rules = [
            'username' => 'required',
            'password' => 'required',
        ];
        // if(Setting::getSetting()->UseRecaptcha == 'Y'){
        //     $rules['g-recaptcha-response'] = 'required|recaptcha';
        // }
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json($validator->messages(), 403);
        }
        // if(Auth::guard('Admin')->attempt(['username' => $request->input('username'), 'password' => $request->input('password'),'type'=>0])){
        if(Auth::guard('Admin')->attempt(['username' => $request->input('username'), 'password' => $request->input('password')])){
            // return response(Auth::guard('Admin')->user()->status,409);
            if(Auth::guard('Admin')->user()->status == 0){
                Auth::guard('Admin')->logout();
                return response()->json(Lang::get('auth.tidakAktif'),401);
            }
            $datagrup=DB::table('tb_grupuser')->where('id',Auth::guard('Admin')->user()->grupUser)->first();
            $statusgrup=$datagrup->status;
            $akses=$datagrup->akses;
            
            if ($statusgrup=='N') {
                Auth::guard('Admin')->logout();
                return response()->json('Grup user anda tidak aktif',401);
            }
            DB::table('users')->where('username',$request->input('username'))->update([
                'last_login_at'=>date('Y-m-d H:i:s')
            ]);
            return response()->json(['code' => 1,'akses' => $akses ,'data' => Lang::get('auth.suksesLogin') ],200);
        }
        else
        {
            return response()->json(['code' => 0, 'data' => Lang::get('auth.gagalLogin') ],401);
        }
    //     $rules=[
    //         'username' => 'required',
    //         'password' => 'required',
    //     ];

    //     $message=[
    //         'required'=>"Isian :attribute diperlukan",
    //     ];

    //     $validator=Validator::make($request->input(),$rules,$message);
    //     if ($validator->fails()) {
    //         $errors=$validator->errors()->messages();
    //         return back()->withErrors($errors)->withInput(Input::except('password'));
    //     }

    //     if (Auth::guard('Admin')->attempt(['username'=>$request->input('username'),'password'=>$request->input('password'),'type'=>0])) {

    //        return redirect()->intended('admin/beranda');

    //    }
    //    $errors=[
    //     'userInvalid' => 'Akun tidak ditemukan',
    // ];
    // return back()->withErrors($errors)->withInput(Input::except('password'));

    }
    function logoutAdmin(Request $request){        
        Auth::guard('Admin')->logout();

        return redirect($to = 'login', $status = 302, $errors = ['pesan'=>'Anda telah logout'], $secure = null);

    }

}
