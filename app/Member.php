<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'noregistrasi', 'nomember', 'tanggalregistrasi', 'kodecabang','kodejenismember','namamember','alamat','kota','kodepos','telepon','handphone','email','kodejenisidentitas','noidentitas','tempatlahir','tanggallahir','kewarganegaraan','kodeagama','kodestatus','jumlahanak','kodejeniskelamin','blokir','poin','internal','eksternal'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   ;
}
